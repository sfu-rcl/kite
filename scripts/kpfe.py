#
# Copyright 2021  Stuart Hoad, Lesley Shannon
# Reconfigurable Computing Lab, Simon Fraser University.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):
#             Stuart Hoad <stuart_hoad@sfu.ca>
#
# kpfe.py
# Kite Parse Front-end
# - Reads kits configuration files and puts them in defined structures (dicts, lists, etc)
# - Generates an intermediate representation of transitions in guarded action form
# - Output is used to generate methodology specific properties and models


import re
import sys
import os
import copy
import json5
import subprocess
import glob

from kpfe import kpfe_parse

#----------------------------------------------------------
#----------------------------------------------------------

#----------------------------------------------------------
# function definitions


#----------------------------------------------------------                
#----------------------------------------------------------
def main():
    """ main routine """
    
    # Specify Kite model files
    #   - Target config
    #   - Common files
    #   - Instructions
    #   - Interrupts/Resets
    #   - State 
    

    
    kite_files_all_def = []
    all_defs = {}
    model_cfg = {}
    model_cfg_pre_alias = {}
    if(len(sys.argv) != 2) :
        print("illegal number of arguments")
        print("usage: kite_parser.py <kite config file>")
        sys.exit("")
        
    kpfe_parse.kpfe_parse(sys.argv[1])
                                                            

#----------------------------------------------------------
if __name__ == '__main__':
    main()
