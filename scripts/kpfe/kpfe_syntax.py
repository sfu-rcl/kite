#
# Copyright 2020  Stuart Hoad, Lesley Shannon
# Reconfigurable Computing Lab, Simon Fraser University.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):
#             Stuart Hoad <stuart_hoad@sfu.ca>
#
# kpfe_syntax.py
# Handles syntax checking of the kite spec

import re
import sys
import os
import copy
import json5

from kpfe import kpfe_common

#----------------------------------------------------------  
#---------------------------------------------------------- 

#---------------------------------------------------------- 
def check_extract_size(all_defs, field, name):

    extracted, dfield = kpfe_common.extract_pcontents(["(",")"], field)
    if not extracted:
        sys.exit("Error. Could not extract any field for size extraction from "+field+", in "+name)
    # check if dfield is defined as an enum, state, subfield, or global
    found_dfield = False
    if kpfe_common.is_defined_var(all_defs, dfield):
        found_dfield = True
    elif dfield in all_defs['enums'].keys():  
        found_dfield = True       
    if not found_dfield:
        sys.exit("Error. "+field+" is not defined, in "+name)

#----------------------------------------------------------            
def check_task_type(all_defs, field, name):
    found_ftype = False
    # the type can be logic (with or without array dimensions), integer, or an enum
    if field.startswith("logic"):
        # check if any dimensions (optional), if so check they are legally defined
#        extracted, dims = kpfe_common.extract_pcontents(["[","]"], field)
#        if extracted:
#            check_dimensions("["+dims+"]", name)
        found_ftype = True
    elif field == "integer":
        found_ftype = True
    elif (field in all_defs['enums'].keys()) | (field in all_defs['typedefs'].keys()):  
        # if not an integer or logic type, must be defined as an enumerated type or typdef
        found_ftype = True
    else:
        sys.exit("Error. Illegal task type for "+field+" in "+name)

#---------------------------------------------------------- 
def check_dimensions(field, name):

    dmatch = re.match(r'^\[(.+)\:(.+)\]$',field)
    found_dims = False
    if dmatch:
        try:
            bound0 = eval(dmatch.group(1))
            if bound0 < 0: 
                sys.exit("Error. Bound 0 is negative. In "+name)
        except:
            sys.exit("Error. Bound 0 is "+dmatch.group(1)+". Does not evaluate to an integer. In "+name)
        try:
            bound1 = eval(dmatch.group(2))
            if bound1 < 0: 
                sys.exit("Error. Bound 1 is negative. In "+name)
        except:
            sys.exit("Error. Bound 1 is "+dmatch.group(2)+". Does not evaluate to an integer. In "+name)  
        found_dims = True
    if not found_dims: 
        sys.exit("Error. Dimensions must be defined as [<bound0>:<bound1>] "+name)


#---------------------------------------------------------- 
def check_exists(all_defs, name):
    """ check the field is defined """
    in_def = False
    def_type = ""    
    for def_key in all_defs.keys():
        if name in all_defs[def_key]:
            in_def = True
            def_type = def_key
            break
            
    return in_def, def_type

#----------------------------------------------------------
def check_conditional_update(all_defs, bi_ops_regex, cu_to_check, instruction_name, field):
    if not isinstance(cu_to_check, list):
        sys.exit("Error. "+field+" field must be a list. In instruction definition "+instruction_name) 
    for clause in cu_to_check:
        if not('condition' in clause ):
            sys.exit("Error. No condition field found in "+field+" clause. In instruction definition "+instruction_name)    
        check_legal_expression(all_defs, clause['condition'], bi_ops_regex)
        if not('state_action_list' in clause):
            sys.exit("Error. No state_action_list field found in "+field+" clause. In instruction definition "+instruction_name)
        if not isinstance(clause['state_action_list'], list):
            sys.exit("Error. state_action list in "+field+" field must be a list. In instruction definition "+instruction_name)
        for curr_action in clause['state_action_list']:
            if not('state_to_update' in curr_action):
                sys.exit("Error. No state_to_update field found in "+field+" clause. In instruction definition "+instruction_name)
            if not isinstance(curr_action['state_to_update'], str):
                sys.exit("Error. "+field+" state_to_update field must be a string. In instruction definition "+instruction_name) 
            if not('update_to_value' in curr_action):
                sys.exit("Error. No update_to_value field found in "+field+" clause. In instruction definition "+instruction_name)
            check_legal_expression(all_defs, curr_action['update_to_value'], bi_ops_regex)
            if not('condition' in curr_action):
                sys.exit("Error. No condition field found in "+field+" clause. In instruction definition "+instruction_name)
            check_legal_expression(all_defs, curr_action['condition'], bi_ops_regex)
                
#----------------------------------------------------------
def check_task(all_defs, task_name, task_asmt, call_term, bi_ops_regex):
    """checks the task call is correct"""
    # task call should be of the form:
    # <task_name>(<inputs>,<outputs>)
    # where inputs and outputs are of the form <formal>=<actual>
    term_args = call_term.split(',')
    mapped_list = []
    call_term_actuals = []
    chk_list = all_defs['tasks'][task_name]['task_args'].keys() 
    chk_list.extend(all_defs['tasks'][task_name]['task_return'].keys())
    
    for term_arg in term_args:
        map_term = ""
        arg_map = term_arg.split('=',1)
        if not(len(arg_map) == 2):
            sys.exit("Error. Incorrect mapping of task arguments in "+call_term)
        map_actual = arg_map[1].lstrip().rstrip()
        map_formal = arg_map[0].lstrip().rstrip()
        mapped_list.append(map_formal)
        call_term_actuals.append(map_actual)
        if not (map_formal in chk_list):
            sys.exit("Error. Formal "+map_formal+" not found in task template for "+task_name)
        # the actual is an expression, so check it is legal with recursive check
        check_legal_expression(all_defs, map_actual, bi_ops_regex)      
        
    # check all the args are mapped 
    
    for chk_item in chk_list:
        if not(chk_item in mapped_list):
            sys.exit("Error. Task call "+call_term+" did not map "+chk_item) 
 
    # check that if multiple assignments are used, they are defined as a concatenation
    concat_extracted, concat_asmt = kpfe_common.extract_pcontents(["{","}"], task_asmt)
    if concat_extracted:
        task_asmt = concat_asmt  
    asmt_bits = task_asmt.split(",")
    if (not concat_extracted) & (len(asmt_bits) > 1):
        # can only be one assignment if not a concatenation
        sys.exit("Error. More than one assignment for a non-concatenated term "+task_asmt)
    # check that the number of assignments matches the template
    if(len(all_defs['tasks'][task_name]['task_return']) != len(asmt_bits)):
        sys.exit("Error. In task "+task_name+", number of task assignments does not match template for assignment "+task_asmt)
        
    # check the assignment(s) are mapped
    for abit_idx, asmt_bit in enumerate(asmt_bits):
        curr_sig = asmt_bit.lstrip().rstrip()
        # check these match the ones in the mapping
        if curr_sig not in call_term_actuals:
            sys.exit("Error. task assignment signal "+curr_sig+" is not in task call "+call_term)

#----------------------------------------------------------             
def remove_slices(all_defs, bi_ops_regex, curr_expr):
    """find any slices in the current expression, check they are legal, and remove them"""
    while True:
        extracted, extracted_term, start, end = kpfe_common.extract_pcontents_pos(["[","]"], curr_expr)
        if extracted:
            # check if more to extract
            extracted_term = remove_slices(all_defs, bi_ops_regex, extracted_term)
            # check the extracted term is a legal expression
            # split into range parts and check if each part is legal - note, could be an expression to calculate
            extr_bits = extracted_term.split(":")
            for extr_bit in extr_bits:
                if not extr_bit.isnumeric():
                    check_legal_expression(all_defs, extr_bit, bi_ops_regex)                
            # remove the term
            curr_expr = curr_expr.replace("["+extracted_term+"]","")
            # could still be more temrs, if so continue, otherwise done
            extracted, extracted_term, start, end = kpfe_common.extract_pcontents_pos(["[","]"], curr_expr)
            if not extracted:
                break
        else:
            break        
    return curr_expr
    
#---------------------------------------------------------- 
def check_legal_expression(all_defs, expression, bi_ops_regex):
    """checks an expression is well formed"""
    
    # an expression is well formed if:
    # - parenthesis and array ranges are balanced
    # - operands are legal (defined, or literals) 
    # - operands are correct length
    # - operators are legal
    # - number of operands is correct for the operator
    # - any task calls are:
    #   - defined tasks
    #   - have the correct template
    
    curr_expr = copy.deepcopy(expression) 
    legal_expr = True 
    if not (expression.count("(") == expression.count(")")):
        sys.exit("Error. Inbalanced parenthesis in "+expression)
    if not (expression.count("{") == expression.count("}")):
        sys.exit("Error. Inbalanced concatenation in "+expression)
    if not (expression.count("[") == expression.count("]")):
        sys.exit("Error. Inbalanced index parenthesis in "+expression)
        
    # could be a concatenation, deal with this first (recursively)
    found_concat, concat_inards = kpfe_common.extract_pcontents(["{","}"], curr_expr)
    if found_concat:
        extracted, concat_bits, start = kpfe_common.extract_pcontent_fields(["{","}"], ",", curr_expr, 0)
        for cbit in concat_bits:
            check_legal_expression(all_defs, cbit, bi_ops_regex)
        curr_expr = curr_expr.replace("{"+concat_inards+"}","$$$marker$$$")   
    else:     
        # expression can be nested, so start at the lowest level and work outwards
        # when checked a term or replaced function, replace it with a marker so
        # we don't check it again
        # when all done, should just be a single marker
        
        while True:
            # remove any slice ranges
            curr_expr = remove_slices(all_defs, bi_ops_regex, curr_expr)
            
            # check for task and/or inner term
            extracted_term, task_name, task_asmt, task_repl = kpfe_common.extract_inner_term(all_defs, curr_expr, bi_ops_regex)
            if task_name != "":
                #found a task
                # check the calling term is correct
                check_task(all_defs, task_name, task_asmt, extracted_term, bi_ops_regex)
                # replace the task call with a marker so we don't check it again
                replacee = task_asmt+task_repl
                curr_expr =  curr_expr.replace(task_repl,"$$$marker$$$")
                curr_expr =  curr_expr.replace(task_asmt,"").lstrip().rstrip()
            else:
                # tokenize expression
                # REVISIT use PLY here?
                # REVISIT add better syntax checks when have proper parser
                expr_bits = kpfe_common.tokenize_expr(extracted_term,bi_ops_regex) 
                
                # everything left should be variables (possibly with slices) or literals
                for ebit in expr_bits:
                    #ignore markers and true/false
                    opmatch = re.match(r''+bi_ops_regex,ebit)
                    is_bi_op = False
                    if opmatch:
                        is_bi_op = True    
                    if (ebit != "$$$marker$$$") & (ebit != "true") & (ebit != "false") & (not is_bi_op):
                        
                        if not (kpfe_common.is_defined_var(all_defs, ebit) | kpfe_common.is_sv_literal(ebit) | kpfe_common.is_int_literal(ebit)):
                            sys.exit("Error. Could not find declaration for "+ebit+" in expression "+curr_expr)
                # replace the term with a marker so we don't check it again
                if "("+extracted_term+")" in curr_expr: 
                    replacee = "("+extracted_term+")"
                else:
                    replacee = extracted_term
                curr_expr = curr_expr.replace(replacee,"$$$marker$$$")
            if curr_expr == "$$$marker$$$":
                break
    return legal_expr
#----------------------------------------------------------            
def check_legal_val_map(all_defs,bi_ops_regex,val_map,allowed_mappings,legal_map_name,map_type): 
    """check the define mapping is allowed"""
    
    map_bits = val_map.split("->")
    if (len(map_bits) != 2):
        sys.exit("Error. Incorrect number of bits found in val_map. In legal_map "+legal_map_name) 
    vmap = ["-","-"]
    for midx, map_bit in enumerate(map_bits):
        listmatch = re.match(r'^\[.*\,.*\]$',map_bit)
        rangematch = re.match(r'^\[.*\:.*\]$',map_bit)  
                        
        if map_bit == "":
            vmap[midx] = "none"
        elif map_bit == "*": 
            vmap[midx] = "any"   
        elif kpfe_common.is_sv_literal(map_bit):
            vmap[midx] = "lit"     
        elif listmatch:
            vmap[midx] = "list"
            # list elements must be SV literals
            extracted, list_vals = extract_pcontents(["[","]"], map_bit)
            elems = list_vals.split(',')
            for elem in elems:
                if not(kpfe_common.is_sv_literal(elem)):
                    sys.exit("Error. list elements in val_map must be SV literals. In legal_map "+legal_map_name) 
        elif rangematch:
            vmap[midx] = "range"
            # range elements must be SV literals
            extracted, list_vals = extract_pcontents(["[","]"], map_bit)
            elems = list_vals.split(':')
            if len(elems) != 2:
                sys.exit("Error. must be 2 range elements in val_map. In legal_map "+legal_map_name)    
            for elem in elems:
                if not(kpfe_common.is_sv_literal(elem)):
                    sys.exit("Error. range elements in val_map must be SV literals. In legal_map "+legal_map_name)   
        else:
            # check if an expression
            if check_legal_expression(all_defs, map_bit, bi_ops_regex):
                vmap[midx] = "expr"
            else:
                sys.exit("Error. Unknown val_map type, "+map_bit+" In legal map "+legal_map_name)    
            
    # check if the mapping is allowed
    this_map = tuple(vmap)
    if not(this_map in allowed_mappings):
        sys.exit("Error. Unsupported "+map_type+" mapping, "+val_map+" In legal map "+legal_map_name)        
         
            
#---------------------------------------------------------- 
def check_enum_syntax(all_defs):
    """check enum definition syntax"""
    
#    enum definitons take the form:
#    {"def_name" : "<name>",
#         "def_type" : "enum",
#         "def" : ["<enum element name 1>",
#                  "<enum element name 2>",
#                  ]},
#
#    when read they are parsed, they are  put into a dict keyed by the name
#    - check the enum name is unique (done when parsed)
#    - check the enum name is not used by any other named structure 
#    - check the enum is defined as a list of strings
#    - check that each element name is unique
#    - check there is at least one element name defined
#    - check the element names are not used by any other structure 
#    if incorrectly structured, json read will fail 

    print("Checking enum syntax...")
    for enum_name in all_defs['enums']:
        already_exists, exist_type = check_exists(all_defs, enum_name)
        if already_exists & (not exist_type == 'enums'):
            sys.exit("Error. Enum "+enum_name+" already exists in def_type "+exist_type)
    
        if not isinstance(all_defs['enums'][enum_name], list):
            sys.exit("Error. Enum must be defined as a list. In Enum "+enum_name)
        
        if len(all_defs['enums'][enum_name]) < 1:
            sys.exit("Error. Enum must be greater than 0. In Enum "+enum_name)
            
        if not(len(set(all_defs['enums'][enum_name])) == len(all_defs['enums'][enum_name])): 
            sys.exit("Error. Enum Elements must be unique. In Enum "+enum_name) 
            
        for elem_name in all_defs['enums'][enum_name]:
            already_exists, exist_type = check_exists(all_defs, elem_name)
            if already_exists:
                sys.exit("Error. Enum Element name already used in "+exist_type)
                
    print("Enum syntax check completed ok.")
    
#---------------------------------------------------------- 
def check_property_syntax(all_defs, bi_ops_regex):
    """check property definition syntax"""
    
#    assertion definitions take the form:
#    {"def_name" : "<name>",
#         "def_type" : "property",
#         "def" : {"type" : <property_type>",
#                  "property" : <property definition>"},
#
#    property definition must be of the form:
#       <expression> or,
#       <expression> |-> <expression>
#    each expression must be a legal Kite-SV expression
#
#
#
#
#    when read they are parsed, they are  put into a dict keyed by the name
#    - check the assertion name is unique (done when parsed)
#    - check the assertion name is not used by any other named structure 
#    - check the assertionis defined as a string
#     - check that the expression is legal
#    if incorrectly structured, json read will fail 
    print("Checking properties syntax...")
    for prop_name in all_defs['properties']:
        already_exists, exist_type = check_exists(all_defs, prop_name)
        if already_exists & (not exist_type == 'properties'):
            sys.exit("Error. property "+prop_name+" already exists in def_type "+exist_type)
    
        if not isinstance(all_defs['properties'][prop_name], dict):
            sys.exit("Error. Property must be defined as a dict. In property "+prop_name)
        if "type" not in all_defs['properties'][prop_name].keys():
            sys.exit("Error. Property must have type defined. In property "+prop_name) 
        if not ((all_defs['properties'][prop_name]['type'] == "assert") |
                (all_defs['properties'][prop_name]['type'] == "assume") |
                (all_defs['properties'][prop_name]['type'] == "cover")):
            sys.exit("Error. Property has illegal type. In property "+prop_name)        
              
        # check the expressions
        prop_bits =  all_defs['properties'][prop_name]['property'].split("|->")
        for prop_bit in prop_bits:
            check_legal_expression(all_defs, prop_bit, bi_ops_regex)   
                
    print("properties syntax check completed ok.")

#---------------------------------------------------------- 
def check_task_syntax(all_defs):
    """check task definition syntax"""
    
#    tssk definitions take the form:
#    {"def_name" : "f_deleg_exc_priv",
#        "def_type" : "task", 
#        "def" : {"task_args" : {"<input arg1>":"<input arg1_dimensions>","<input arg2>":"<input arg2_dimensions>", ..],
#                 "task_return" : {<output arg1>":"<output arg1_dimensions>","<output arg2>":"<output arg2_dimensions>", ..],
#                 "task_body" : "<sv_task_description>"}},   
#
#    sv_task_description is a list of strings describing the task body in SystemVerilog
#
#    when read they are parsed, they are  put into a dict keyed by the name
#    - check the task name is unique (done when parsed)
#    - check the task name is not used by any other named structure 
#    - check there is at least 1 input or output argument described as a dict with the name and dimensions
#       - the dimensions may be defined as a dimensions field "[bound0:bound1]", 
#         or can use $size(<enum/signal/state>) to derive the size
#      Even if no arguments are used, task_args and task_return must still be defined as dicts
#      (empty if no arguments)
#    - check the task body is defined as a list of string
#    if incorrectly structured, json read will fail 
    arg_len = 0
    return_len = 0
    print("Checking task syntax...")
    for task_name in all_defs['tasks']:
        already_exists, exist_type = check_exists(all_defs, task_name)
        if already_exists & (not exist_type == 'tasks'):
            sys.exit("Error. task "+task_name+" already exists in def_type "+exist_type)
        if('task_args' in all_defs['tasks'][task_name]):  
            if not isinstance(all_defs['tasks'][task_name]['task_args'], dict):
                sys.exit("Error. task input args  must be defined as a dict. In task "+task_name)
            for arg in all_defs['tasks'][task_name]['task_args'].keys():
                if not isinstance(all_defs['tasks'][task_name]['task_args'][arg], str):
                    sys.exit("Error. task arg type in the dict must be defined as a string. Arg = "+str(arg)+" In task "+task_name)
                # the type can be logic (with or without array dimensions), integer, or an enum
                check_task_type(all_defs, all_defs['tasks'][task_name]['task_args'][arg], task_name)
            arg_len = len(all_defs['tasks'][task_name]['task_args'].keys())
        
        if('task_return' in all_defs['tasks'][task_name]): 
            if not isinstance(all_defs['tasks'][task_name]['task_return'], dict):
                sys.exit("Error. task returns must be defined as a dict. In task "+task_name)
            for arg in all_defs['tasks'][task_name]['task_return'].keys():
                if not isinstance(all_defs['tasks'][task_name]['task_return'][arg], str):
                    sys.exit("Error. task return dimensions in the dict must be defined as a string. Arg = "+str(arg)+" In task "+task_name)
                # the type can be logic (with or without array dimensions), integer, or an enum
                check_task_type(all_defs, all_defs['tasks'][task_name]['task_return'][arg], task_name)
            return_len = len(all_defs['tasks'][task_name]['task_return'].keys())
                    
        if(arg_len + return_len) == 0:
            sys.exit("Error. task must have at least one input or output. In task "+task_name)      
                
    print("Task syntax check completed ok.")        
    
#---------------------------------------------------------- 
def check_legal_maps_syntax(all_defs, bi_ops_regex):
    """check state legal map definition syntax"""
    
#    legal-map definitions take the form:
#       {"def_name" : "<legal_map_name>",
#        "def_type" : "state_legal_map", 
#        "def" : {"wr_map" : [{"val_map" : "<attempted write value>-><resulting write value>", 
#                              "condition" : "<write mapping condition>"},
#                             ...],
#                 "rd_map" : [{"val_map" : "<state value>-><resulting read value>", 
#                              "condition" : "read mapping condition"}
#                             ...]}
#       },   
#
#    -Mappings (val_map field) may contain the following values with "->" indicating the mapping operation
#           - x means a single literal value (x must be a SystemVerilog typed literal) 
#           - expr means a legal Kite-SV expression
#           - * means any value
#           - "" (no character) means "maps to/from nothing"
#           - [x:y] means a range from x to y (x and y must be SystemVerilog typed literals)
#                   both parts of the range must be defined
#           - [x,y,z] means a list of literals (x,y and z must be SystemVerilog typed literals)
#                     lists cannot be empty
#
#    -Write mapping string
#    - Write mapping is described as one or more clauses to describe how attempted write 
#      values map to the resulting write value
#      The attepted->resulting values are strings considered as pairs with various legal combinations:
#           - *->* means the state value get the attempted write value
#           - {x,*,[x:y],[x,y,z]}->"" means the specified literal, range, list or any does not result in 
#                                        any value writen (ie read-only)
#           - x->y means an attempted write of the value x results in y
#           - *->y means an attempted write of any value results in y
#           - [x:y]->z means an attempted write of any value in the range from x to y results in z
#           - x->expr(x) means an attempted write of the value x results in the current value of expr where the write value replaces x in the exprssion term
#           - *->expr(*) means an attempted write of any value results in the current value of expr where the write value replaces * in the exprssion term
#                        e.g *->* & mask  performs a bitwise and of the write value and mask 
#           - [x:y]->expr means an attempted write of any value in the range from x to y results in the 
#                        current value of expr
#           - [x,y,z]->expr means an attempted write of any value in the list x,y,z results in the
#                          current value of expr
#           - [x:y]->* means an attempted write of any value in the range from x to y results in the 
#                      corresponding value written, values outside the range result in no write
#           - [x,y,z]->a means an attempted write of any value in the list x,y,z results in a
#           - [x,y,z]->[a,b,c] means an attempted write of x results in a, y results in b, z results in c. 
#                              the number of elements in each list must match            
#           - [x,y,z]->* means an attempted write of any value in the list x,y,z results in the 
#                       corresponding value written, values outside the list result in no write          
#      No other combinations are legal write mapping definitions
#       REVISIT- add multiple ranges in  list?
#      The condition defines when the clause is effective. This must be defined as a Kite-SV expression in a string
#
#    -Read mapping string
#    - Read mapping is described as one or more clauses to describe how state values 
#      map to the resulting read value
#           - *->* means the resulting read value is the value in the state
#           - x->y means if the value in the state is x, the resulting read value is y
#           - *->y means any value in the state results in the read value y
#           - *->expr(*) means any value in the state results in the read value being the current value of expr with * replaced by 
#                        the read value in the expression term. e.g *->* & mask  performs a bitwise and of the read value and mask 
#           - [x:y]->z means if the value in the state is in the range defined by [x:y], 
#                      the resulting read value is z
#           - [x,y,z]->z means if the value in the state is in the list defined by [x,y,z], 
#                        the resulting read value is z
#           - [x,y,z]->[a,b,c] means if the value in the state is in the list defined by [x,y,z], 
#                              the corresponding value in [a,b,c] will be read
#      No other combinations are legal read mapping definitions
#      The condition defines when the clause is effective. This must be defined as a Kite-SV expression in a string
#
#    if incorrectly structured, json read will fail 
    
    # REVIST some modes not yet supported by the generator
#    allowed_wr_maps = [("lit","none"), ("any","none"), ("range","none"), ("list","none"),
#                       ("lit","lit"),  ("any","lit"),  ("range","lit"),  ("list","lit"),
#                       ("lit","expr"), ("any","expr"), ("range","expr"), ("list","expr"),
#                                       ("any","any"),  ("range","any"),  ("list","any"),
#                                                                       ("list","list")]                                                                      
    allowed_wr_maps = [("lit","none"), ("any","none"),
                       ("lit","lit"),  ("any","lit"),  
                                       ("any","expr"),  
                                       ("any","any")]
                                     
   # REVIST some modes not yet supported by the generator                                                                    
#    allowed_rd_maps = [("lit","lit"), ("any","lit"), ("any","expr"), ("any","any "), ("list","lit"), ("list","list"), ("range","lit")] 
                                     
    allowed_rd_maps = [("lit","lit"),  ("any","lit"), ("any","expr"), ("any","any")]                                                         

    print("Checking legal-map syntax...")
    for legal_map_name in all_defs['legal_maps'].keys():
        already_exists, exist_type = check_exists(all_defs, legal_map_name)
        if already_exists & (not exist_type == 'legal_maps'):
            sys.exit("Error. legal_map "+legal_map_name+" already exists in def_type "+exist_type)
            
        # legal map must have a wr_map field which is list of mapping clauses (dicts) and 
        # must have a rd_map field which is list of mapping clauses (dicts) 
        map_types = {"wr_map": allowed_wr_maps, "rd_map": allowed_rd_maps}
        
        for mtype in map_types.keys():
            if not(mtype in all_defs['legal_maps'][legal_map_name]):
                sys.exit("Legal map must have a "+mtype+". In legal_map "+legal_map_name)        
            if not isinstance(all_defs['legal_maps'][legal_map_name][mtype], list):
                sys.exit("Error. legal_map "+mtype+"  must be defined as a list. In legal_map "+legal_map_name)
            if not (len(all_defs['legal_maps'][legal_map_name][mtype]) > 0):
                sys.exit("Error. legal_map "+mtype+" must have 1 or more clauses. In legal_map "+legal_map_name)
            for map_clause in all_defs['legal_maps'][legal_map_name][mtype]:
                if not isinstance(map_clause, dict):
                    sys.exit("Error. legal_map "+mtype+" clause must be defined as a dict. In legal_map "+legal_map_name)   
                if (not('val_map' in map_clause.keys())) | (not('condition' in  map_clause.keys())):   
                    sys.exit("Error. legal_map "+mtype+" clause must have val_map and condition fields. In legal_map "+legal_map_name)
                if not isinstance(map_clause['val_map'], str):
                    sys.exit("Error. legal_map "+mtype+" val_map must be defined as a string. In legal_map "+legal_map_name) 
                if not isinstance(map_clause['condition'], str):
                    sys.exit("Error. legal_map "+mtype+" condition must be defined as a string. In legal_map "+legal_map_name) 
                # check the val_map is legal
                check_legal_val_map(all_defs,bi_ops_regex,map_clause['val_map'],map_types[mtype],legal_map_name,mtype) 
                # check the condition expression is legal
                check_legal_expression(all_defs, map_clause['condition'], bi_ops_regex)              
                
    print("Legal-map syntax check completed ok.")  
    
#---------------------------------------------------------- 
def check_globals_syntax(all_defs, bi_ops_regex):
    """check global signal definition syntax"""
    
#    global signal definitions take the form:
#    {"def_name" : "<global signal name>",
#     "def_type" : "global_signal",
#     "def" :  {"dimensions" : "[<bound0>:<bound1>]", 
#               "type" : "<global type>", 
#               "default" : "<default value>", 
#               "assignment" : [{"update_to_value" : "<assignment value expression>",
#                                "condition" : "<assignment condition expression>"},
#                               ...]}},
#    
#    
#   - "dimensions" is a string to define the global signal width
#     always includes <bound0>:<bound1> even for 1 bit signals, 
#     <bound0>, <bound1> must be positive integers or evaluate to positive integers    
#
#   - "type" This string defines the type of global signal to direct IR generation. It can be:
#       - "state_assignment". General global signal for use and assignment in the model
#                             can be assigned by instructions or in the global assignment
#       - "primary_input".    Indicates this will not be driven by the model, but by an external driver
#       - "primary_output".   Indicates this will be used as a primary output. In all other ways
#                             behaves as "state_assignment"
#       - "nd_var"            Non-deterministic signal. Indicates it will not be driven by assignments in 
#                             the specification    
#       - "nd_const"          Non-deterministic signal which should take a stable constant value.  
#                             Indicates it will not be driven by assignments in the specification
#       - "debug_assignment"  Indicates that this signal is used only for specification or back-end model 
#                             debug
#     No other types are legal
#    
#   - "default" This string defines the value that the signal will take if not assigned
#               Can be defined as an empty string ("") but must always be defined  
#    
#   - "assignment" This describes optional conditional updates to the global signal
#                  The assignment is defined as a list of conditional updates
#                  The assignments field must always be present, but the list can be empty
#    
#    
#    if incorrectly structured, json read will fail 

    allowed_types = ["state_assignment", "primary_input", "primary_output", "nd_var", "nd_const", "debug_assignment"]
    
    print("Checking global signal syntax...")
    for global_name in all_defs['global_sigs'].keys():
        print("Checking global signal "+global_name)
        curr_glob = all_defs['global_sigs'][global_name]
        if not('dimensions' in curr_glob):
            sys.exit("Error. No dimensions found. In global signal "+global_name) 
        check_dimensions(curr_glob['dimensions'], global_name)
        if not('type' in curr_glob):
            sys.exit("Error. No type found. In global signal "+global_name)
        if not(curr_glob['type'] in  allowed_types):
            sys.exit("Error. Global type "+curr_glob['type']+" is not supported. In global signal "+global_name)   
        if not('default' in curr_glob):
            sys.exit("Error. No default found. In global signal "+global_name)
        # check the default expression is legal
        check_legal_expression(all_defs, curr_glob['default'], bi_ops_regex) 
        if not('assignment' in curr_glob):
            sys.exit("Error. No assignment found. In global signal "+global_name)
        if not isinstance( curr_glob['assignment'], list):
            sys.exit("Error. Assignment must be defined as a list. In global signal "+global_name)
        for clause in curr_glob['assignment']:
            if not('update_to_value' in clause):
                sys.exit("Error. Global assignment clause must include update_to_value. In global signal "+global_name) 
            check_legal_expression(all_defs, clause['update_to_value'], bi_ops_regex)   
            if not('update_to_value' in clause):
                sys.exit("Error. Global assignment clause must include condition. In global signal "+global_name) 
            check_legal_expression(all_defs, clause['condition'], bi_ops_regex)      
            
                
    print("Global signal syntax check completed ok.")   
    
#---------------------------------------------------------- 
def check_states_syntax(all_defs, bi_ops_regex):
    """check states definition syntax"""
    
#    states definitions take the form:
   
#    
# "<state_name>" : {
#           "views" : {"<view_name>": {"dimensions" : "[<bound0>:<bound1>]",
#                                      "array_aliases" : [{"alias" : "<alias_name", "index": "<alias index>"}],
#                                      "organization" : [{"condition" : "<organization condition expression>",
#                                                        "dimensions" : "[<bound0>:<bound1>]",
#                                                        "bit_map" : [{"field" : "<subfield name>", 
#                                                                      "position" : "[<bit position max>:<bit position min>]", 
#                                                                      "value" : "<value expression for this position>"},
#                                                         ...]}]},
#                     ...
#                     },                                                            
#            "subfields" : {
#               "<subfield name>" : {"dimensions" : "[<bound0>:<bound1>]",
#                                    "legal_value_map" : "<legal value map name>", 
#                                    "reset_value" : "<reset value>"}
#           },
#           ...
#       }, 
#    
#    note, these are the standard supported fields, architecture specific fields may be added
#    - There must be at least 1 view
#       - a view must have dimensions and organization fields, array_aliases is optional
#           - dimensions must be a string of the form [<bound0>:<bound1>]
#           - if present, for the array alias description:
#               -the alias field must exist and must refer to a defined alias  
#               -the index must resolve to a positive integer (though can be expressed as decimal, or hexadecimal, or SV literal)
#               -the index must be in range of the defined alias index bounds
#               -the state (view) dimensions must match the alias sub-dimensions
#           - organizations must have at least one organization description
#               - the organization description must contain
#                   - a condition expression
#                   - the dimensions
#                   - the bit map to describe the mapping of subfields into bit positions, there 
#                     must be at least one bit map containing:
#                       - the field (must be a defined subfield in this state). Undefined fields must be "-"
#                       - the bit position in the form [<bit position max>:<bit position min>]
#                       - the value expression that is assigned to this bitfield            
#    if incorrectly structured, json read will fail 

    print("Checking state definition syntax...")
    for state_name in all_defs['states']:
        print("Checking state definition for "+state_name)
        curr_state = all_defs['states'][state_name]
        if not('views' in curr_state):
            sys.exit("Error. No view field found. In state definition "+state_name)
        if not (len(curr_state['views']) > 0):
            sys.exit("Error. At least one view must be defined. In state definition "+state_name)
        for view_name in curr_state['views'].keys():  
            curr_view = curr_state['views'][view_name]  
            if not('dimensions' in curr_view):
                sys.exit("Error. No state view dimensions found. In state definition "+state_name) 
            check_dimensions(curr_view['dimensions'], view_name)
            dmin, dmax = kpfe_common.extract_field_min_max_idx(curr_view['dimensions'])
            if ('array_aliases' in curr_view):
                for curr_alias in curr_view['array_aliases']:
                    #curr_alias = curr_view['array_aliases'][alias_name]
                    if not('alias' in curr_alias):
                        sys.exit("Error. No alias field found in array alias description. In state definition "+state_name)
                    alias_name = curr_alias['alias']
                    if not(alias_name in all_defs['state_aliases']):
                        sys.exit("Error. Array alias definition "+alias_name+" does not exist. In state definition "+state_name) 
                    if not('index' in curr_alias):
                        sys.exit("Error. No index field found in array alias description. In state definition "+state_name)
                    is_integer, lval = kpfe_common.convert_to_int(curr_alias['index'])
                    if (not is_integer) | (not lval >= 0):
                        sys.exit("Error. Index must be a positive integer (or resolve to one). In state definition "+state_name)  
                    rmin, rmax = kpfe_common.extract_field_min_max_idx(all_defs['state_aliases'][alias_name]['dimensions'])
                    if (lval > int(rmax)) | (lval < int(rmin)):
                        sys.exit("Error. Alias Index for "+alias_name+" out of range. In state definition "+state_name) 
                    # check sizes match
                    sdmin, sdmax = kpfe_common.extract_field_min_max_idx(all_defs['state_aliases'][alias_name]['sub_dimensions'])
                    if(dmin != sdmin) | (dmax != sdmax):
                        sys.exit("Error. Alias dimensions for "+alias_name+" do not match definition subdimensions. In state definition "+state_name) 
            if not('organization' in curr_view):
                sys.exit("Error. No organization field found. In state definition "+state_name) 
            if not (len(curr_view['organization']) > 0):
                sys.exit("Error. At least one organization must be defined. In state definition "+state_name)   
            for curr_org in curr_view['organization']: 
                if not('condition' in curr_org):
                    sys.exit("Error. No organization condition found. In state definition "+state_name) 
                check_legal_expression(all_defs, curr_org['condition'], bi_ops_regex)
                if not('dimensions' in curr_org):
                    sys.exit("Error. No organization dimensions found. In state definition "+state_name) 
                check_dimensions(curr_org['dimensions'], state_name)
                if not('bit_map' in curr_org):
                    sys.exit("Error. No organization bit_map found. In state definition "+state_name)
                if not (len(curr_org['bit_map']) > 0):
                    sys.exit("Error. At least one organization bit map must be defined. In state definition "+state_name)  
                for curr_bm in curr_org['bit_map']:  
                    if not('field' in curr_bm):
                        sys.exit("Error. No organization bitmap subfield found. In state definition "+state_name) 
                    if not((curr_bm['field'] in curr_state['subfields'].keys()) | (curr_bm['field'] == "-")) :
                        # allowed to use subfields from other states, but that subfield must specify curr_state in its add_state_access field
                        # search the other states for this subfield (defined by curr_bm['field']) and if found, check add_state_access
                        found_add_state = False
                        for chk_state in all_defs['states'].keys():
                            if curr_bm['field'] in all_defs['states'][chk_state]['subfields'].keys():
                                if "add_state_access" in all_defs['states'][chk_state]['subfields'][curr_bm['field']]:
                                    if state_name in all_defs['states'][chk_state]['subfields'][curr_bm['field']]['add_state_access']:
                                        found_add_state = True
                                        print("found additional state access mapping for "+curr_bm['field']+" in state "+chk_state)
                        if not found_add_state:
                            sys.exit("Error. unmatched subfield "+curr_bm['field']+" in bit map field must describe a subfield in this state. In state definition "+state_name)
                    if not('position' in curr_bm):
                        sys.exit("Error. No organization bitmap position found. In state definition "+state_name)     
                    check_dimensions(curr_bm['position'], state_name)
                    # REVISIT add check that position is in range and not overlapping, and dimensions match subfield definition
                    if not('value' in curr_bm):
                        sys.exit("Error. No organization bitmap value found. In state definition "+state_name)
                    check_legal_expression(all_defs, curr_bm['value'], bi_ops_regex)
        if not('subfields' in curr_state):
            sys.exit("Error. No subfields field found. In state definition "+state_name)
        if not (len(curr_state['subfields']) > 0):
            sys.exit("Error. At least one subfield must be defined. In state definition "+state_name)
        for subfield_name in curr_state['subfields'].keys():  
            curr_subfield = curr_state['subfields'][subfield_name]
            if not('dimensions' in curr_subfield):
                sys.exit("Error. No subfield "+subfield_name+" dimensions found. In state definition "+state_name) 
            check_dimensions(curr_subfield['dimensions'], state_name)     
            if not('legal_value_map' in curr_subfield):
                sys.exit("Error. No subfield "+subfield_name+" legal_value_map found. In state definition "+state_name)  
            if not(curr_subfield['legal_value_map'] in all_defs['legal_maps'].keys()):
                sys.exit("Error. No subfield "+subfield_name+" legal_value_map "+curr_subfield['legal_value_map']+" defined. In state definition "+state_name) 
            if not('reset_value' in curr_subfield):
                sys.exit("Error. No subfield "+subfield_name+" reset_value field found. In state definition "+state_name)               
            if curr_subfield['reset_value'] != "": 
                check_legal_expression(all_defs, curr_subfield['reset_value'], bi_ops_regex)         
                    
    print("State syntax check completed ok.")   
    
#---------------------------------------------------------- 
def check_instruction_syntax(all_defs, bi_ops_regex):
    """check instruction definition syntax"""
    
#    instruction definitions take the form:
#"<instruction name>" : {
#            "mnemonic" : "<instruction menemonic>",
#            "encoding" : "<instruction encoding expression>",
#            "match" : [
#                {"condition" : "<match condtion expression>",
#                 "state_action_list": [
#                    {"state_to_update" : "<match update state/signal name>", 
#                     "update_to_value" : "<match update state/signal value expression>", 
#                     "condition" : "<match update state/signal condition expression>"},
#                      ...]}],                          
#            "exceptions" : [
#                {"exception_type": "<exception type>"
#                 "condition" : "<exception action condtion>",
#                 "state_action_list": [
#                    {"state_to_update" : "<exception update state/signal name>", 
#                     "update_to_value" : "<exception update state/signal value expression>", 
#                     "condition" : "<exception update state/signal condition expression>"},
#                      ...]}],,
#            "retire" : [
#                {"condition" : "<retire action condtion>",
#                 "state_action_list": [
#                    {"state_to_update" : "<retire update state/signal name>", 
#                     "update_to_value" : "<retire update state/signal value expression>", 
#                     "condition" : "<retire update state/signal condition expression>"},
#                      ...]}]
#        },
#
#
#   - instuction name string must describe a unique name (will fail in the input parsing if not)
#       - instruction mnemonic string must describe a unique mnemonic
#       - instruction encoding string describes encoding. Field are matched and extarcted from the encoding.
#         The encoding lenght must match the instruction length, except for special cases where the encoding is defined as "-"
#         bits may be x or wildcards (?)
#       - match. All instructions must have a match section. This describes the condition for this instruction to execute.
#                match is described as a dict containing the match condition and state action list 
#           - condition. This expression string describes the condition that must be true for this instruction to execute
#           - state action list. List of updates defines what states/signals are updated on a match. 
#             Signals get updated combinationally, states sequentially
#               - state to update. String to indicate which state or signal are updated 
#               - update_to_value. An expression to define what value the signal or state is updated to.        
#               - condition. An expression to indicate the condition for the update to occur
#       - exception. Optional section to define the behaviour of exceptions when the match condition is met
#           - Exception type. Exception description, this must be defined as part of an enumerated type (REVISIT not currently used)
#           - Condition and state update fields as match section
#       - retire. Optional section to define the behaviour of an instruction when it retires
#           - Condition and state update fields as match section
#
#    if incorrectly structured, json read will fail 
    print("Checking instruction syntax...")
    for instruction_name in all_defs['instructions'].keys():
        curr_instr = all_defs['instructions'][instruction_name]
        print("Checking instruction definition for "+instruction_name)
        if not('mnemonic' in curr_instr):
            sys.exit("Error. No mnemonic field found. In instruction definition "+instruction_name)
        for icheck_name in all_defs['instructions'].keys():
            curr_instr_check = all_defs['instructions'][icheck_name]
            if(icheck_name != instruction_name) & (curr_instr['mnemonic'] == curr_instr_check['mnemonic']):
                sys.exit("Error. Duplicate mnemonic found. In instruction definition "+instruction_name)  
        if not('encoding' in curr_instr):
            sys.exit("Error. No encoding field found. In instruction definition "+instruction_name)
        if curr_instr['encoding'] != "-": 
            # check that the length of the encoding field matches the defined instruction length
            if not('instruction' in all_defs['global_sigs']):  
                sys.exit("Error. Could not find global instruction definition. In instruction definition "+instruction_name)
            ilen = kpfe_common.extract_field_len(all_defs['global_sigs']['instruction']['dimensions'])
            found_enc_len, enc_len = kpfe_common.find_operand_width(all_defs, curr_instr['encoding'])
            if not found_enc_len:
                sys.exit("Error. Could not extract length from encoding "+curr_instr['encoding']+" In instruction definition "+instruction_name)  
            else:
                if(ilen != enc_len):
                    sys.exit("Error. Instruction field length is "+str(ilen)+" encoding length is "+str(enc_len)+" In instruction definition "+instruction_name)
        if not('match' in curr_instr):
            sys.exit("Error. No match field found. In instruction definition "+instruction_name) 
        check_conditional_update(all_defs, bi_ops_regex, curr_instr['match'], instruction_name, 'match')
        if('exception' in curr_instr):
            check_conditional_update(all_defs, bi_ops_regex, curr_instr['exception'], instruction_name, 'exception')
        if('retire' in curr_instr):
            check_conditional_update(all_defs, bi_ops_regex, curr_instr['retire'], instruction_name, 'retire')
            
   
    print("Instruction syntax check completed ok.")

#----------------------------------------------------------    
def syntax_check(all_defs, bi_ops_regex):
    """performs initial syntax checking on parsed spec"""
    
    # Kite spec is not context-free so check each part 
    # seperately as it is structured differently.
    
    #include signed and unsigned as built in operators for syntax checking  
    bi_ops_regex = bi_ops_regex+"|\$signed|\$unsigned" 
    #bi_ops_regex = bi_ops_regex
    
    #check_enum_syntax(all_defs)
    check_property_syntax(all_defs, bi_ops_regex)  
    # aliases already replaced 
    check_task_syntax(all_defs)
    # macros already replaced
    check_legal_maps_syntax(all_defs, bi_ops_regex)
    check_globals_syntax(all_defs, bi_ops_regex)
    # REVISIT memories - need to update with PMA
    check_states_syntax(all_defs, bi_ops_regex)
    # REVISIT fix performance issue with concatenations in instruction expression checks
    #check_instruction_syntax(all_defs, bi_ops_regex)

#---------------------------------------------------------- 
#---------------------------------------------------------- 
