#
# Copyright 2020  Stuart Hoad, Lesley Shannon
# Reconfigurable Computing Lab, Simon Fraser University.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):
#             Stuart Hoad <stuart_hoad@sfu.ca>
#
# kpfe_common.py
# Kite Parser common functions

import re
import sys
import os
import copy
import json5
import datetime


#----------------------------------------------------------
#----------------------------------------------------------

#----------------------------------------------------------
def convert_to_int(val):
    """convert the provided string field to an integer (if possible)"""
    
    is_int = False
    value = -1
    logic_match = re.match(r'(\d+)\'([d|h|b])([\d|a-f|A-F]+)',val)
    hex_match = re.match(r'0x([\d|a-f|A-F]+)',val)
    dec_match = re.match(r'(\d+)',val)
    if logic_match:
        length = int(logic_match.group(1))
        dtype = logic_match.group(2)
        # if the logic vector is not binary, convert to binary first
        if(dtype == "b"):
            value = int(logic_match.group(3),2)
            is_int = True
        elif(dtype == "d"):
            value = int(logic_match.group(3))
            is_int = True
        elif(dtype == "h"):    
            value = int(logic_match.group(3), 16)
            is_int = True
    elif hex_match:
        value = int(hex_match.group(1), 16)
        is_int = True
    elif dec_match:
        value = int(dec_match.group(1))
        is_int = True
    
    return is_int, value
    
def is_int_literal(val):
    """check if the string as an integer literal"""
    is_literal = False
    limatch = re.match(r'^(\d+)$',val)
    if limatch:
        is_literal = True
            
    return is_literal
    
#----------------------------------------------------------
def is_sv_literal(val):
    """ Check if the string is an SV literal """
    is_literal = True
    # could be a concatenation, deal with this first (recursively)
    found_concat, concat_inards = extract_pcontents(["{","}"], val)
    if found_concat:
        extracted, concat_bits, start = extract_pcontent_fields(["{","}"], ",", val, 0)
        for cbit in concat_bits:
            if not is_sv_literal(cbit):
                is_literal = False      
    else:  
        # treat true as 1'b1 and false as 1'b0
        if val == "true":
            val = "1'b1"
        if val == "false":
            val = "1'b0"
        is_literal = False  
        litmatch = re.match(r'(\d+)\'([d|h|b])([\d|x|X|a-f|A-F|\?]+)',val)    
        if litmatch:
            is_literal = True
            
    return is_literal  
    
#----------------------------------------------------------
def is_defined_var(all_defs, val):
    """ check if the string isa defined variable """
    
    is_var = True
    # could be a concatenation, deal with this first (recursively)
    found_concat, concat_inards = extract_pcontents(["{","}"], val)
    if found_concat:
        extracted, concat_bits, start = extract_pcontent_fields(["{","}"], ",", val, 0)
        for cbit in concat_bits:
            if not is_defined_var(all_defs, cbit):                is_var = False      
    else:   
        # remove slice range if present 
        val = re.sub(r'\[.+\]$','',val)    
        is_var = False
        for state in all_defs['states']:
            if val in all_defs['states'][state]['views'].keys():
                is_var = True
            if val in all_defs['states'][state]['subfields'].keys():
                is_var = True
        if val in all_defs['state_aliases'].keys():
            is_var = True
        if val in all_defs['global_sigs'].keys():
            is_var = True
        if val in all_defs['local_sigs'].keys():
            is_var = True
        if val in all_defs['memories'].keys():
            is_var = True  
        for enum in all_defs['enums']:
            if val in all_defs['enums'][enum]:
                is_var = True    
    return is_var   
    
#----------------------------------------------------------
def remove_continuations(infile):
    """ Remove JSON continuations """
    file_array = []
    file_array.extend(infile.readlines())
    fixed_file_string = re.sub(r'\\\n\s*',' ',' '.join(file_array))
    return fixed_file_string
    
#----------------------------------------------------------    
def get_width_str(width):
    """return the width string (dimensions) for the specified integer"""
    if width == 1:
        width_str = "[0:0]"
    else:
        width_str = "["+str(width-1)+":0]"
    return width_str    

#----------------------------------------------------------
def extract_pcontents(enclosing_char_list, pstring):
    """extract the characters in pstring between the matching parenthesis defined in enclosing_char_list"""
    plevel = 0
    found_closing = 0
    found_opening = 0
    idx = 0
    opening_pos = 0
    closing_pos = 0
    
    while (found_closing == 0) & (idx != len(pstring)):         
        if(pstring[idx] == enclosing_char_list[0]):
            if(plevel == 0):
               found_opening = 1
               opening_pos = idx
            plevel = plevel + 1
            
        elif (pstring[idx] == enclosing_char_list[1]):
            plevel = plevel - 1
            if(plevel < 0):
                sys.exit("unmatched closing "+enclosing_char_list[1]+" in term : "+pstring)
            if(plevel == 0):
               found_closing = 1
               closing_pos = idx
                           
        idx = idx + 1
    
    if ((found_opening == 1) & (found_closing == 1)):
        return_string = pstring[opening_pos+1:closing_pos]
    else:
        return_string = pstring
        
    return ((found_opening == 1) & (found_closing == 1)), return_string

#----------------------------------------------------------
def extract_pcontents_pos(enclosing_char_list, pstring):
    """extract the characters in pstring between the matching parenthesis defined in enclosing_char_list"""
    plevel = 0
    found_closing = 0
    found_opening = 0
    idx = 0
    opening_pos = 0
    closing_pos = 0
    
    while (found_closing == 0) & (idx != len(pstring)):
         
        if(pstring[idx] == enclosing_char_list[0]):
            if(plevel == 0):
               found_opening = 1
               opening_pos = idx
            plevel = plevel + 1
            
        elif (pstring[idx] == enclosing_char_list[1]):
            plevel = plevel - 1
            if(plevel < 0):
                sys.exit("unmatched closing "+enclosing_char_list[1]+" in term : "+pstring)
            if(plevel == 0):
               found_closing = 1
               closing_pos = idx
                           
        idx = idx + 1
    
    if ((found_opening == 1) & (found_closing == 1)):
        return_string = pstring[opening_pos+1:closing_pos]
    else:
        return_string = pstring
        
    return ((found_opening == 1) & (found_closing == 1)), return_string, opening_pos,closing_pos
    
#----------------------------------------------------------
def extract_pcontents_pos_start(enclosing_char_list, pstring, start_pos):
    """extract the characters in pstring between the matching parenthesis defined in enclosing_char_list"""
    plevel = 0
    found_closing = 0
    found_opening = 0
    idx = 0
    opening_pos = 0
    closing_pos = 0
    fpstring = pstring[start_pos:]
    while (found_closing == 0) & (idx != len(fpstring)):
         
        if(fpstring[idx] == enclosing_char_list[0]):
            if(plevel == 0):
               found_opening = 1
               opening_pos = idx+start_pos
            plevel = plevel + 1
            
        elif (fpstring[idx] == enclosing_char_list[1]):
            plevel = plevel - 1
            if(plevel < 0):
                sys.exit("unmatched closing "+enclosing_char_list[1]+" in term : "+fpstring)
            if(plevel == 0):
               found_closing = 1
               closing_pos = idx+start_pos
                           
        idx = idx + 1
    
    if ((found_opening == 1) & (found_closing == 1)):
        return_string = pstring[opening_pos+1:closing_pos]
    else:
        return_string = pstring
        
    return ((found_opening == 1) & (found_closing == 1)), return_string, opening_pos,closing_pos
    
#----------------------------------------------------------
def extract_pcontent_fields(enclosing_char_list, field_delimiter, pstring, start_pos):
    """extract the comma delimted fields in pstring between the matching parenthesis defined in enclosing_char_list"""
    
    plevel = 0
    cplevel = 0
    splevel = 0
    found_closing = 0
    found_opening = 0
    idx = start_pos
    idx_offset = 0
    arg_delimiter_list = [0]
    arg_assign_delimiter_list = []
    
    fpstring = pstring[start_pos:]
    dfpstring = fpstring
    start_pos = 0
    p_extracted = False
    if len(enclosing_char_list) > 0:       
        p_extracted, extr_fpstring, start, end = extract_pcontents_pos(enclosing_char_list, fpstring)
        if p_extracted:
            dfpstring = extr_fpstring
            start_pos = start
    else:
        p_extracted = True   
        
    # find argument delimiters - note function arguments may themselves contain delimeters, so 
    # only apply outside of function calls
    
    while (idx != len(dfpstring)):
         
        if(dfpstring[idx] == "("):
            plevel = plevel + 1
            
        elif (dfpstring[idx] == ")"):
            plevel = plevel - 1
            if(plevel < 0):
                sys.exit("unmatched ) in delimiter search in string"+dfpstring)
                
        if(dfpstring[idx] == "{"):
            cplevel = cplevel + 1
            
        elif (dfpstring[idx] == "}"):
            cplevel = cplevel - 1
            if(cplevel < 0):
                sys.exit("unmatched } in delimiter search in string"+dfpstring)
                
        if(dfpstring[idx] == "["):
            splevel = splevel + 1
            
        elif (dfpstring[idx] == "]"):
            splevel = splevel - 1
            if(splevel < 0):
                sys.exit("unmatched ] in delimiter search in string"+dfpstring)
                
                
        if((dfpstring[idx] == field_delimiter) & (plevel == 0) & (cplevel == 0) & (splevel == 0)):
            arg_delimiter_list.append(idx)
            
        idx = idx + 1
    
    arg_list = [dfpstring[i:j] for i,j in zip(arg_delimiter_list, arg_delimiter_list[1:]+[None])]
    
    for a_idx, arg in enumerate(arg_list):
        arg_list[a_idx] = arg.lstrip(field_delimiter)          
    
    # return arguments (without enclosing parenthesis) opening position and closing position
    
    return p_extracted, arg_list, start_pos    
    
#----------------------------------------------------------
def extract_field_len(dim_def):
    """ exctract the field length from a dimension string """
    d_extracted, dims = extract_pcontents(["[","]"], dim_def)
    dim_bits = dims.split(":")
    if(len(dim_bits) ==2):
        if(dim_bits[0] >= dim_bits[1]):
            field_length = eval("("+dim_bits[0]+"-"+dim_bits[1]+"+1)")
        else:
            field_length = eval("("+dim_bits[1]+"-"+dim_bits[0]+"+1)")
    else:
        sys.exit("could not extract field length")
    return field_length

#----------------------------------------------------------
def extract_field_min_max_idx(dim_def):
    """extract the max and min index from a dimension string """
    
    d_extracted, dims = extract_pcontents(["[","]"], dim_def)
    dim_bits = dims.split(":")
    if(len(dim_bits) ==2):
        if(dim_bits[0] >= dim_bits[1]):
            min_idx = dim_bits[1]
            max_idx = dim_bits[0]
        else:
            min_idx = dim_bits[0]
            max_idx = dim_bits[1]
    else:
        sys.exit("could not extract field max/min")
    return min_idx, max_idx
    
#----------------------------------------------------------
def extract_field_min_max_idx_int(dim_def):
    """extract the max and min index from a dimension string """
    
    # REVISIT modify above with try-except and deprecate this
    
    dim_bits_int =[0,0]
    d_extracted, dims = extract_pcontents(["[","]"], dim_def)
    dim_bits = dims.split(":")
    if(len(dim_bits) ==2):
        dim_bits_int[0] = eval(dim_bits[0]) 
        dim_bits_int[1] = eval(dim_bits[1]) 
        if(dim_bits_int[0] >= dim_bits_int[1]):
            min_idx = dim_bits_int[1]
            max_idx = dim_bits_int[0]
        else:
            min_idx = dim_bits_int[0]
            max_idx = dim_bits_int[1]
    else:
        sys.exit("could not extract field max/min")
    return min_idx, max_idx            
     
    
#--------------------------------------------------------------    
def find_task(all_defs, tstring):
    """search for any tasks in tstring"""
    
    tlist = []
    
    task_regex = '|'.join(all_defs['tasks'].keys())
#    print("task regex is "+task_regex)
#    tlist = re.findall(r'(?<![a-zA-Z0-9_])'+task_regex+'(?![a-zA-Z0-9_])',tstring)
    for task in all_defs['tasks'].keys():
        tmatch = re.search(r'(?<![a-zA-Z0-9_])'+task+'(?![a-zA-Z0-9_])',tstring)
        if tmatch:
            tlist.append(task)
##        for match in re.finditer(task,tstring):
##            tlist.append(match.group())
    return tlist
      
#--------------------------------------------------------------
def extract_inner_term(all_defs, term, bi_ops_regex):
    """find inner most term related to a task """

    task_name =""
    task_name_list = []
    
    # check if the term is a task
    #   -if so, still attempt to extract contents, but if the extracted contents do not contain any 
    #    further inner terms, return the original term and task name
    
    extracted, extracted_term, start, end = extract_pcontents_pos(["(",")"], term)
 
    if extracted:        
        # test to find any tasks   
        pre_string =  term[:start].lstrip().rstrip()
        tmatch = re.match(r'.*\b(.+)$',pre_string)
        if tmatch:
            task_name_list = find_task(all_defs, tmatch.group(1))
            if(len(task_name_list) == 0):
                task_name == ""
                task_asmt = ""
                task_repl = ""
            elif(len(task_name_list) == 1):
                split_regex = bi_ops_regex+"|\(|\=|\={2}"
                task_name = task_name_list[0].lstrip().rstrip()
                task_asmt_bits = pre_string.split(":=")
                eq_index = pre_string.rfind(":=")
                # check for any built in operators preceding the output assignments - these are a boundary
                task_asmt = re.split(split_regex,pre_string[:eq_index])[-1]
                task_repl = pre_string[eq_index:]+"("+extracted_term+")"
                print("found task "+task_name+" in "+term)
            else:
                sys.exit("Parser Error. illegal number of tasks found in term extraction = "+str(len(task_name_list)))
    
        # check if any further inner-terms to find
        
        next_extracted_term, next_task_name, next_task_asmt, next_task_repl = extract_inner_term(all_defs, extracted_term, bi_ops_regex)
        if (task_name == "") | (next_extracted_term != extracted_term):
            extracted_term = next_extracted_term
            task_name = next_task_name
            task_asmt = next_task_asmt
            task_repl = next_task_repl
    else:
        extracted_term = term
        task_name = ""  
        task_asmt = "" 
        task_repl = "" 
    # if the innermost term is the argument for a task 
    # return the task name along with the term  
    
    return extracted_term, task_name, task_asmt, task_repl   
    
    
#--------------------------------------------------------------    
def find_state_alias(all_defs, astring):
    """search for any tasks in astring"""
    
    sa_regex = '|'.join(all_defs['state_aliases'].keys())
    samatch = re.search(r'(?<![a-zA-Z0-9_\.])('+sa_regex+')(?![a-zA-Z0-9_\.])',astring)
    if samatch:
        sa_found = samatch.group(1)
    else:
        sa_found = "" 
          
#    alist = []
#    sa_found = ""
#    
#    for sa in all_defs['state_aliases'].keys():
#        samatch = re.match(r'.*(?<![a-zA-Z0-9_])'+sa+'(?!\s*\=|[a-zA-Z0-9_]).*',astring)
#        if samatch:
#            sa_found = sa
#            break 
    return sa_found
    
       
#--------------------------------------------------------------
def extract_sa_term(all_defs, term):
    """ extract state aliases from term """
    
    sa_name =""
    sa_name_list = []
    
    # check if the term contains a state alias
    #   -if so, still attempt to extract contents, but if the extracted contents do not contain any 
    #    further inner terms, return the original term and alias name
    
    extracted, extracted_term, start, end = extract_pcontents_pos(["[","]"], term)
 
    if extracted:
        
        # test to find any state aliases   
        pre_string =  term[:start].lstrip().rstrip()
        samatch = re.match(r'.*\b(.+)$',pre_string)
        if samatch:
            sa_name = find_state_alias(all_defs, samatch.group(1))
            if sa_name == "":
                sa_repl = ""
            else:
                sa_repl = sa_name+"["+extracted_term+"]"
    
        # check if any further inner-terms to find        
        next_extracted_term, next_sa_name, next_sa_repl = extract_sa_term(all_defs, extracted_term)
        if (sa_name == "") | (next_extracted_term != extracted_term):
            extracted_term = next_extracted_term
            sa_name = next_sa_name
            sa_repl = next_sa_repl
    else:
        extracted_term = term
        sa_name = ""
        sa_repl = ""  
    
    return extracted_term, sa_name, sa_repl  

#--------------------------------------------------------------
def get_sig_lit_len(all_defs, sig):
    """retrun the length of the signal or literal"""
    
    # return 0 if not found
    sig_len = 0
   
#--------------------------------------------------------------    
def find_sig_dims(all_defs, sig):
    """global, subfield, state, or local"""
    
    # check if "sig" is a global, subfield, state, memory, or local if so
    # return the dimensions
    sig_in_state = False
    sig_in_subfield = False
    for state in all_defs['states'].keys():
        if sig in all_defs['states'][state]['views'].keys():
            sig_in_state = True  
            state_dim = all_defs['states'][state]['views'][sig]['dimensions']  
        if sig in all_defs['states'][state]['subfields'].keys():
            sig_in_subfield = True
            subfield_dim = all_defs['states'][state]['subfields'][sig]['dimensions']                         
    
    if sig_in_state:
        dims = state_dim
    elif sig in all_defs['state_aliases'].keys():
        dims = all_defs['state_aliases'][sig]['sub_dimensions']
    elif sig in all_defs['global_sigs'].keys():
        dims = all_defs['global_sigs'][sig]['dimensions']  
    elif (sig in all_defs['local_sigs'].keys()) & (all_defs['local_sigs'][sig]['type'] == "local_var"):
        dims = all_defs['local_sigs'][sig]['dimensions'] 
    elif sig in all_defs['memories'].keys():
        dims = all_defs['memories'][sig]['sub_dimensions']
    else:
        dims = ""
    return dims

#--------------------------------------------------------------    
def extract_literal(lstring):
    """extract a signle literal value if possible"""
    
    is_literal = False
    length = 0
    value = ""
    lmatch = re.match(r'^(\d+)\'([d|h|b])([\d|x|X|a-f|A-F|\?]+)$',lstring)
    if lmatch:
        length = int(lmatch.group(1))
        dtype = lmatch.group(2)   
        if(dtype == "d"):
            value = bin(int(lmatch.group(3))).replace("0b", "").zfill(length)
        elif(dtype == "h"):    
            value = bin(int(lmatch.group(3), 16))[2:].zfill(length)
        elif(dtype == "b"): 
            value = lmatch.group(3)
        else:
            sys.exit("unknown type in extract literal")
        is_literal = True 
    limatch = re.match(r'^(\d+)$',lstring)
    if limatch:
        length = 32
        value = bin(int(limatch.group(1))).replace("0b", "").zfill(length)
        is_literal = True    
            
    return is_literal, length, value
    
#--------------------------------------------------------------    
def extract_literal_int(lstring):
    """extract a single literal value if possible and return the value as an integer"""
    
    is_literal = False
    length = 0
    value = ""
    lmatch = re.match(r'^(\d+)\'([d|h|b])([\d|x|X|a-f|A-F|\?]+)$',lstring)
    if lmatch:
        length = int(lmatch.group(1))
        dtype = lmatch.group(2)   
        if(dtype == "d"):
            value = int(lmatch.group(3))
        elif(dtype == "h"):    
            value = int(lmatch.group(3), 16)
        elif(dtype == "b"): 
            value = int(lmatch.group(3), 2)
        else:
            sys.exit("unknown type in extract literal")
        is_literal = True 
    limatch = re.match(r'^(\d+)$',lstring)
    if limatch:
        length = 32
        value = int(limatch.group(1))
        is_literal = True    
            
    return is_literal, length, value

#--------------------------------------------------------------    
def check_equiv_literals(lstring):
    """if the term compares literals, attempt to evaluate"""
    estring = lstring
    #bits = lstring.split("==")
    op_regex = "\=\=|\!\=|\>\=|\<=|\>|\<"
    bits = re.split(r'('+op_regex+')',lstring)
    if len(bits) == 3:
        l_is_literal, llen, lval = extract_literal_int(bits[0].lstrip().rstrip())
        r_is_literal, rlen, rval = extract_literal_int(bits[2].lstrip().rstrip())
        operator = bits[1].lstrip().rstrip() 
        eval_string = str(lval)+" "+operator+" "+str(rval)       
        if(l_is_literal & r_is_literal):
            if eval(eval_string):  
                estring = "1'b1";
            else:
                estring = "1'b0";    
    
    return estring
    
#----------------------------------------------------------   
def clean_invert(term):
    
    clean_term = term.rstrip().lstrip()
    while True:
        if ("!1'b" in clean_term) | ("~1'b" in clean_term) :
            clean_term = clean_term.replace("!1'b0","1'b1")
            clean_term = clean_term.replace("~1'b0","1'b1")
            clean_term = clean_term.replace("!1'b1","1'b0")
            clean_term = clean_term.replace("~1'b1","1'b0")
            clean_term = clean_term.replace("!1'bx","1'bx")
            clean_term = clean_term.replace("~1'bx","1'bx")
        else:
            break
    
            
    return clean_term

#----------------------------------------------------------
def term_cleanup(term, marker_list):
    """cleanup redundant terms"""
    # remove redundant 1'b1/1'b0/1'bx terms   
    # only replace at the same parenthesis level
    term = copy.deepcopy(term)
    # start at innermost level and work out recursively until no levels remain
    curr_term = term
    while True:
        extracted, extracted_term, start, end = extract_pcontents_pos(["(",")"], curr_term)
        pre_string =  curr_term[:start].lstrip().rstrip()
        post_string =  curr_term[end+1:].lstrip().rstrip()
        if extracted:
            # check if more extracting is needed (ie this is not yet the innermost term)
            # term_cleanup returns a list of marker structures containing the marker name and what it replaces
            marker_list = term_cleanup(extracted_term, marker_list)
            curr_term = pre_string+marker_list[-1]['name']+post_string
        else:
            break
    #else:
    # at the innermost level
    # replace any redundant terms
    # term may contain a comma delimited list of parameters, if so only replace with a parameter, not across them
    
    bits = curr_term.split(',')
    for bit_idx,bit in enumerate(bits):
        # check for equivalent literals
        bits[bit_idx] = check_equiv_literals(bits[bit_idx])
        
        # check for inverted constants (!1'b0, !1'b1, !1'bx, ~1'b0, ~1'b1, ~1'bx) and replace first (operator precedence)
        bits[bit_idx] = clean_invert(bits[bit_idx])
    
        # remove "&& 1'b1", "1'b1 &&"
        bits[bit_idx] = re.sub(r'\&{2}\s*1\'b1', '', bits[bit_idx])
        bits[bit_idx] = re.sub(r'1\'b1\s*\&{2}', '', bits[bit_idx])
    
        #replace "&& 1'b0", "1'b0 &&"
        tmatch = re.match(r'.*\&\&\s*1\'b0.*', bits[bit_idx])
        if tmatch:
            bits[bit_idx] = "1'b0"
        tmatch = re.match(r'.*1\'b0\s*\&\&.*', bits[bit_idx])
        if tmatch:
            bits[bit_idx] = "1'b0"  
    
        #remove "|| 1'b0", "1'b0 ||"
        bits[bit_idx] = re.sub(r'\|\|\s*1\'b0', '', bits[bit_idx])
        bits[bit_idx] = re.sub(r'1\'b0\s*\|\|', '', bits[bit_idx])
       
        #replace "|| 1'b1", "1'b1 ||"
        tmatch = re.match(r'.*\|\|\s*1\'b1.*', bits[bit_idx])
        if tmatch:
            bits[bit_idx]= "1'b1"
        tmatch = re.match(r'.*1\'b1\s*\|\|.*', bits[bit_idx])
        if tmatch:
            bits[bit_idx] = "1'b1" 
            
        #replace anything with 1'bx in the term with 1'bx
        tmatch = re.match(r'.*\|\|\s*1\'bx.*', bits[bit_idx])
        if tmatch:
            bits[bit_idx] = "1'bx"
    #return it as a marker
    marker_term ={}
    marker_idx = len(marker_list)
    marker_term['name'] = "***term_marker_"+str(marker_idx)+"***"
    marker_term['replaces'] = ','.join(bits)
    marker_list.append(copy.deepcopy(marker_term))   
    return marker_list


#--------------------------------------------------------------
def find_operand_width(all_defs, operand):
    """ search state, signals, memories, and vector aliases for signal, or extract from literal and return length"""
    
    #check if the operand includes a slice, in which case the slice range will define the length
    # need to differentiate with indices. slice will have 2 range bounds, index will have 1
    # note that the index may itself include something with a slice, so check the number of range bounds
    # outside any furthe ranges 
    # eg x[instruction[19:15]], instruction[19:15] is an index to x not the slice range of 19:15
    
    # if operand is a concatenation, break it into parts, determine component length and sum width
    found_concat, concat_inards = extract_pcontents(["{","}"], operand)
    
    # check if the operand is an SV literal
    lmatch = re.match(r'(\d+)\'([d|h|b])([\d|x|X|a-f|A-F|\?]+)',operand)
    # check if the operand is an integer literal
    imatch = re.match(r'(?<![a-zA-Z_\'])(\d+)(?![a-zA-Z_\'])',operand)
    total_len = 0
    if found_concat: # concatenation
        extracted, concat_bits, start = extract_pcontent_fields(["{","}"], ",", operand, 0)
        total_len = 0
           
        for cbit in concat_bits:
            cbit = cbit.lstrip().rstrip()
            # if the concat-bit includes a repetition, extract the repetition multiplier arg
            r_match = re.match(r'(\d+)\{.+\}.*', cbit)
            if r_match:
                rep_count = int(r_match.group(1))
                extracted, bitfield = extract_pcontents(["{","}"], cbit)
            else:
                rep_count = 1
                bitfield = cbit 
            found, bitlen = find_operand_width(all_defs, bitfield) 
            total_len = total_len + (bitlen*rep_count)
        found_operand_len = 1
    else:
        #check if the operand includes a slice, in which case the slice range will define the length
        # need to differentiate with indices. slice will have 2 range bounds, index will have 1
        # note that the index may itself include something with a slice, so check the number of range bounds
        # outside any furthe ranges 
        # eg x[instruction[19:15]], instruction[19:15] is an index to x not the slice range of 19:15
        # Also need to take care of slices on multi-register aliases (eg x), since here the slice will
        # specify a number of registers so the width will be the number of base registers selected 
        # by the slice * the underlying subregister width
        
        sig_in_state = False
        sig_in_subfield = False
        for state in all_defs['states'].keys():
            if operand in all_defs['states'][state]['views'].keys():
                sig_in_state = True
                state_dim = all_defs['states'][state]['views'][operand]['dimensions']
            if operand in all_defs['states'][state]['subfields'].keys():
                sig_in_subfield = True
                subfield_dim = all_defs['states'][state]['subfields'][operand]['dimensions']
    
        extracted, slice_bits, field_start = extract_pcontent_fields(["[","]"], ":", operand, 0)
        found_slice = 0
        found_alias_slice = 0
        if(len(slice_bits) == 2):
            operand = operand[:field_start].rstrip().lstrip()
            if operand in all_defs['state_aliases'].keys():
                sublength = extract_field_len(all_defs['state_aliases'][operand]['sub_dimensions'])
                alias_slice_len = (eval("("+slice_bits[0]+"-"+slice_bits[1]+"+1)"))*sublength 
                found_alias_slice = 1
            else:
                found_slice = 1
                slice_len = eval("("+slice_bits[0]+"-"+slice_bits[1]+"+1)")
        elif(len(slice_bits) == 1) & extracted:
            # if index, remove index fields
            operand = operand[:field_start].rstrip().lstrip()
            if operand in all_defs['state_aliases'].keys():
                sublength = extract_field_len(all_defs['state_aliases'][operand]['sub_dimensions'])
                alias_slice_len = sublength 
                found_alias_slice = 1
            else:
                found_slice = 1
                slice_len = 1
    
        if found_slice: # slice
            # check for slice (after concatenation as it may be only part of concatenated signal)
            # if so, length is explicitly defined by the slice
            total_len = slice_len
            found_operand_len = 1
        elif found_alias_slice: # alias slice
            total_len = alias_slice_len
            found_operand_len = 1
        elif lmatch: # literal
            total_len = int(lmatch.group(1))
            found_operand_len = 1
        elif imatch: # integer literal
            total_len = 32
            found_operand_len = 1
        else:
            # search in signal, state (including subfields) and vector aliases
            found_operand_len = 0
            if sig_in_state:
                total_len = extract_field_len(state_dim)
                found_operand_len = 1
            elif operand in all_defs['state_aliases'].keys():
                total_len = extract_field_len(all_defs['state_aliases'][operand]['sub_dimensions'])
                found_operand_len = 1
            elif operand in all_defs['global_sigs'].keys():
                total_len = extract_field_len(all_defs['global_sigs'][operand]['dimensions'])
                found_operand_len = 1  
            elif operand in all_defs['local_sigs'].keys():
                total_len = extract_field_len(all_defs['local_sigs'][operand]['dimensions'])
                found_operand_len = 1 
            elif operand in all_defs['memories'].keys():
                total_len = extract_field_len(all_defs['memories'][operand]['sub_dimensions'])
                found_operand_len = 1 
            elif sig_in_subfield:
                total_len = extract_field_len(subfield_dim)
                found_operand_len = 1
                 
    return found_operand_len, total_len     

#---------------------------------------------------------
def tokenize_expr(extracted_term,bi_ops_regex):

    expr_bits = re.split(r'('+bi_ops_regex+')(?!'+bi_ops_regex+')',re.sub(r'\s*', '', extracted_term))
    new_expr_bits = []
    for bit in expr_bits:
        expr_bits_bits = re.split(r'('+bi_ops_regex+')(?!'+bi_ops_regex+')',bit)
        expr_bits_bits = [ elem for elem in expr_bits_bits if elem != ""]
        new_expr_bits.extend(expr_bits_bits)
    return new_expr_bits    
    
    
#--------------------------------------------------------------
def extract_inner(term):

    extracted, extracted_term = extract_pcontents(["(",")"], term) 
    if extracted:
        # check if any further inner-terms to find
        next_extracted, next_extracted_term = extract_inner(extracted_term) 
        extracted_term = next_extracted_term
        extracted = True
                     
    else:
        extracted_term = term
    return extracted, extracted_term

#----------------------------------------------------------    
def find_term_length(all_defs, term, bi_map):

    markers = {}
    
    # remove signed/unsigned functions (do not affect length)
    term = term.replace("$unsigned","")
    term = term.replace("$signed","")
    
    length, markers = get_term_length(all_defs, term, bi_map, markers)
    return length
    
#----------------------------------------------------------    
def get_term_length(all_defs, term, bi_map, markers): 
    """ finds the resulting length of the term"""
    
    # REVISIT build proper AST for the term
    # REVISIT - do AST building up-front
    
    bi_ops_regex_list = []
    for op in bi_map.keys():
        bi_ops_regex_list.append(bi_map[op]['regex'])
    bi_ops_regex = '|'.join(bi_ops_regex_list)
    
    all_done = False
    
    extract_attempts = 0        
    while not all_done:
        # deal with concatenations first
        found_concat, concat_inards = extract_pcontents(["{","}"], term)
        if found_concat: # concatenation
            concat_extracted, concat_bits, concat_start = extract_pcontent_fields(["{","}"], ",", term, 0)
            total_len = 0
            for cbit in concat_bits:
                cbit = cbit.lstrip().rstrip()
                # if the concat-bit includes a repetition, extract the repetition multiplier arg
                r_match = re.match(r'^(.+)\{.+\}.*', cbit)
                if r_match:
                    try:
                        rep_count = eval(r_match.group(1))
                        field_extracted, field = extract_pcontents(["{","}"], cbit)
                    except: 
                        rep_count = 1
                        field = cbit 
                else:
                    rep_count = 1
                    field = cbit 
                fieldlen, markers = get_term_length(all_defs, field, bi_map, markers)
                total_len = total_len + (fieldlen*rep_count)
            # replace the concatenation with a marker of length total_len
            replacee = "{"+concat_inards+"}"
            marker_desc = {}
            marker_idx = len(markers)
            marker_name = "@@@marker"+str(marker_idx)+"@@@"
            marker_desc['len'] = total_len    
            marker_desc['replaces'] = replacee
            markers[marker_name] = marker_desc 
            term = term.replace(replacee,marker_name)
            
        else:
            it_extracted, inner_term = extract_inner(term)  
            term_bits = tokenize_expr(inner_term,bi_ops_regex)
            # get the length for every term bit that isn't an operator
            term_struct = []
            for term_bit in term_bits:
                term_desc = {}
                omatch = re.match(r''+bi_ops_regex, term_bit)
                mmatch = re.match(r'(\@{3}marker\d+\@{3})', term_bit)
                if omatch:
                    term_desc['type'] = "operator"
                    term_desc['value'] = term_bit
                    term_desc['len'] = 0
                else:
                    if (term_bit == "true") | (term_bit == "false"):
                        term_desc['type'] = "variable"
                        term_desc['value'] = term_bit
                        term_desc['len'] = 1
                    elif mmatch:
                        term_desc['type'] = "variable"
                        term_desc['value'] = term_bit
                        term_desc['len'] = markers[term_bit]['len']
                    else:    
                        found_var_len, var_len = find_operand_width(all_defs, term_bit)  
                        if not found_var_len:
                            sys.exit("Error could not find length for term variable : "+term_bit)
                        term_desc['type'] = "variable"
                        term_desc['value'] = term_bit
                        term_desc['len'] = var_len 
                term_struct.append(term_desc)                  
            # bi map is defined in precedence order
            # reduce the expression in order
            ops_all_gone = False
            attempts = 0
            while not ops_all_gone:
                num_ts_bits = len(term_struct)
                for curr_op in bi_map.keys():                    
                    for tsidx, tsbit in enumerate(term_struct):
                        marker_desc = {}
                        if tsbit['type'] == "operator":
                            if tsbit['value'] == curr_op:
                                # need to decide exactly which operator in the case of ones that can be unary or multary
                                is_unary = False 
                                if (bi_map[curr_op]['min_ops'] == "1") & (bi_map[curr_op]['max_ops'] == "1"):
                                    is_unary = True
                                elif (bi_map[curr_op]['min_ops'] == "1"):
                                    # check the context
                                    if tsidx == 0:
                                        is_unary = True
                                    elif tsidx > 0:
                                        if term_struct[tsidx-1]['type'] == "operator":
                                            is_unary = True                                    
                                        
                                #decide what to do with the operator
                                # reduce to single term
                                if is_unary:
                                    # find adjacent right (non-deleted) variable/operator
                                    # if another unary operator is adjacent right (eg ~&var) , 
                                    # skip over this one for now even though higher precedence
                                    if num_ts_bits < tsidx+1:
                                        sys.exit("no adjacent variable to unary operator in struct")
                                    found_adj_op = False
                                    found_adj_var = False
                                    for i in range(tsidx+1,num_ts_bits+1):
                                        if term_struct[i]['type'] == "operator":
                                            if bi_map[term_struct[i]['value']]['min_ops'] != "1":
                                                sys.exit("Error. invalid adjacent operators in term: "+term)
                                            found_adj_op = True
                                            break
                                        elif term_struct[i]['type'] == "variable":
                                            found_adj_var = True
                                            var_idx = i
                                            break
                                    if not found_adj_op:
                                        if not found_adj_var:
                                            sys.exit("must be variable to unary operator in struct")
                                        marker_idx = len(markers)
                                        marker_name = "@@@marker"+str(marker_idx)+"@@@"
                                        
                                        if bi_map[curr_op]['rwidth'] == "u1m*":
                                        # result width is context sepecific for operators that could be unary or multary
                                        # if unary then 1, otherwise defined by the operands which must be the same width
                                            marker_desc['len'] = 1
                                        elif bi_map[curr_op]['rwidth'] == "2*": 
                                            marker_desc['len'] = 2*term_struct[var_idx]['len']
                                        elif bi_map[curr_op]['rwidth'] == "*": 
                                            marker_desc['len'] = term_struct[var_idx]['len']
                                        else:
                                            marker_desc['len'] = int(bi_map[op]['rwidth'])
                                           
                                        marker_desc['replaces'] = term_struct[tsidx]['value']+term_struct[var_idx]['value']
                                        markers[marker_name] = marker_desc 
                                        term_struct[tsidx]['type'] = "variable"  
                                        term_struct[tsidx]['value'] = marker_name
                                        term_struct[tsidx]['len'] = marker_desc['len']
                                        term_struct[var_idx]['type'] = "deleted"  
                                        term_struct[var_idx]['value'] = ""
                                        term_struct[var_idx]['len'] = 0
                                else:
                                    # replace pairs 
                                    # find adjacent left and right (non-deleted) variables
                                    # unarys should have already been reduced so just check for 
                                    # variables adjacent
                                    if(tsidx == 0):
                                        sys.exit("Error Cannot have multary term at the start of an expression, in term :"+term)
                                    found_adj_var_left = False
                                    found_adj_var_right = False
                                    for i in range(tsidx+1,num_ts_bits+1):
                                        if term_struct[i]['type'] == "operator":
                                            sys.exit("Error should not be adjacent multary terms (unaries should already be reduced), in term : "+term)
                                        elif term_struct[i]['type'] == "variable":
                                            found_adj_var_right = True
                                            var_right_idx = i
                                            break
                                    for i in range(tsidx-1,-1,-1):
                                        if term_struct[i]['type'] == "operator":
                                            sys.exit("Error should not be adjacent multary terms (unaries should already be reduced), in term : "+term)
                                        elif term_struct[i]['type'] == "variable":
                                            found_adj_var_left = True
                                            var_left_idx = i
                                            break
                                            
                                    if not (found_adj_var_left & found_adj_var_left):
                                        sys.exit("must be variables adjacent to multary operator in struct, in term : "+term)
                                    marker_idx = len(markers)
                                    marker_name = "@@@marker"+str(marker_idx)+"@@@"
                                    if bi_map[curr_op]['rwidth'] == "u1m*":
                                        # result width is context sepecific for operators that could be unary or multary
                                        # if unary then 1, otherwise defined by the operands which must be the same width:
                                        if(term_struct[var_left_idx]['len'] != term_struct[var_right_idx]['len']):
                                            sys.exit("for multary operator "+curr_op+" variables must be the same length, in term : "\
                                                +term+ " lhs: "+str(term_struct[var_left_idx]['len'])+ " rhs: " +str(term_struct[var_right_idx]['len'])) 
                                        marker_desc['len'] = term_struct[var_left_idx]['len']
                                    elif bi_map[curr_op]['rwidth'] == "2*":                                        
                                        # result width comes from the operands, both operands must be the same length
                                        if(term_struct[var_left_idx]['len'] != term_struct[var_right_idx]['len']):
                                            sys.exit("for multary operator "+curr_op+" variables must be the same length, in term : "\
                                                +term+ " lhs: "+str(term_struct[var_left_idx]['len'])+ " rhs: " +str(term_struct[var_right_idx]['len'])) 
                                        marker_desc['len'] = 2*term_struct[var_left_idx]['len']
                                    elif bi_map[curr_op]['rwidth'] == "*":
                                        # result width comes from the operands, both operands must be the same length
                                        if(term_struct[var_left_idx]['len'] != term_struct[var_right_idx]['len']):
                                            sys.exit("for multary operator "+curr_op+" variables must be the same length, in term : "\
                                                +term+ " lhs: "+str(term_struct[var_left_idx]['len'])+ " rhs: " +str(term_struct[var_right_idx]['len'])) 
                                        marker_desc['len'] = term_struct[var_left_idx]['len']  
                                    elif bi_map[curr_op]['rwidth'] == "op0": 
                                        marker_desc['len'] = term_struct[var_left_idx]['len']
                                    elif bi_map[curr_op]['rwidth'] == "op1":
                                        marker_desc['len'] = term_struct[var_right_idx]['len'] 
                                    else:
                                        marker_desc['len'] = int(bi_map[op]['rwidth'])   
                                    marker_desc['replaces'] = term_struct[var_left_idx]['value']+term_struct[tsidx]['value']+term_struct[var_right_idx]['value']
                                    markers[marker_name] = marker_desc
                                    term_struct[tsidx]['type'] = "variable"  
                                    term_struct[tsidx]['value'] = marker_name
                                    term_struct[tsidx]['len'] = marker_desc['len']
                                    term_struct[var_right_idx]['type'] = "deleted"  
                                    term_struct[var_right_idx]['value'] = ""
                                    term_struct[var_right_idx]['len'] = 0
                                    term_struct[var_left_idx]['type'] = "deleted"  
                                    term_struct[var_left_idx]['value'] = ""
                                    term_struct[var_left_idx]['len'] = 0
                            
                # remove deleted terms
                # check if any terms remain
                new_term_struct = []
                op_remaining = False
                for item in term_struct:
                    if item['type'] != "deleted":
                        new_term_struct.append(item)
                    if item['type'] == "operator":
                        op_remaining = True
                ops_all_gone = not op_remaining
                term_struct = copy.deepcopy(new_term_struct)
                if ops_all_gone:
                    # should only be a single term left in term_struct
                    if len(term_struct) != 1:
                        print("term struct when all done is "+str(term_struct))
                        sys.exit("Error. More than one term bit left after reduction")
                            
            # replace the inner term with a marker
            if it_extracted:
                term = term.replace("("+inner_term+")",term_struct[0]['value'])
                all_done = False
            else: 
                term = term.replace(inner_term,term_struct[0]['value'])
                all_done = True
                term_field_len = term_struct[0]['len']
                  
    return term_field_len, markers     
    
#----------------------------------------------------------    
def replace_inline_ref(all_defs, astring):
    """ replaces inline references in the string """
    
    replaced_any = False
    refmatch = re.findall(r'\@(.+?)\@',astring)
    for ref in refmatch:
        replacing_ref = False
        def_fields = ref.split(".") 
        cdict = all_defs[def_fields[0]]
        if len(def_fields) > 1:                   
            try:
                for dfkey in def_fields[1:]:
                    if isinstance(cdict, list):
                        cdict = cdict[int(dfkey)]
                    else:    
                        cdict = cdict[dfkey]   
                replacing_ref = True     
            except:
                cdict = ref 
        refvalue = cdict
        if replacing_ref: 
            astring = astring.replace("@"+ref+"@",refvalue)
            replaced_any = True
        else:
            sys.exit("Error. Could not replace reference "+ref+" in "+astring)
            
    return replaced_any, astring                  
    
#----------------------------------------------------------
#----------------------------------------------------------               
        
          
