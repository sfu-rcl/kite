#
# Copyright 2021  Stuart Hoad, Lesley Shannon
# Reconfigurable Computing Lab, Simon Fraser University.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):
#             Stuart Hoad <stuart_hoad@sfu.ca>
#
# kpfe_parse.py
# Kite Parse Front-end
# - Reads kits configuration files and puts them in defined structures (dicts, lists, etc)
# - Generates an intermediate representation of transitions in guarded action form
# - Output is used to generate methodology specific properties and models


import re
import sys
import os
import copy
import json5
import time

from kpfe import kpfe_common
from kpfe import kpfe_spec_io
from kpfe import kpfe_st_extract
from kpfe import kpfe_st_opt
from kpfe import kpfe_syntax
from kpfe import kpfe_spec_edit

#----------------------------------------------------------
#----------------------------------------------------------
# function definitions
#----------------------------------------------------------    
def dump_irm(irm, irm_filename):
    """write transitions to file"""   
    
    kite_files_irm_def = irm_filename 
    kite_files_irm_def = os.path.expandvars(kite_files_irm_def)
    with open(kite_files_irm_def, 'w') as dumpfile:
        json5.dump(irm, dumpfile, indent=2) 
        
    print("wrote IRM to "+ irm_filename)
    
#----------------------------------------------------------
def kpfe_parse(kite_cfg_file):
    """read definitions, generate definition structures, and generate intermediate representation model"""

    start_parse = time.perf_counter()
        
    kite_files_all_def = []
    all_defs = {}
    edit_defs = []

    kite_cfg = kpfe_spec_io.get_kite_cfg(kite_cfg_file)
    
    
    bi_map = {"~"         : {"regex" :  "\~{1}"            , "min_ops" : "1", "max_ops" : "1", "rwidth" : "*"},
              "!"         : {"regex" :  "\!{1}(?![\=])"    , "min_ops" : "1", "max_ops" : "1", "rwidth" : "1"},           
              "&"         : {"regex" :  "\&{1}(?![\&])"    , "min_ops" : "1", "max_ops" : "2", "rwidth" : "u1m*"},
              "|"         : {"regex" :  "\|{1}(?![\|])"    , "min_ops" : "1", "max_ops" : "2", "rwidth" : "u1m*"},
              "^"         : {"regex" :  "\^{1}"            , "min_ops" : "1", "max_ops" : "2", "rwidth" : "u1m*"},
              "+"         : {"regex" :  "\+{1}"            , "min_ops" : "2", "max_ops" : "*", "rwidth" : "*"},
              "-"         : {"regex" :  "\-{1}"            , "min_ops" : "2", "max_ops" : "*", "rwidth" : "*"},
              "*"         : {"regex" :  "\*{1}"            , "min_ops" : "2", "max_ops" : "*", "rwidth" : "2*"},
              "/"         : {"regex" :  "\/{1}"            , "min_ops" : "2", "max_ops" : "*", "rwidth" : "*"},
              "%"         : {"regex" :  "\%{1}"            , "min_ops" : "2", "max_ops" : "*", "rwidth" : "*"},
              ">>"        : {"regex" :  "\>{2}(?![\>])"    , "min_ops" : "2", "max_ops" : "2", "rwidth" : "op0"},
              ">>>"       : {"regex" :  "\>{3}"            , "min_ops" : "2", "max_ops" : "2", "rwidth" : "op0"},
              "<<"        : {"regex" :  "\<{2}"            , "min_ops" : "2", "max_ops" : "2", "rwidth" : "op0"},
              "=="        : {"regex" :  "\={2}"            , "min_ops" : "2", "max_ops" : "2", "rwidth" : "1"},
              "!="        : {"regex" :  "\!\="             , "min_ops" : "2", "max_ops" : "2", "rwidth" : "1"},
              ">="        : {"regex" :  "\>\="             , "min_ops" : "2", "max_ops" : "2", "rwidth" : "1"},
              "<="        : {"regex" :  "\<\="             , "min_ops" : "2", "max_ops" : "2", "rwidth" : "1"},
              ">"         : {"regex" :  "\>(?![\>|\=])"    , "min_ops" : "2", "max_ops" : "2", "rwidth" : "1"},
              "<"         : {"regex" :  "\<(?![\<|\=])"    , "min_ops" : "2", "max_ops" : "2", "rwidth" : "1"},
              "&&"        : {"regex" :  "\&{2}"            , "min_ops" : "2", "max_ops" : "*", "rwidth" : "*"},
              "||"        : {"regex" :  "\|{2}"            , "min_ops" : "2", "max_ops" : "*", "rwidth" : "*"}}
              
              
    bi_ops_regex_list = []
    for op in bi_map.keys():
        bi_ops_regex_list.append(bi_map[op]['regex'])
    bi_ops_regex = '|'.join(bi_ops_regex_list)
              
# read definition file, build all_defs structure, perform preprocinng steps, and syntax check
                            
    # read target configuration file(s)
    # read model common definitions file(s)
    all_defs = kpfe_spec_io.extract_definitions(all_defs, kite_cfg)
    edit_defs = kpfe_spec_io.extract_edits(edit_defs, kite_cfg)
    
    # read states and fix up multi-def indices
    all_defs['states'] ={}
    all_defs['state_aliases'] = {}
    all_defs = kpfe_spec_io.extract_states(all_defs, kite_cfg)
    all_defs = kpfe_spec_io.fix_ms_indices(all_defs, kite_cfg)
        
    # read instructions
    all_defs['instructions'] = {}
    all_defs = kpfe_spec_io.extract_instructions(all_defs, kite_cfg)
    
    # optionally edit all_defs
    print("edit defs : "+str(edit_defs))
    if len(edit_defs) > 0:
        if "kite_files_alldefs_def" in kite_cfg['file_descriptors']:
            kpfe_spec_io.dump_alldefs(all_defs, kite_cfg, "_pre_edit") 
        all_defs = kpfe_spec_edit.edit_all_defs(all_defs, edit_defs, kite_cfg)
        if "kite_files_alldefs_def" in kite_cfg['file_descriptors']: 
            kpfe_spec_io.dump_alldefs(all_defs, kite_cfg, "_post_edit")
    
    #local signals
    all_defs['local_sigs'] = {}
    all_defs = kpfe_spec_io.build_locals(all_defs, kite_cfg)
    
    #extract task calls
    all_defs['task_calls'] = {}
    all_defs = kpfe_spec_io.build_task_calls(all_defs, kite_cfg, bi_ops_regex)
    
    if(kite_cfg['build_opts']['extract_mem_refs']):
        all_defs = kpfe_spec_io.extract_mem_refs(all_defs, kite_cfg)
     
    # write tasks and enums to the task/enum file
    kpfe_spec_io.gen_tasks_enums(all_defs, kite_cfg)
    
    # replace inline references in globals
    all_defs =  kpfe_spec_io.replace_global_inline_refs(all_defs)

    # check syntax
    print("Checking Syntax of input definitions...")
    kpfe_syntax.syntax_check(all_defs, bi_ops_regex)
    
    # extract_task_dimensions
    # if any task arguments/returns are defined with $size, replace them 
    # with the underlying dimensions
    all_defs = kpfe_spec_io.replace_task_dims(all_defs, kite_cfg)
    
    # build state alias maps
    all_defs = kpfe_spec_io.build_alias_map(all_defs, kite_cfg)
    
    if(kite_cfg['build_opts']['remove_ns_slices']):
        all_defs = kpfe_spec_io.extract_ns_slices(all_defs, kite_cfg, bi_map)
        
    # write all_defs to a file described in kite_files_alldefs_def for debug
    if "kite_files_alldefs_def" in kite_cfg['file_descriptors']:
        kpfe_spec_io.dump_alldefs(all_defs, kite_cfg, "") 
            
     

# build intermediate representation model (IRM) 
    irm = {}
    
    # replace signed/unsigned functions
    
    #local signal tranistions
    print("Extracting local signals...")
    irm['local_sig_trx'] = kpfe_st_extract.extract_local_sigs("local_var", all_defs, kite_cfg, kite_cfg['build_opts'])
    #global signal tranistions
    print("Extracting global signals...")
    irm['global_sig_trx'] = kpfe_st_extract.extract_global_sigs(all_defs, kite_cfg, kite_cfg['build_opts'])
    # state transitions
    print("Extracting state transitions...")
    irm['state_trx'] = kpfe_st_extract.extract_states(all_defs, bi_map, kite_cfg, kite_cfg['build_opts'])
    # memory transitions
    print("Extracting memory transitions...")
    irm['memory_trx'] = kpfe_st_extract.extract_memories(all_defs, kite_cfg, kite_cfg['build_opts'])
    if(kite_cfg['build_opts']['extract_mem_refs']):
        print("Extracting memory reference local signals...")
        irm['mem_ref_local_trx'] = kpfe_st_extract.extract_local_sigs("memref_var", all_defs, kite_cfg, kite_cfg['build_opts'])
    # state alias descriptions
    #irm['state_alias_trx'] = kpfe_st_extract.extract_array_aliases(all_defs, kite_cfg, irm)
    # task local signal declarations
    print("Extracting task local signals...")
    irm['task_local_call_sigs'] = kpfe_st_extract.extract_local_task_signals(all_defs, kite_cfg, kite_cfg['build_opts'])
    # task calls
    print("Adding task calls...")
    irm['task_calls'] = copy.deepcopy(all_defs['task_calls'])
    # cover events
    print("Adding properties...")
    irm = kpfe_st_extract.extract_properties(all_defs, irm, kite_cfg)
    
    if(kite_cfg['build_opts']['remove_ns_slices']):
        print("Cleaning up non-signal slices...")
        irm = kpfe_st_extract.cleanup_ns_slices(all_defs, irm, bi_map)
    
    # write irm to file
    print("Writing initial IRM...")
    irm_count = kpfe_st_opt.count_conditional_actions(irm)
    irm_file = kite_cfg['file_descriptors']['kite_files_irm_def']
    dump_irm(irm, irm_file)
    
    end_ibuild = time.perf_counter()
    
    #*****************************
    # IRM Optimizations
    # perform optimization - remove redundant conditions
    start_opt0 = time.perf_counter()
    if(kite_cfg['build_opts']['max_opt_level'] >= 0):
        print(" perfoming redundant term optimization...")   
        irm = kpfe_st_opt.opt_redundant_cond_terms(irm)
        irm_count = kpfe_st_opt.count_conditional_actions(irm)
        # write opt_0 irm to file
        irm_file = kite_cfg['file_descriptors']['kite_files_irm_def']+".opt_0"
        dump_irm(irm, irm_file)
    end_opt0 = time.perf_counter()
    
    # perform optimization -  local variable deduplication
    start_opt1 = time.perf_counter()
    if(kite_cfg['build_opts']['max_opt_level'] >= 1):
        print(" perfoming local variable deduplication...")    
        irm = kpfe_st_opt.opt_dedup_locals(irm, 'local_sig_trx')
        if(kite_cfg['build_opts']['extract_mem_refs']):
            irm = kpfe_st_opt.opt_dedup_locals(irm, 'mem_ref_local_trx')
        irm_count = kpfe_st_opt.count_conditional_actions(irm)
        # write opt_1 irm to file
        irm_file = kite_cfg['file_descriptors']['kite_files_irm_def']+".opt_1"
        dump_irm(irm, irm_file)
    end_opt1 = time.perf_counter()
    
    # REVISIT
    # perform optimization - common subexpression elimination
    if(kite_cfg['build_opts']['max_opt_level'] >= 2):
        print(" perfoming common subexpression elimination...")    
        #irm = kpfe_st_opt.opt_cse(irm)
        irm_count = kpfe_st_opt.count_conditional_actions(irm)
        # write opt_2 irm to file
        irm_file = kite_cfg['file_descriptors']['kite_files_irm_def']+".opt_2"
        dump_irm(irm, irm_file)
        
    # dump instruction list
        # create the filter list so (optionally) only instructions that cause transitions are reported
    utilized_instr_list = kpfe_spec_io.gen_ifilter_list(irm, all_defs, kite_cfg)   

    kpfe_spec_io.instruction_log(all_defs,kite_cfg,utilized_instr_list)
    kpfe_spec_io.full_debug_instruction_log(all_defs, kite_cfg,utilized_instr_list)
    
    
    end_parse = time.perf_counter()
    
    print("Total parse time = "+str(end_parse - start_parse))
    print("Total iniital build time = "+str(end_ibuild - start_parse))
    print("Total opt0 time = "+str(end_opt0 - start_opt0))
    print("Total opt1 time = "+str(end_opt1 - start_opt1))
    


#----------------------------------------------------------                
#----------------------------------------------------------
def main():
    """ main routine """
    
    # Specify Kite model files
    #   - Target config
    #   - Common files
    #   - Instructions
    #   - Interrupts/Resets (defined as pseudo instructions)
    #   - State 
    

    all_defs = {}
    if(len(sys.argv) != 2) :
        print("illegal number of arguments")
        print("usage: kite_parser.py <kite config file>")
        sys.exit("")
        
    kpfe_parse(sys.argv[1])
                                                                     

#----------------------------------------------------------
if __name__ == '__main__':
    main()
