#
# Copyright 2020  Stuart Hoad, Lesley Shannon
# Reconfigurable Computing Lab, Simon Fraser University.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):
#             Stuart Hoad <stuart_hoad@sfu.ca>
#
# kpfe_st_opt.py
# Functions for optimizing state/global signal transitions in irm

import re
import sys
import os
import copy
import json5
import operator
import hashlib


from kpfe import kpfe_common
#----------------------------------------------------------
def count_conditional_actions(irm):
    """ search the irm for all conditional actions and report number of them"""
    
    ca_count_desc ={}   
    ctype_desc = ['local_sig_trx','global_sig_trx', 'state_trx','memory_trx'] 
    ttype_desc = ['transitions', 'next_state_transitions','state_assignment', 'rd_map_assignment'] 
    ca_count_desc['total'] = 0
    for ctype in ctype_desc:
        ca_count_desc[ctype] = 0
        for var in irm[ctype].keys():
            for desc_field in irm[ctype][var].keys():
                if(desc_field in ttype_desc):
                    for cause in irm[ctype][var][desc_field].keys():
                        ca_count_desc[ctype] = ca_count_desc[ctype] + len(irm[ctype][var][desc_field][cause])                 
        ca_count_desc['total'] = ca_count_desc['total'] + ca_count_desc[ctype] 
        print("conditional-action count for "+ctype+" = "+str(ca_count_desc[ctype]))
    print("Total conditional-action count = "+str(ca_count_desc['total'])) 
    return ca_count_desc
    
#----------------------------------------------------------    
def opt_dedup_locals(irm, trx_field):
    """ remove all duplicate locals and replace with a single instance"""

    # build list of unique local transitions indexed by hash
    unique_trx = {}
    local_map = {}
    for local_trx in irm[trx_field].keys():
        local_hash = irm[trx_field][local_trx]['transitions_hash']
        if local_hash not in unique_trx.keys():
            new_desc ={}
            # add new transition set to unique_trx set
            unique_trx[local_hash] = copy.deepcopy(irm[trx_field][local_trx])
        local_map[local_trx] = "kite_local_"+local_hash
        
    #build a giant regex of all the new local sigs
    locals_regex = '|'.join(local_map.keys())

    # replace all references to the old local value with the deduplicated local
    # turn the irm into a big json file string so don't have to worry about structure
    irmstring = json5.dumps(irm)
#    for local_sig in local_map.keys():
#        irmstring =re.sub(r'(?<![a-zA-Z0-9_])'+local_sig+'(?!\s*\=[^\=]|[a-zA-Z0-9_])',local_map[local_sig],irmstring)
#        #irmstring =re.sub(r'(?<![a-zA-Z0-9_\.])('+locals_regex+')(?!\s*\=[^\=]|[a-zA-Z0-9_])',local_map['\1'], irmstring) 
    irmstring =re.sub(r'(?<![a-zA-Z0-9_\.])('+locals_regex+')(?!\s*\=[^\=]|[a-zA-Z0-9_])',lambda m: local_map.get(m.group()), irmstring) 
        
    irm = json5.loads(irmstring)
    
#    ctype_desc = ['local_sig_trx','global_sig_trx', 'state_trx','memory_trx','mem_ref_local_trx'] 
#    ttype_desc = ['transitions', 'next_state_transitions','state_assignment', 'rd_map_assignment']
#    ftype_desc = ["index_condition", "condition","action"]
#    for ctype in ctype_desc:
#        for var in irm[ctype].keys():
#            for desc_field in irm[ctype][var].keys():
#                if(desc_field in ttype_desc):
#                    for causer in irm[ctype][var][desc_field].keys():
#                        for cidx, condaction in enumerate(irm[ctype][var][desc_field][causer]):
#                            for field in condaction:
#                                if field in ftype_desc:
#                                    new_local_sigs = {}
#                                    curr_field = condaction[field]
#                                    for local_sig in local_map.keys():
#                                        curr_field = re.sub(r'(?<![a-zA-Z0-9_])'+local_sig+'(?!\s*\=[^\=]|[a-zA-Z0-9_])',local_map[local_sig],curr_field)  
#                                    irm[ctype][var][desc_field][causer][cidx][field] = curr_field    
#                                    if len(new_local_sigs) > 0:
#                                        all_defs['local_sigs'].update(new_local_sigs)  
#                                          
#    irmstring = json5.dumps(irm['task_local_call_sigs'])
#    for local_sig in local_map.keys():
#        irmstring =re.sub(r'(?<![a-zA-Z0-9_])'+local_sig+'(?!\s*\=[^\=]|[a-zA-Z0-9_])',local_map[local_sig],irmstring)
#    irm = json5.loads(irm['task_local_call_sigs'])
#    
#    
#    for tidx, tc in enumerate(irm['task_calls']):
#        tcstring = tc
#        for local_sig in local_map.keys():
#            tcstring =re.sub(r'(?<![a-zA-Z0-9_])'+local_sig+'(?!\s*\=[^\=]|[a-zA-Z0-9_])',local_map[local_sig],tcstring)
#        irm['task_calls'][tidx] = tcstring
#    
#     
#    for pidx, prop in enumerate(irm['properties']):
#        pstring = prop
#        for local_sig in local_map.keys():
#            pstring =re.sub(r'(?<![a-zA-Z0-9_])'+local_sig+'(?!\s*\=[^\=]|[a-zA-Z0-9_])',local_map[local_sig],pstring)
#        irm['properties'][pidx] = pstring    
        

    new_local_desc = {}
    # build new local description and replace the old one.
    # build new local description
    for ulocal_hash in unique_trx.keys():
        new_local_name = "kite_local_"+ulocal_hash
        new_local_desc[new_local_name] = copy.deepcopy(unique_trx[ulocal_hash])
               
    # replace any local references in the new locals themselves with the new local names
    new_local_desc_string = json5.dumps(new_local_desc)
#    for local_sig in local_map.keys():
#        new_local_desc_string = re.sub(r'(?<![a-zA-Z0-9_])'+local_sig+'(?!\s*\=[^\=]|[a-zA-Z0-9_])',local_map[local_sig],new_local_desc_string)
    new_local_desc_string = re.sub(r'(?<![a-zA-Z0-9_\.])('+locals_regex+')(?!\s*\=[^\=]|[a-zA-Z0-9_])',lambda m: local_map.get(m.group()), new_local_desc_string) 
    new_local_desc = json5.loads(new_local_desc_string)
    
    # replace local_sig_trx in irm
    irm[trx_field] = copy.deepcopy(new_local_desc)
    
     
    
    return irm

#----------------------------------------------------------
def opt_redundant_cond_terms(irm):
    """ search all conditional actions and remove redundant terms"""
    
    ctype_desc = ['local_sig_trx','global_sig_trx', 'state_trx','memory_trx'] 
    ttype_desc = ['transitions', 'next_state_transitions','state_assignment', 'rd_map_assignment'] 
    for ctype in ctype_desc:
        for var in irm[ctype].keys():
            for desc_field in irm[ctype][var].keys():
                if(desc_field in ttype_desc):
                    for cause in irm[ctype][var][desc_field].keys():
                        new_ca_list = []
                        for ca_idx, ca in enumerate(irm[ctype][var][desc_field][cause]):
                            init_ca_cond = ca['condition']
                            ca['condition'] = remove_redundant_terms(ca['condition'])
                            # if condition now evaluates to 1'b0, remove conditional action altogether
                            cmatch = re.match(r'^\(*1\'b0\)*$',ca['condition'])
                            cnmatch = re.match(r'^\(*\!\(*1\'b1\)*$',ca['condition'])
                            if (not cmatch) & (not cnmatch):
                                new_ca_list.append(ca)   
#                            else:
#                                print("eliminated fully redundant term "+init_ca_cond)
                        irm[ctype][var][desc_field][cause]= copy.deepcopy(new_ca_list)                       

    return irm
    


#----------------------------------------------------------    
def clean_constant(term, const_regex):
    all_done = False
    while not all_done:
        bsearch = re.search(r'\('+const_regex+'\)', term)
        if bsearch:
            term = re.sub(r'\('+const_regex+'\)', const_regex, term)
        else:
            all_done = True
            
    return term
      
#----------------------------------------------------------
def remove_redundant_terms(term):
    """performs an initial logical pass to remove redundant terms"""
    while True:
        more_to_do = False
        marker_list =[]
        marker_list = kpfe_common.term_cleanup(term, marker_list)
        new_term = marker_list[-1]['name']
        # turn the list into a dict
        marker_desc = {}
        for rmarker in marker_list:
            marker_desc[rmarker['name']] = rmarker['replaces']
    
        # replace all markers - keep going until none left
        while True:
            mmatch = re.match(r'.*(\*{3}term_marker_\d+\*{3}).*',new_term)
            if mmatch:                
                repl_marker = kpfe_common.clean_invert(marker_desc[mmatch.group(1)])
                if(repl_marker == "1'b0") | (repl_marker == "1'b1") | (repl_marker == "1'bx"):
                    replacer = repl_marker
                    replacee = mmatch.group(1)
                    new_term = new_term.replace(replacee, replacer)                   
                else:
                    replacer = "("+repl_marker+")"
                    replacee = mmatch.group(1)
                    new_term = new_term.replace(replacee, replacer)
                #remove parenthesis only terms
                
            else:
                break 
              
        new_term = clean_constant(new_term, '1\'b1')
        new_term = clean_constant(new_term, '1\'b0')
        new_term = clean_constant(new_term, '1\'bx')
        #tmatch = re.match(r'.*[\||\&]{2}\s*1\'b[0|1|x].*', new_term)
        tmatch = re.match(r'.*[\||\&]{2}\s*1\'b[0|1|x].*', new_term)
        if tmatch:
            more_to_do = True
        tmatch = re.match(r'.*1\'b[0|1|x]\s*[\||\&]{2}.*', new_term)
        if tmatch:
            more_to_do = True
                
        if more_to_do:
            term = new_term
        else:
            break
            
    return new_term
    
#----------------------------------------------------------    
def find_inner_term(term, marker_list):
    """find innermost term"""
    
    # start at innermost level and work out recursively until no levels remain
    curr_term = term
    while True:
        extracted, extracted_term, start, end = extract_pcontents_pos(["(",")"], curr_term)
        pre_string =  curr_term[:start].lstrip().rstrip()
        post_string =  curr_term[end+1:].lstrip().rstrip()
        if extracted:
            # check if more extracting is needed (ie this is not yet the innermost term)
            marker_list = find_inner_term(extracted_term, marker_list)
            curr_term = pre_string+marker_list[-1]['name']+post_string
        else:
            break   
    # extract the subexpressions in the inner term    
        
    #return inner term as a marker so we don't search it again
    marker_term ={}
    marker_idx = len(marker_list)
    marker_term['name'] = "***term_marker_"+str(marker_idx)+"***"
    marker_term['replaces'] = curr_term
    marker_list.append(copy.deepcopy(marker_term))  
    
    return marker_list
    

#----------------------------------------------------------    
def opt_cse(irm):
    """perform common subexpression elimination on the irm"""
    
    # CSE is a bit easier in Kite as we only optimize the conditions and update_to_value fields
    # in these fields we know that if we create temporaries for the replaced expressions, there can be no dependencies
    # (though maybe suboptimal optimization depending on the ordering of elimination)
    
    # deal with as a tree. each signal is a leaf, then groups (locals, globals, etc), then all
    
    # find subexpresions in the term
    # for the set of subexpressions, rank in order of appearance in the set
    # replace any above the threshold with a new local assigned to that expression
    
    
