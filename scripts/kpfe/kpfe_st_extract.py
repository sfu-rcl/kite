#
# Copyright 2020  Stuart Hoad, Lesley Shannon
# Reconfigurable Computing Lab, Simon Fraser University.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):
#             Stuart Hoad <stuart_hoad@sfu.ca>
#
# kpfe_st_extract.py
# Functions for extracting state/global signal transitions from definitions

import re
import sys
import os
import copy
import json5
import operator
import hashlib

from kpfe import kpfe_common
from kpfe import kpfe_st_opt
from kpfe import kpfe_spec_io


#----------------------------------------------------------
#----------------------------------------------------------  
  
#----------------------------------------------------------    
def find_updates_body(local_desc_list, update_action, curr_signal, update_type, instruction_name, instr_condition, updater_condition, default_condition):
    """ generates conditional actions for signal updates """
    
    for action in update_action:  
        if (curr_signal == action['state_to_update']) :
            condition_action = {}
            condition_action['index_condition'] = ""
            if(update_type == "match"):
                condition_action['condition'] = "("+instr_condition + ") && (" + \
                                                     action['condition']+ ")"
            else:
                condition_action['condition'] = "("+instr_condition + ") && (" + \
                                                     updater_condition + ") && (" + \
                                                     action['condition']+ ")"    
            condition_action['condition'] = condition_action['condition'].replace("true","1'b1")
            condition_action['condition'] = condition_action['condition'].replace("false","1'b0")
            if True: 
                if default_condition == "":
                    default_condition = "("+condition_action['condition']+")"
                else:
                    default_condition = default_condition + " || "+"("+condition_action['condition']+")"
                condition_action['action'] = action['update_to_value']
                condition_action['action'] = condition_action['action'].replace("true","1'b1")
                condition_action['action'] = condition_action['action'].replace("false","1'b0")
                # for debug, useful to know where the transition came from so record that
                condition_action['cause'] = {'instruction': instruction_name, 'update_type': update_type}
                local_desc_list.append(condition_action) 
            
    return local_desc_list, default_condition

#----------------------------------------------------------    
def find_signal_updates(all_defs, curr_signal, is_local_sig, dimensions, build_default):
    """find all updates that affect the current local/global signal(curr_signal)"""

    updates = {}
    default_condition = ""
    
    curr_subfield_desc = {}
    field_updates = {}
    for instruction_name in all_defs['instructions'].keys(): 
        instruction = all_defs['instructions'][instruction_name]
        if(len(instruction['match']) > 1):
            sys.ext("cannot have more than 1 instruction match condition (instruction = "+instruction_name+")")
        instr_condition = instruction['match'][0]['condition'] 
        local_desc_list = []   
        # check exceptions and retire descriptors
        other_desc_to_check = ['match','exceptions','retire','interrupt','reset']
        for update_type in instruction.keys():
            if is_local_sig & (update_type == 'locals'):
                #updates can only defined in the "assignments" section
                updater_condition = "true"               
                update_action = instruction['locals']['assignments']
                if len(update_action) > 0:
                    local_desc_list, default_condition = \
                      find_updates_body(local_desc_list, update_action, curr_signal, update_type, instruction_name, instr_condition, updater_condition, default_condition) 
            elif update_type in other_desc_to_check:
                for updater in instruction[update_type] :
                    updater_condition = updater['condition']
                    update_action = updater['state_action_list']
                    if len(update_action) > 0:
                        local_desc_list, default_condition = \
                          find_updates_body(local_desc_list, update_action, curr_signal, update_type, instruction_name, instr_condition, updater_condition, default_condition) 
                    
        if len(local_desc_list) > 0:           
            field_updates[instruction_name] = copy.deepcopy(local_desc_list)  
        
    # add any updates caused by the assignment term 
    if (not is_local_sig):
        local_desc_list = []
        for assgn in all_defs['global_sigs'][curr_signal]['assignment'] :       
            condition_action = {}           
            condition_action['index_condition'] = ""
            condition_action['condition'] = assgn['condition']
            condition_action['condition'] = condition_action['condition'].replace("true","1'b1")
            condition_action['condition'] = condition_action['condition'].replace("false","1'b0")
            if default_condition == "":
                default_condition = "("+condition_action['condition']+")"
            else:
                default_condition = default_condition + " || "+"("+condition_action['condition']+")"
            condition_action['action'] = assgn['update_to_value']  
            condition_action['action'] = condition_action['action'].replace("true","1'b1")
            condition_action['action'] = condition_action['action'].replace("false","1'b0") 
            condition_action['cause'] = {'instruction': "", 'update_type': "global"}
            local_desc_list.append(condition_action) 
        if len(local_desc_list) > 0:
            field_updates['global_assignment'] = copy.deepcopy(local_desc_list)   
     
    
    if is_local_sig:
    
        # add any updates from task call locals
        # note, there should not be any other assignments in this case!
        # no default condition as always driven by the task output
        # only need to check locals as globals alread directly include the tc local in the 
        # global assignment
        local_desc_list = []
        for tcl_sig in all_defs['local_sigs'].keys():
            if(all_defs['local_sigs'][tcl_sig]['type'] == "taskcall_local_var"):           
                if curr_signal == all_defs['local_sigs'][tcl_sig]['local_name']:
                    condition_action = {}
                    condition_action['index_condition'] = ""
                    condition_action['condition'] = "1'b1"
                    condition_action['action'] = tcl_sig
                    condition_action['action'] = condition_action['action'].replace("true","1'b1")
                    condition_action['action'] = condition_action['action'].replace("false","1'b0")
                    condition_action['cause'] = {'instruction': all_defs['local_sigs'][tcl_sig]['instruction'], 'update_type': "task call local assign"}
                    local_desc_list.append(condition_action) 
        if len(local_desc_list) == 1:   
            field_updates['task_local_assignment'] = copy.deepcopy(local_desc_list)       
        if len(local_desc_list) > 1: 
            sys.ext("Error. Too many task call local assignments to signal "+curr_signal)
            
        # add any updates from memrefs  
        
        local_desc_list = []      
        
        if(all_defs['local_sigs'][curr_signal]['type'] == "memref_var"):  
            for assgn in all_defs['local_sigs'][curr_signal]['assignment'] :     
                condition_action = {}           
                condition_action['index_condition'] = ""
                condition_action['condition'] = assgn['condition']
                condition_action['condition'] = condition_action['condition'].replace("true","1'b1")
                condition_action['condition'] = condition_action['condition'].replace("false","1'b0")
                if default_condition == "":
                    default_condition = "("+condition_action['condition']+")"
                else:
                    default_condition = default_condition + " || "+"("+condition_action['condition']+")"
                condition_action['action'] = assgn['update_to_value'] 
                condition_action['action'] = condition_action['action'].replace("true","1'b1")
                condition_action['action'] = condition_action['action'].replace("false","1'b0")  
                condition_action['cause'] = {'instruction': "", 'update_type': "memref"}
                local_desc_list.append(condition_action) 
        if len(local_desc_list) == 1:   
            field_updates['memref_assignment'] = copy.deepcopy(local_desc_list)        
        if len(local_desc_list) > 1: 
            sys.ext("Error. Too many memory reference assignments to signal "+curr_signal)
         
        
    # add default so if no other conditions are true, the signal is assigned to the default value
    if is_local_sig:
        sig_default = all_defs['local_sigs'][curr_signal]['default']
    else:
        sig_default = all_defs['global_sigs'][curr_signal]['default']
    if build_default: 
        
        if sig_default != "":
            condition_action = {}
            local_desc_list = []
            condition_action['index_condition'] = ""
            if(default_condition == ""):
                condition_action['condition'] = "1'b1"
            else:
                condition_action['condition'] = "!("+default_condition.rstrip("||")+")"
            condition_action['action'] = sig_default
            condition_action['action'] = condition_action['action'].replace("true","1'b1")
            condition_action['action'] = condition_action['action'].replace("false","1'b0")
            
            condition_action['cause'] = {'instruction': "", 'update_type': "default"}
            local_desc_list.append(condition_action) 
            field_updates['default_assignment'] = copy.deepcopy(local_desc_list)
                                     
    # calculate hash of state_alias conditional-actions - later this will be used for deduplication
    hstring = json5.dumps(field_updates)
    updates_hash = hashlib.md5(hstring.encode('utf-8')).hexdigest()                           
    # make format consistent with state transition format so can re-use generation
    if is_local_sig:
        signal_type = "local_signal"
    else:
        signal_type = all_defs['global_sigs'][curr_signal]['type']
    curr_subfield_desc['default_value'] = sig_default
    curr_subfield_desc['signal_type'] = signal_type
    curr_subfield_desc['implemented_cond'] = "1'b1" 
    curr_subfield_desc['dimensions'] = dimensions 
    curr_subfield_desc['transitions_hash'] = updates_hash             
    curr_subfield_desc['transitions'] = field_updates      

    return curr_subfield_desc
    
#----------------------------------------------------------    
def find_sa_updates(all_defs, curr_sa):
    """ find all the transitions in the template for the current state alias"""
    field_updates = {}
    local_desc_list = []
    curr_field_desc = {}
    sa_name = all_defs['local_sigs'][curr_sa]['sa_name']
    sa_index = all_defs['local_sigs'][curr_sa]['index']
    #sa_instr = all_defs['local_sigs'][curr_sa]['instruction']
    sa_instr = "*"
    for cidx, caction in enumerate(all_defs['state_alias_templates'][sa_name]):
        condition_action = {}
        condition_action['index_condition'] = ""
        condition_action['condition'] = caction['condition'].replace("***index***",sa_index)
        condition_action['action'] = caction['action']
        condition_action['action'] = condition_action['action'].replace("true","1'b1")
        condition_action['action'] = condition_action['action'].replace("false","1'b0")
        condition_action['cause'] = {'instruction': sa_instr, 'update_type': "state alias local assign"}
        local_desc_list.append(condition_action)
    field_updates['state_alias'] = copy.deepcopy(local_desc_list) 
    # calculate hash of state_alias conditional-actions - later this will be used for deduplication
    hstring = json5.dumps(field_updates)
    updates_hash = hashlib.md5(hstring.encode('utf-8')).hexdigest()
    # make format consistent with state transition format so can re-use generation
    curr_field_desc['default_value'] = ""
    curr_field_desc['signal_type'] = "local_signal"
    curr_field_desc['implemented_cond'] = "1'b1"     
    curr_field_desc['dimensions'] = all_defs['state_aliases'][sa_name]['sub_dimensions'] 
    curr_field_desc['transitions_hash'] = updates_hash            
    curr_field_desc['transitions'] = field_updates
    return curr_field_desc

#----------------------------------------------------------     
def find_ns_slice_updates(all_defs, curr_sig):

    field_updates = {}
    local_desc_list = []
    curr_field_desc = {}
    for assgn in all_defs['local_sigs'][curr_sig]['assignment'] :       
        condition_action = {}           
        condition_action['index_condition'] = ""
        condition_action['condition'] = assgn['condition']
        condition_action['condition'] = condition_action['condition'].replace("true","1'b1")
        condition_action['condition'] = condition_action['condition'].replace("false","1'b0")
        condition_action['action'] = assgn['update_to_value']
        condition_action['action'] = condition_action['action'].replace("true","1'b1")
        condition_action['action'] = condition_action['action'].replace("false","1'b0")   
        condition_action['cause'] = {'instruction': "", 'update_type': "ns_slice_local"}
        local_desc_list.append(condition_action) 
    if len(local_desc_list) > 0:
        field_updates['local_assignment'] = copy.deepcopy(local_desc_list)
    # calculate hash of state_alias conditional-actions - later this will be used for deduplication
    hstring = json5.dumps(field_updates)
    updates_hash = hashlib.md5(hstring.encode('utf-8')).hexdigest()
    curr_field_desc['default_value'] = ""
    curr_field_desc['signal_type'] = "local_signal"
    curr_field_desc['implemented_cond'] = "1'b1"     
    curr_field_desc['dimensions'] =  dimensions = all_defs['local_sigs'][curr_sig]['dimensions']
    curr_field_desc['transitions_hash'] = updates_hash            
    curr_field_desc['transitions'] = field_updates
    return curr_field_desc
    
#----------------------------------------------------------    
def find_mem_updates(all_defs, curr_mem, dimensions, build_default):
    """find all updates that affect the current memory"""
    
    updates = {}
    default_condition = ""
    
    curr_subfield_desc = {}
    field_updates = {}
    for instruction_name in all_defs['instructions'].keys():  
        instruction = all_defs['instructions'][instruction_name]
        if(len(instruction['match']) > 1):
            sys.ext("cannot have more than 1 instruction match condition (instruction = "+instruction_name+")")
        instr_condition = instruction['match'][0]['condition'] 
        local_desc_list = []   
        # check exceptions and retire descriptors
        desc_to_check = ['match','exceptions','retire','interrupt','reset']
        for update_type in instruction.keys():
            if update_type in desc_to_check:
                for updater in instruction[update_type] :
                    updater_condition = updater['condition']
                    for action in updater['state_action_list']:
                        # memories are typically indexed, retain the index so it can later be used 
                        # as a condition to access a specific element
                        mem_match = re.match(r'\s*(\S+?)\[(.+?)\].*',action['state_to_update'])
                        if mem_match:
                            mem_name = mem_match.group(1).lstrip().rstrip()
                            i_extracted, mem_index = kpfe_common.extract_pcontents(["[","]"], action['state_to_update'])
                        else:
                            mem_name = action['state_to_update']
                            mem_index = ""
                        
                        if (curr_mem == mem_name) :
                            condition_action = {}
                            condition_action['index_condition'] = "["+mem_index+"]"
                            if(update_type == "match"):
                                condition_action['condition'] = "("+instr_condition + ") && (" + \
                                                                     action['condition']+")"
                            else:
                                condition_action['condition'] = "("+instr_condition + ") && (" + \
                                                                     updater_condition + ") && (" + \
                                                                     action['condition']+")"                               
                            condition_action['condition'] = condition_action['condition'].replace("true","1'b1")
                            condition_action['condition'] = condition_action['condition'].replace("false","1'b0")

                            if default_condition == "":
                                default_condition = "("+condition_action['condition']+")"
                            else:
                                default_condition = default_condition + " || "+"("+condition_action['condition']+")"
                            condition_action['action'] = action['update_to_value']
                            condition_action['action'] = condition_action['action'].replace("true","1'b1")
                            condition_action['action'] = condition_action['action'].replace("false","1'b0")
                            # for debug, useful to know where the transition came from so record that
                            condition_action['cause'] = {'instruction': instruction_name, 'update_type': update_type}
                            local_desc_list.append(condition_action)
        if len(local_desc_list) > 0: 
            field_updates[instruction_name] = copy.deepcopy(local_desc_list)  
    
    if build_default:
        # add default so if no other conditions are true, the memory remains the same value
        condition_action = {}
        local_desc_list = []
        condition_action['index_condition'] = ""
        if(default_condition == ""):
            condition_action['condition'] = "1'b1"
        else:
            condition_action['condition'] = "!("+default_condition.rstrip("||")+")"
 
        condition_action['action'] = curr_mem
        condition_action['action'] = condition_action['action'].replace("true","1'b1")
        condition_action['action'] = condition_action['action'].replace("false","1'b0")
        condition_action['cause'] = {'instruction': "", 'update_type': "default"}
        local_desc_list.append(condition_action)
        field_updates['default_assignment'] = copy.deepcopy(local_desc_list)
    else:
        curr_subfield_desc['default_value'] = ""
 
                               
    # make format consistent with state transition format so can re-use generation
    curr_subfield_desc['implemented_cond'] = "1'b1"
    curr_subfield_desc['dimensions'] = dimensions             
    curr_subfield_desc['next_state_transitions'] = field_updates      

    return curr_subfield_desc
    
#----------------------------------------------------------
def state_in_array_alias(all_defs, bi_map, action, map_action_value, curr_state, curr_subfield, instr_condition, updater_condition, map_condition, instruction_name, update_type, defined_wr_val):
    """check if the updater term is actually an alias to the current subfield"""

    
    array_alias_condition_action_list = []
    states_to_check = []
    is_any_array_alias = False    
    state_match = re.match(r'\s*(\S+?)\[(.+?)\].*',action['state_to_update'])
    i_extracted, alias_index = kpfe_common.extract_pcontents(["[","]"], action['state_to_update'])
    
    # By default, seach in the state the subfield is defined in, but Kite also allows subfields to be mapped 
    # into other states if explicitly defined by the optional "add_state_access" field    
    states_to_check.append(curr_state)
    if "add_state_access" in all_defs['states'][curr_state]['subfields'][curr_subfield]:
        states_to_check.extend(all_defs['states'][curr_state]['subfields'][curr_subfield]['add_state_access'])  
    for this_state in states_to_check:
        for curr_view in all_defs['states'][this_state]['views']:
            found_map = 0
            found_state = 0
            if state_match:
                alias_name = state_match.group(1).lstrip().rstrip()
                 
                # search vector aliases to check if state_to_update_field is an alias to curr_view
                for s_alias in all_defs['state_aliases'].keys():
                    if(alias_name == s_alias):
                        # find the alias index corresponding to this state
                        for mapping in all_defs['state_aliases'][s_alias]['mapping']:
                            map_bits = mapping.split('<->')
                            # state alias
                            if (curr_view == map_bits[0]) | (curr_view == map_bits[1]):
                                
                                map_match = re.match(r'.*'+alias_name+'\[(.+?)\].*',mapping)
                                m_extracted, index_condition = kpfe_common.extract_pcontents(["[","]"], mapping)
                                if map_match:
                                    #index_condition = map_match.groups(1)
                                    found_map = 1
                                else: 
                                    sys.exit("Error. could not find mapping index for subfield "+ curr_subfield)                           
            elif action['state_to_update'] == curr_view:
                found_state = 1 
                         
            if (found_map == 1) | (found_state == 1):                
                is_array_alias = True
                if found_map == 1:
                    # if index condition is an integer, determine length of index field
                    index_t_match = re.match(r'.*\'[d|h|b].*',index_condition)
                    if not index_t_match:
                        index_index_length = kpfe_common.find_term_length(all_defs, alias_index, bi_map)
                        index_condition = str(index_index_length)+"'d"+index_condition    
                    # check if alias_index is an integer
                    imatch = re.match(r'(?<![a-zA-Z_\'])(\d+)(?![a-zA-Z_\'])',alias_index)
                    if imatch:
                        mod_alias_index = "32'd"+alias_index
                    else:
                        mod_alias_index = alias_index
                            
                    array_alias_condition = "("+mod_alias_index+ " == "+index_condition+") &&"
                else:
                    array_alias_condition = "" 
                    
                # find out which slice is assigned to the subfield
                # note the position is conditional (eg XLEN=32, XLEN=64 have different bit positions for some fields)!
                # these must be mutually exclusive, but could theoretically be dynamic (eg with writable MXL)
                # to deal with this return a separate conditiona/action pair for each organization condition
                # REVISIT - optimize if assignment is the same for all organizations?
                
                # subfield may not be present for a given organization
                in_some_config = False
                for org in all_defs['states'][this_state]['views'][curr_view]['organization']:
                    subfield_in_org = False
                    for org_subfield_bitmap in org['bit_map']:
                        if curr_subfield == org_subfield_bitmap['field']:
                            subfield_in_org = True
                            subfield_in_org_pos =  org_subfield_bitmap['position'] 
                            subfield_in_org_val =  org_subfield_bitmap['value']  
                    if(subfield_in_org):
                        in_some_config = True
                        condition_action = {}
                        # check if assignment is less than the entire width of the subfield, if so add an index condition
                        # to define the range updated
                        found_map_slice, slice_inards = kpfe_common.extract_pcontents(["[","]"],subfield_in_org_val)
                        if found_map_slice:
                            condition_action['index_condition'] = "["+slice_inards+"]"
                        else:
                            condition_action['index_condition'] = ""
                        # if a defined_wr slice is required on the condition, add it here based on the position in the org
                        # if required a marker, ***map_slice_marker*** will have been added to map_condition
                        # if the subfield is not the entire state, then add position slice
                        map_condition = map_condition.replace("***map_slice_marker***",subfield_in_org_pos)
                        if(update_type == "match"):
                            condition_action['condition'] = "("+org['condition'] + ") && " + \
                                                                 array_alias_condition + "("+\
                                                                 instr_condition + ") && (" + \
                                                                 action['condition'] + ") && (" + \
                                                                 map_condition+")"
                        else:
                            condition_action['condition'] = "("+org['condition'] + ") && " + \
                                                                 array_alias_condition + "("+\
                                                                 instr_condition + ") && (" + \
                                                                 updater_condition + ") && (" + \
                                                                 action['condition'] + ") && (" + \
                                                                 map_condition+")"                
                        condition_action['condition'] = condition_action['condition'].replace("true","1'b1")
                        condition_action['condition'] = condition_action['condition'].replace("false","1'b0")
                        
                        dimensions = all_defs['states'][this_state]['views'][curr_view]['dimensions']
                        if defined_wr_val:
                            slice_range = "" 
                        elif dimensions != subfield_in_org_pos:
                            if kpfe_common.extract_field_len(dimensions) < kpfe_common.extract_field_len(subfield_in_org_pos):                            
                                # check if condition evaluates to 1'b0
                                eval_condition = kpfe_st_opt.remove_redundant_terms(condition_action['condition'])
                                cmatch = re.match(r'^\(*1\'b0\)*$',eval_condition)
                                cnmatch = re.match(r'^\(*\!\(*1\'b1\)*$',eval_condition)
                                if (not cmatch) & (not cnmatch):
                                    print("Warning. Truncating field assignment with non-zero condition in "+curr_view)  
                                slice_range = dimensions 
                            else:
                                slice_range = subfield_in_org_pos
                        else:
                            slice_range = ""  
                        #condition_action['action'] = "("+map_action_value+")"+slice_range 
                        condition_action['action'] = map_action_value.replace("***slice_range_marker***",slice_range)
                        condition_action['action'] = condition_action['action'].replace("true","1'b1")
                        condition_action['action'] = condition_action['action'].replace("false","1'b0")    
                        condition_action['cause'] = {'instruction': instruction_name, 'update_type': update_type, 'view': curr_view, 'state' : this_state}
                        array_alias_condition_action_list.append(condition_action) 
                          
                is_array_alias = in_some_config                      
            else:
                is_array_alias = False
                
            if is_array_alias:
                is_any_array_alias = True  
    
    return is_any_array_alias, array_alias_condition_action_list
    
#----------------------------------------------------------
def find_state_updates(all_defs, bi_map, curr_state,  curr_subfield, dimensions, build_default):
    """find all state updates that affect the view of the current state (curr_view of curr_state)"""        

    updates = {}   
    default_condition = ""
    
    curr_subfield_desc = {}
    field_updates = {}
    
    for instruction_name in all_defs['instructions'].keys():
        instruction = all_defs['instructions'][instruction_name]  
        if(len(instruction['match']) > 1):
            sys.ext("cannot have more than 1 instruction match condition (instruction = "+instruction_name+")")
        instr_condition = instruction['match'][0]['condition']  
        local_desc_list = []  
        # check match, exceptions and retire descriptors
        desc_to_check = ['match','exceptions','retire']
        for update_type in instruction.keys():           
            if update_type in desc_to_check:
                for updater in instruction[update_type] :
                    updater_condition = updater['condition']
                    for action in updater['state_action_list']:
                        # need conditional-action term for each write mapping value-condition pair
                        legal_value_map_name = all_defs['states'][curr_state]['subfields'][curr_subfield]['legal_value_map']
                        legal_map = all_defs['legal_maps'][legal_value_map_name]
                        if( len(legal_map['wr_map']) < 1):
                            sys.exit("Error. Could not find legal write map for "+curr_subfield)
                        for wr_mapping in legal_map['wr_map']:
                            map_bits = wr_mapping['val_map'].split('->')
                            if not (len(map_bits) == 2):
                                sys.exit("Error. could not find attempted->resulting write mapping for "+wr_mapping['val_map'])
                            attempted_write_value = map_bits[0].lstrip().rstrip()
                            resulting_write_value = map_bits[1].lstrip().rstrip()
                            #if no resulting_write_value. then no update to perform
                            if not (resulting_write_value == ""): 
                                defined_wr_val = False                              
                                if(attempted_write_value == "*"):
                                    map_condition = wr_mapping['condition']
                                    # the resulting write value could be a signal, literal or expression
                                    # if it includes a wildcard, replace it with action['update_to_value'] 
                                    map_action_value = "("+resulting_write_value+")"
                                    map_action_value = map_action_value.replace("*","("+action['update_to_value']+")***slice_range_marker***")
                                else:
                                    map_condition = "((("+action['update_to_value']+")***map_slice_marker*** == "+attempted_write_value+") && "+wr_mapping['condition']+")"
                                    if(resulting_write_value == "*"):
                                        sys.ext("Error. Cannot have wildcard resulting value if attempted value is not a wildcard in "+wr_mapping['val_map'])
                                    map_action_value = "("+resulting_write_value+")***slice_range_marker***" 
                                    defined_wr_val = True
                                
                                is_array_alias, array_alias_condition_action_list = \
                                    state_in_array_alias(all_defs, bi_map, action, map_action_value, curr_state, curr_subfield,\
                                                         instr_condition, updater_condition, map_condition, instruction_name, update_type, defined_wr_val)
                                is_subfield = (curr_subfield == action['state_to_update'])
                        
                                # if assignee is defined as a state (whole register) or array alias to a state, then assignment of this subfield may be 
                                # narrower than the entire assignment value, so may need to slice
                                if is_array_alias:
                                    local_desc_list.extend(copy.deepcopy(array_alias_condition_action_list))
                                    # build term for default assignement, could be a list of condition-actions, if so combine them
                                    for list_item in local_desc_list:
                                        if default_condition == "":
                                            default_condition = "("+list_item['condition']+")"
                                        else:
                                            default_condition = default_condition + " || "+"("+list_item['condition']+")"   
                                # if assignee is the subfield then no slicing required
                                elif is_subfield :
                                    map_condition = map_condition.replace("***map_slice_marker***","")
                                    condition_action = {}
                                    condition_action['index_condition'] = ""
                                    if(update_type == "match"):
                                        condition_action['condition'] = "("+instr_condition + ") && (" + \
                                                                             map_condition + ") && (" + \
                                                                             action['condition']+")"
                                    else:
                                        condition_action['condition'] = "("+instr_condition + ") && (" + \
                                                                             map_condition + ") && (" + \
                                                                             updater_condition + ") && (" + \
                                                                             action['condition']+")"
                                    condition_action['condition'] = condition_action['condition'].replace("true","1'b1")
                                    condition_action['condition'] = condition_action['condition'].replace("false","1'b0")
                                    if default_condition == "":
                                        default_condition = "("+condition_action['condition']+")"
                                    else:
                                        default_condition = default_condition + " || "+"("+condition_action['condition']+")"
                                    condition_action['action'] = map_action_value.replace("***slice_range_marker***","")
                                    condition_action['action'] = condition_action['action'].replace("true","1'b1")
                                    condition_action['action'] = condition_action['action'].replace("false","1'b0")
                                    # for debug, useful to know where the transition came from so record that
                                    condition_action['cause'] = {'instruction': instruction_name, 'update_type': update_type,\
                                                                 'view': "", 'state' : curr_state}
                                    local_desc_list.append(condition_action) 
        if len(local_desc_list) > 0:
            field_updates[instruction_name] = copy.deepcopy(local_desc_list)
    
    # add default so if no other conditions are true, the state remains the same value
    # optional because not needed for some models and adds a large default term in some cases which affects optimization performance
    if build_default:
        condition_action = {}
        local_desc_list = []
        condition_action['index_condition'] = ""
        if(default_condition == ""):
            condition_action['condition'] = "1'b1"
        else:
            condition_action['condition'] = "!("+default_condition.rstrip("||")+")"
        condition_action['action'] = "kite_int_reg_"+curr_subfield
        condition_action['action'] = condition_action['action'].replace("true","1'b1")
        condition_action['action'] = condition_action['action'].replace("false","1'b0")
        condition_action['cause'] = {'instruction': "", 'update_type': "default"}
        local_desc_list.append(condition_action) 
        field_updates['default_assignment'] = copy.deepcopy(local_desc_list)      
      
    curr_subfield_desc['arch_state'] = curr_state
    curr_subfield_desc['default_value'] = ""
    #subfields are implemented if any view containing them is implemented, so combine all views implemented state
    impl_term = ""
    for cview in all_defs['states'][curr_state]['views'].values():
        impl_term = impl_term+"("+cview['implemented']+") || "
    impl_term = impl_term.rstrip(" || ")
    impl_term = impl_term.replace("true","1'b1")
    impl_term = impl_term.replace("false","1'b0")
    curr_subfield_desc['implemented_cond'] = impl_term
    curr_subfield_desc['implemented_cond'] = "1'b1" 
    curr_subfield_desc['dimensions'] = dimensions             
    curr_subfield_desc['next_state_transitions'] = field_updates  
    
        

    return curr_subfield_desc   
    
#----------------------------------------------------------     
def find_state_assignments(all_defs, curr_state, curr_view, dimensions): 

    field_updates = {}
    local_desc_list = []
    curr_desc = {}

    orgs = all_defs['states'][curr_state]['views'][curr_view]['organization']
    for curr_org in orgs:
        condition_action = {}
        condition_action['index_condition'] = "" 
        condition_action['condition'] = curr_org['condition']
        condition_action['condition'] = condition_action['condition'].replace("true","1'b1")
        condition_action['condition'] = condition_action['condition'].replace("false","1'b0")
        # evaluate the condition, if it evaluates to false (1'b0), don't add the condition
        condition_action['condition'] = kpfe_st_opt.remove_redundant_terms(condition_action['condition'])
        czmatch = re.match(r'^\(*1\'b0\)*$',condition_action['condition'])
        if not czmatch:  
            expected_len = kpfe_common.extract_field_len(curr_org['dimensions'])
            all_field_len = 0
            unordered_bits = []
            if len(curr_org['bit_map']) < 1:
                sys.exit("No field position mappings defined for "+curr_view) 
            for org_subfield_bitmap in curr_org['bit_map']:
                all_field_len = all_field_len + kpfe_common.extract_field_len(org_subfield_bitmap['position'])                            
                min_idx, max_idx = kpfe_common.extract_field_min_max_idx_int(org_subfield_bitmap['position'])
                exists = False
                for u_item in unordered_bits:
                    if min_idx == u_item[0]:
                        exists = True   
                if exists:
                    sys.exit("Error. Already declared organization subfield bit map starting at "+str(min_idx)+" in state "+curr_view) 
                unordered_bits.append((min_idx,org_subfield_bitmap['value']))
                
            if all_field_len > expected_len:
                sys.exit("Error. Too many bits declared in subfield map in state "+curr_view+ " Expected "+str(expected_len)+" got "+str(all_field_len))
            if all_field_len > expected_len:
                sys.exit("Error. Too few bits declared in subfield map in state "+curr_view+ " Expected "+str(expected_len)+" got "+str(all_field_len))        
            ordered_bits = unordered_bits
            if len(unordered_bits) > 1:   
                ordered_bits.sort(key = operator.itemgetter(0), reverse = True)
            val_string = ""
            for valbit in ordered_bits:
                val_string = val_string + valbit[1] +","    
            val_string = val_string.rstrip(',')
            if len(ordered_bits) > 1:
                val_string = "{"+val_string+"}"
        
            condition_action['action'] = val_string
            condition_action['action'] = condition_action['action'].replace("true","1'b1")
            condition_action['action'] = condition_action['action'].replace("false","1'b0")
            condition_action['cause'] = {'instruction': "*", 'update_type': "state_org_mapping", 'view': curr_view, 'state' : curr_state}
            local_desc_list.append(condition_action)
        
    if len(local_desc_list) > 0:        
        field_updates['*'] = copy.deepcopy(local_desc_list)      
    # make format consistent with state transition format so can re-use generation  
    curr_desc['arch_state'] = curr_state
    curr_desc['default_value'] = "" 
    curr_desc['implemented_cond'] = "1'b1" 
    curr_desc['dimensions'] = dimensions             
    curr_desc['state_assignment'] = field_updates        
    return curr_desc  


#----------------------------------------------------------    
def find_state_read_function(all_defs, curr_state, curr_subfield, dimensions):

    curr_subfield_desc = {}
    field_updates = {}
    local_desc_list = []

    # need conditiona-action term for each read mapping value-condition pair
    legal_value_map_name = all_defs['states'][curr_state]['subfields'][curr_subfield]['legal_value_map']
    legal_map = all_defs['legal_maps'][legal_value_map_name]
    if( len(legal_map['rd_map']) < 1):
        sys.exit("Error. Could not find legal read map for "+curr_subfield)
    for rd_mapping in legal_map['rd_map']:
        map_bits = rd_mapping['val_map'].split('->')
        if not (len(map_bits) == 2):
            sys.exit("Error. could not find attempted->resulting read mapping for "+rd_mapping['val_map'])
        register_map_value = map_bits[0].lstrip().rstrip()
        resulting_read_value = map_bits[1].lstrip().rstrip()
        #if no resulting_write_value. then no update to perform
        if not (resulting_read_value == ""):                                
            if(register_map_value == "*"):
                map_condition = rd_mapping['condition']
                # the resulting read value could be a signal, literal or expression
                # if it includes a wildcard, replace it with "kite_int_reg_"+curr_subfield 
                map_action_value = resulting_read_value
                map_action_value = map_action_value.replace("*","kite_int_reg_"+curr_subfield)
            else:
                map_condition = "((kite_int_reg_"+curr_subfield+" == "+register_map_value+") && "+rd_mapping['condition']+")"
                if(resulting_read_value == "*"):
                    sys.ext("Error. Cannot have wildcard resulting value if attempted value is not a wildcard in "+rd_mapping['val_map'])
                map_action_value = resulting_read_value
            condition_action = {}
            condition_action['index_condition'] = "" 
            condition_action['condition'] = map_condition
            condition_action['condition'] = condition_action['condition'].replace("true","1'b1")
            condition_action['condition'] = condition_action['condition'].replace("false","1'b0")
 
            condition_action['action'] = map_action_value
            condition_action['action'] = condition_action['action'].replace("true","1'b1")
            condition_action['action'] = condition_action['action'].replace("false","1'b0")
            condition_action['cause'] = {'instruction': "*", 'update_type': "read_mapping", 'view': "", 'state' : curr_state} 
            local_desc_list.append(condition_action)
    if len(local_desc_list) > 0:
        field_updates['*'] = copy.deepcopy(local_desc_list)      
    # make format consistent with state transition format so can re-use generation  
    curr_subfield_desc['arch_state'] = curr_state
    curr_subfield_desc['default_value'] = "" 
    #read functions are implemented if any view implemented, so combine all views implemented state
    impl_term = ""
    for cview in all_defs['states'][curr_state]['views'].values():
        impl_term = impl_term+"("+cview['implemented']+") || "
    impl_term = impl_term.rstrip(" || ")
    impl_term = impl_term.replace("true","1'b1")
    impl_term = impl_term.replace("false","1'b0")
    curr_subfield_desc['implemented_cond'] = impl_term 
    curr_subfield_desc['dimensions'] = dimensions             
    curr_subfield_desc['rd_map_assignment'] = field_updates 
            
    return curr_subfield_desc              

#----------------------------------------------------------        
def extract_local_sigs(extract_type, all_defs, kite_cfg, build_opts):
    """Extract transitions for global signals"""
    transitions = {}
    # local signal transitions can be defined explicitly in the locals section or 
    # any of the other sections (match, exception, retire)
    # local signal transitions can also be definied implictly by a task assignment
    # in this case the task assignment gets added to the task list with any local signals replaced by 
    # (global) temp local signal names and mappings added from the tmps to the locals
    for curr_sig in all_defs['local_sigs']:
        if(all_defs['local_sigs'][curr_sig]['type'] == extract_type):
            dimensions = all_defs['local_sigs'][curr_sig]['dimensions']
            build_default = build_opts['gen_signal_default']
            transitions[curr_sig] = find_signal_updates(all_defs, curr_sig, True, dimensions, build_default) 
    if extract_type == "local_var":
        #find_sa_updates
        # add the transitions for state alias locals
        for curr_sig in all_defs['local_sigs']:
            if(all_defs['local_sigs'][curr_sig]['type'] == "state_alias_read_var"):
                transitions[curr_sig] = find_sa_updates(all_defs, curr_sig)
         
    return transitions            

#----------------------------------------------------------
def cleanup_ns_slices(all_defs, irm, bi_map):
    """Remove non-signal slices and replace with local signals"""
    
    #Generation may create non-signal slices (slices of complex terms in parethesis)
    #The kite specification also allows this to simplify definition.
    # Though the input language is nominally SystemVerilog-like, this is not 
    # supported by SV in some tools, therefore, optionally the non-signal slice
    # can be removed and replaced by a new local signal
    
    transitions = {}
#    new_local_sigs = {}
#    # turn the irm into a big json file string so don't have to worry about structure
#    irmstring = json5.dumps(irm)
    
    local_prefix = "kite_ns_slice_local_sig_pex_"
    local_idx = 0
#    irmstring, new_local_sigs, local_idx, modified_ns_slice = kpfe_spec_io.extract_ns_slices_from_string(all_defs, irmstring, local_prefix, local_idx, bi_map)
#    irm = json5.loads(irmstring)
#    if len(new_local_sigs) > 0:
#        all_defs['local_sigs'].update(new_local_sigs)     
    
    ctype_desc = ['local_sig_trx','global_sig_trx', 'state_trx','memory_trx','mem_ref_local_trx'] 
    ttype_desc = ['transitions', 'next_state_transitions','state_assignment', 'rd_map_assignment']
    ftype_desc = ["index_condition", "condition","action"]
    for ctype in ctype_desc:
        for var in irm[ctype].keys():
            for desc_field in irm[ctype][var].keys():
                if(desc_field in ttype_desc):
                    for causer in irm[ctype][var][desc_field].keys():
                        for cidx, condaction in enumerate(irm[ctype][var][desc_field][causer]):
                            for field in condaction:
                                if field in ftype_desc:
                                    new_local_sigs = {}
                                    old_field = condaction[field]
                                    irm[ctype][var][desc_field][causer][cidx][field], new_local_sigs, local_idx, modified_ns_slice =\
                                      kpfe_spec_io.extract_ns_slices_from_string(all_defs, condaction[field], local_prefix, local_idx, bi_map)       
                                    if len(new_local_sigs) > 0:
                                        all_defs['local_sigs'].update(new_local_sigs)                              
                 
    for curr_sig in all_defs['local_sigs']:
        if(all_defs['local_sigs'][curr_sig]['type'] == "ns_slice_local_var"):
            transitions[curr_sig] = find_ns_slice_updates(all_defs, curr_sig)  
    
    irm['local_sig_trx'].update(transitions)
    
    return irm                    
      
#----------------------------------------------------------
def extract_global_sigs(all_defs, kite_cfg, build_opts):
    """Extract transitions for global signals"""
     
    transitions = {}
    
    # generate terms for global signals
    # do not generate assignments for primary inputs,outputs or nd_vars
    for curr_sig in all_defs['global_sigs']:
        # find all transitions and conditions for the specified signal
        dimensions = all_defs['global_sigs'][curr_sig]['dimensions']
        build_default = build_opts['gen_signal_default']
        transitions[curr_sig] = find_signal_updates(all_defs, curr_sig, False, dimensions, build_default)
    
    return transitions


#----------------------------------------------------------
def merge_transitions(all_defs, curr_su_transitions, exist_transitions):
    """ merge transactions from multiple updates"""
    new_su_transitions = {}
    # go through all possible update causes
    cause_list = list(all_defs['instructions'].keys())
    cause_list.extend(['default_assignment'])
    for cause in cause_list:
        some_in_new = False
        some_in_old = False
        if cause in curr_su_transitions.keys(): 
            if len(curr_su_transitions[cause]) > 0:
                some_in_new = True
        if cause in exist_transitions.keys():
            if len(exist_transitions[cause]) > 0:  
                some_in_old = True          
        if some_in_new & some_in_old:
            # merge
            if cause == 'default_assignment':
                # for default just merge the condition
                new_updates = exist_transitions[cause]
                new_updates[0]['condition'] = new_updates[0]['condition']+" && "+ curr_su_transitions[cause][0]['condition']
            else:
                new_updates = exist_transitions[cause]
                new_updates.extend(curr_su_transitions[cause]) 
            new_su_transitions[cause] = copy.deepcopy(new_updates)  
        elif some_in_new:
            new_su_transitions[cause] = curr_su_transitions[cause]
        elif some_in_old:
            new_su_transitions[cause] =  exist_transitions[cause] 
    
    return new_su_transitions                     
    
#----------------------------------------------------------
def extract_states(all_defs, bi_map, kite_cfg, build_opts):
    """Extract updates for all state (subfields) descriptions (except memories)"""
    
    # state update may be defined by 
    # - the (register) state name (view) which accesses all sub-fields in the state
    # - the subfield name directly which only accesses the specified subfield
    # - the array alias (if exists)
    #
    
    # state can be accessed as an array alias, so resolve all array aliases. When accessed as an array alias, the index match 
    # becomes an implicit condition
    # where state is accessed by the register rather than the subfields, the whole register is accessed and aligned as per
    # the register definition
    
    # when an instruction updates state it is explicitly defined in "update_state" fields in the state action list(s)
    #
    # all instructions state updates are guarded by the instruction match condition
    # the condition for the update is explicitly defined in the exception or retire condition, and the individual state 
    # update terms in the state action list(s)
    # therefore the guard term for any given state update is a combination of:
    #   - the instruction match condition
    #   - the retire/exception/interrupt/reset condition
    #   - the individual state update condition 
    #   - if the state is referenced by a vector alias, then also match the index 
    #  
    
    # Need to include read/write mapping function to take into account implementation specific readability/writability of fields
    # The write mapping is combined with the condition/action with the subfield state renamed to an internal field    
    # The read mapping is generated seperately per field to map the internal field to the subfield state                                                                      
    
    transitions = {}
    
    for curr_state in all_defs['states']:
        
        # find all updates (transition actions and conditions) for the specified state subfield    
        #state_transitions = {}                                                                    
        state_assignments = {}   
        build_default = build_opts['gen_state_default']                                                                 
        for curr_subfield in all_defs['states'][curr_state]['subfields']:                          
            dimensions = all_defs['states'][curr_state]['subfields'][curr_subfield]['dimensions']
            curr_su = find_state_updates(all_defs, bi_map, curr_state, curr_subfield, dimensions, build_default)
            transitions["kite_int_reg_"+curr_subfield] = copy.deepcopy(curr_su)
            transitions[curr_subfield] = find_state_read_function(all_defs, curr_state, curr_subfield, dimensions)                  
    
        # By default, seach in the state the subfield is defined in, but Kite also allows subfields to be mapped 
        # into other states if explicitly defined by the "add_state_access" field
        states_to_check = []    
        states_to_check.append(curr_state)
        if "add_state_access" in all_defs['states'][curr_state]['subfields'][curr_subfield]:
            states_to_check.extend(all_defs['states'][curr_state]['subfields'][curr_subfield]['add_state_access'])  
        for this_state in states_to_check:                     
            for curr_view in all_defs['states'][this_state]['views']:         
                # for each subfield, create the mapping assignments to the containing register
                # note that there can be multiple assignments to deal with different conditions (eg mode, xlen)
                # and these can be dynamic
                # each view can have a different mapping
                dimensions = all_defs['states'][this_state]['views'][curr_view]['dimensions']
                transitions[curr_view] = copy.deepcopy(find_state_assignments(all_defs, this_state, curr_view, dimensions))   
    return transitions


#----------------------------------------------------------
def extract_memories(all_defs, kite_cfg, build_opts):
    """Extract updates for all memories"""
    
    transitions = {}
    
    for curr_mem in all_defs['memories']:
        # find all updates (transition actions and conditions) for the specified memory
        # treat each memory as a single state (array) for now
        dimensions = all_defs['memories'][curr_mem]['dimensions']+all_defs['memories'][curr_mem]['sub_dimensions']
        build_default = build_opts['gen_mem_default']
        transitions[curr_mem] = find_mem_updates(all_defs, curr_mem, dimensions, build_default)
             
        
    return transitions  
    
#----------------------------------------------------------        
#def extract_mem_refs(irm, all_defs, kite_cfg, build_opts):
#    """Extract any memory read references"""
#    
#    # search though the transition definitions in all sections for any memory 
#    # read references in either the update or condition terms
#    # replace the reference with a local signal (in the memory ref signal section),
#    # if the exact term already exists, then do not create a new local signal but use the existing one
#    
#    new_local_sigs = {}
#    local_prefix = "kite_mref_local_sig_"
#    local_idx = 0
#    
#    ctype_desc = ['local_sig_trx','global_sig_trx', 'state_trx','memory_trx'] 
#    ttype_desc = ['transitions', 'next_state_transitions','state_assignment', 'rd_map_assignment'] 
#    for ctype in ctype_desc:
#        for var in irm[ctype].keys():
#            for desc_field in irm[ctype][var].keys():
#                if(desc_field in ttype_desc):
#                    for cause in irm[ctype][var][desc_field].keys():
#                        new_ca_list = []
#                        for ca_idx, ca in enumerate(irm[ctype][var][desc_field][cause]):
#                            irm[ctype][var][desc_field][cause][ca_idx]['condition'], ref_local_sigs, local_idx = \
#                                extract_mem_refs_from_string(all_defs, ca['condition'], local_prefix, local_idx)
#                            if len(ref_local_sigs) > 0:
#                                new_local_sigs.update(ref_local_sigs)
#                            irm[ctype][var][desc_field][cause][ca_idx]['action'], ref_local_sigs, local_idx = \
#                                extract_mem_refs_from_string(all_defs, ca['action'], local_prefix, local_idx)
#                            if len(ref_local_sigs) > 0:
#                                new_local_sigs.update(ref_local_sigs)  
#                        
#    # add a new section to the irm for memory references
#    irm['mem_refs'] = new_local_sigs
#
#
#    return irm
#    
#    
#    transitions = {}
#    # local signal transitions can be defined explicitly in the locals section or 
#    # any of the other sections (match, exception, retire)
#    # local signal transitions can also be definied implictly by a task assignment
#    # in this case the task assignment gets added to the task list with any local signals replaced by 
#    # (global) temp local signal names and mappings added from the tmps to the locals
#    for curr_sig in all_defs['local_sigs']:
#        if(all_defs['local_sigs'][curr_sig]['type'] == extract_type):
#            dimensions = all_defs['local_sigs'][curr_sig]['dimensions']
#            print("finding local signal transitions for "+curr_sig)
#            build_default = build_opts['gen_signal_default']
#            transitions[curr_sig] = find_signal_updates(all_defs, curr_sig, True, dimensions, build_default)
##    for curr_sig in all_defs['local_sigs']:
##        if(all_defs['local_sigs'][curr_sig]['type'] == "taskcall_local_var"):
##            transitions[all_defs['local_sigs'][curr_sig]['local_name']] = find_tclocal_updates(all_defs, curr_sig, dimensions)    
#    
#    find_sa_updates
#    # add the transitions for state alias locals
#    for curr_sig in all_defs['local_sigs']:
#        if(all_defs['local_sigs'][curr_sig]['type'] == "state_alias_read_var"):
#            transitions[curr_sig] = find_sa_updates(all_defs, curr_sig)
#     
#    return transitions            

#----------------------------------------------------------
def extract_state_aliases(all_defs, kite_cfg, irm):
    """Extract state aliases locals, and add the access functions to the IRM"""  
    
    # identify state aliases and build access templates
    templates = build_sa_templates(all_defs, kite_cfg)
    
    # identify variants of templates required and build them
    
    # replace accesses in the IRM
    
    # add the specialized templates as global signals   
    

#for curr_s_alias in all_defs['state_aliases']:

#----------------------------------------------------------
def extract_local_task_signals(all_defs, kite_cfg, build_opts):
    """ return a list of the descriptors of all local task call signals"""
    tcls = {}
    for curr_sig in all_defs['local_sigs'].keys():
        if(all_defs['local_sigs'][curr_sig]['type'] == "taskcall_local_var"):
            curr_desc = {}
            curr_desc['dimensions'] = all_defs['local_sigs'][curr_sig]['dimensions']
            tcls[curr_sig] = curr_desc
    return tcls
        
 
    
#----------------------------------------------------------
def extract_properties(all_defs, irm, kite_cfg):
    """ add properties defined in the specification"""
    
    # add a per instruction event (match condition) cover
    
    props = {}
    # instruction match event
    # match condition true for each instructions
    for instruction_name in all_defs['instructions'].keys():  
        print("generating instruction cover for : "+instruction_name)
        event_name = "spec_instr_cov_"+instruction_name.lower()
        instruction = all_defs['instructions'][instruction_name]
        instr_condition = instruction['match'][0]['condition']
        instr_condition = instr_condition.replace("true","1'b1")
        instr_condition = instr_condition.replace("false","1'b0")
        instr_condition = kpfe_st_opt.remove_redundant_terms(instr_condition)
        # the event is now reduced to 1'b0, then don't add
        add_prop = True
        ematch = re.match(r'^\(*1\'b0\)*$',instr_condition)
        if ematch:
            add_prop = False   
        if add_prop: 
            prop_desc = {}
            prop_desc['type'] = "cover"
            prop_desc['property'] = instr_condition
            props[event_name] = prop_desc 
    
    # add specification defined properties
    for spec_prop in all_defs['properties']:
        curr_prop = all_defs['properties'][spec_prop]
        prop_desc = {}
        prop_desc['type'] = curr_prop['type']
        prop_property = curr_prop['property']
        prop_property = prop_property.replace("true","1'b1")
        prop_property = prop_property.replace("false","1'b0")
        prop_property = kpfe_st_opt.remove_redundant_terms(prop_property)
        prop_desc['property'] = prop_property
        if(curr_prop['type'] == "cover"):
            prop_name = "spec_cov_"+spec_prop
        elif(curr_prop['type'] == "assert"):
            prop_name = "spec_assert_"+spec_prop
        elif(curr_prop['type'] == "assume"):
            prop_name = "spec_assume_"+spec_prop
        else:
            sys.exit("Error. Illegal property type in "+spec_prop)
        props[prop_name] = prop_desc
            
    irm['properties'] = copy.deepcopy(props) 
        
    return irm          

