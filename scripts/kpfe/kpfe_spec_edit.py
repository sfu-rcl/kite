#
# Copyright 2020  Stuart Hoad, Lesley Shannon
# Reconfigurable Computing Lab, Simon Fraser University.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):
#             Stuart Hoad <stuart_hoad@sfu.ca>
#
# kpfe_spec_edit.py
# Functions to perform script driven specification edits
# WARNING This is an advanced/experimental feature.
# Edits may cause illegal descriptions that break parsing in unpredictable ways, care must be exercised

import re
import sys
import os
import copy
import json5
import math

from kpfe import kpfe_common
from kpfe import kpfe_st_opt
from kpfe import kpfe_syntax


#----------------------------------------------------------  
#---------------------------------------------------------- 

#----------------------------------------------------------    
def get_edit_def_file(kite_cfg):
    """ read and parse the kite spec edit definition file file """
    edit_def = {}
    edit_def_file = os.path.expandvars(kite_cfg['file_descriptors']['kite_files_spec_edit_def'])
    print("*** Reading spec edit definition file file : " +edit_def_file)
    
    with open(edit_def_file, 'r') as edit_def_file_hndl:
        edit_def = json5.loads(kpfe_common.remove_continuations(edit_def_file_hndl))['edit_def']
    return edit_def 
    
#----------------------------------------------------------     
def make_all_def_edits(all_defs, edit_def):    
    """ perform the specified edits on all_defs"""
    
    # The edit definitions are a list of edit descriptions of the form
    # [ {"field_specifier" : "<heirachical regex>", "op_specifier" : "<operation>", "op_args" : ["<op arg field(s)>"....]},...
    # {
    # the field to operate on is expressed by a "heirachical regex"
    # this is of the form (<regex expr>).(<regex expr>)... where each regex expr" specifies a key, index or string 
    # within the all_defs structure.
    # The regex can include non exact matches and wildcards, in which case all matching fields will be operated on
    # The hierarchy can be incomplete, in which case where the match ends all fields will be operated on, though a 
    # match will not be made if subsequent lower hierarchy does not match
    #
    # Operations are performed on matched blocks
    #   - delete
    #     This removes the structure at the matched level and removes it, 
    #     or replaces it with an empty one ("", []. {}) if at the root
    #   - replace
    #     This replaces the structure at the matched level with the defined json5 formatted block
    #   - modify
    #     This perfoms a regular expression substitution of the specified string within the entire matched block 
    #   - insert
    #     This inserts the defined json5 formatted block at the matched level with 
    #     Note the matched level must be a list or dict
    #     The first argument is the name. This is ignored for list insertion, but must be present
    #   - duplicate
    #     This creates a copy of the structure at the matched level, renames it with the name modifier/substitution regex
    #     and inserts it at the same level as the structure it is duplicating
    
    # for each edit definition entry
    for def_entry in edit_def:
        entry_field = def_entry['field_specifier']
        entry_op = def_entry['op_specifier']
        entry_op_args = def_entry['op_args']
        
        # extract the heirachical regex
        # regex definitions must be enclosed in parenthesis and delimited by "."
        # regex fields may include "." so extract regex's first
        curr_entry_field = entry_field;
        h_regex_list = []
        while True:
            r_extracted, regex_expr, r_start, r_end = kpfe_common.extract_pcontents_pos(["(",")"], curr_entry_field)
            if r_extracted:
                # put the regex in the list and remove the extracted regex so it's not found again
                h_regex_list.append(regex_expr)
                if len(curr_entry_field) > r_end:
                    curr_entry_field = curr_entry_field[:r_start]+ curr_entry_field[r_end+1:]
                else:
                   curr_entry_field = curr_entry_field[:r_start]
            else:
                break
                
        
        if len(h_regex_list) == 0:
            sys.exit("Error. No heirachical regex fields found in "+entry_field)
        # search for the matching field and perform any specified editing
        new_def = copy.deepcopy(all_defs)
        curr_def = copy.deepcopy(all_defs)
        all_defs = search_def(all_defs, curr_def, h_regex_list, 0, [], entry_op, entry_op_args)

            
    return all_defs
    
#----------------------------------------------------------    
def search_def_body(all_defs, curr_defs, h_regex_list, regex_list_idx, hierachy_list, op, op_args_list, curr_field):

    ###print("checking if curr field : "+str(curr_field) +" matches current regex "+h_regex_list[regex_list_idx]+ "from regex list "+str(h_regex_list))
    fmatch = re.match(h_regex_list[regex_list_idx],str(curr_field))
    made_deletion = False
    if fmatch:
        ###print("found matching field "+str(curr_field))
        this_hierachy_list = copy.deepcopy(hierachy_list)
        this_hierachy_list.append(curr_field)
        this_regex_list_idx = regex_list_idx
        # if this is the last item in the regex list, then this is the field to operate on
        # perform the specified operation
        if regex_list_idx == (len(h_regex_list)-1):
            print("last regex matched. Performing op "+op+" field is "+str(curr_field)+ " in "+str(curr_defs))
            # perform operation
            if op == "delete":
                # delete
                print("deleted "+str(curr_defs[curr_field]))
                all_defs, made_deletion = spec_edit_delete(all_defs, this_hierachy_list) 
                #del curr_new_def+this_hierachy_term
            elif op == "delete_match":
                # delete matching
                print("deleting (if matched) "+str(curr_defs[curr_field]))
                all_defs, made_deletion = spec_edit_delete_match(all_defs, this_hierachy_list, op_args_list) 
                #del curr_new_def+this_hierachy_term
            elif op == "replace":
                # replace
                print("replaced "+str(curr_defs[curr_field])+" with "+str(op_args_list[0]))
                all_defs = spec_edit_replace(all_defs, this_hierachy_list, op_args_list[0])
            elif op == "modify":
                # modify
                def_string = json5.dumps(curr_defs[curr_field])
                replace_regex = op_args_list[0]
                replacer = op_args_list[1]
                def_string = re.sub(r''+replace_regex,replacer,def_string)
                print("modified "+str(curr_field)+" to "+def_string)
                all_defs = spec_edit_replace(all_defs, this_hierachy_list, json5.loads(def_string))
                #curr_new_def+this_hierachy_term = json5.loads(def_string)
            elif op == "insert":
                # insert
                print("inserting into "+str(curr_defs[curr_field]))
                if isinstance(curr_defs[curr_field], list):
                    all_defs = spec_edit_insert_list_item(all_defs, this_hierachy_list, op_args_list)                   
                elif isinstance(curr_defs[curr_field], dict):
                    all_defs = spec_edit_insert_dict_item(all_defs, this_hierachy_list, op_args_list)
                else:
                    sys.exit("Error. Can only insert into a dict or list")
                print("inserted field into "+str(curr_field))
            elif op == "duplicate":
                # insert
                if isinstance(curr_defs[curr_field], list):
                    all_defs = spec_edit_duplicate_list_item(all_defs, this_hierachy_list, op_args_list)                   
                elif isinstance(curr_defs[curr_field], dict):
                    all_defs = spec_edit_duplicate_dict_item(all_defs, this_hierachy_list, op_args_list)
                else:
                    sys.exit("Error. Can only insert into a dict or list")
                print("duplicated field "+str(curr_field))
            else:
                sys.exit("Error. Unknown editing operation "+op)
        else :
            # set curr_defs to next level of the current regex and repeat
            # need to keep track of where the search is in the hierachy so
            # push onto the hierarchy term
            this_regex_list_idx = this_regex_list_idx + 1
            all_defs = search_def(all_defs, curr_defs[curr_field], h_regex_list, this_regex_list_idx, this_hierachy_list, op, op_args_list)

    return all_defs, made_deletion


#----------------------------------------------------------    
def search_def(all_defs, curr_defs, h_regex_list, regex_list_idx, hierachy_list, op, op_args_list):  
    """search the current level """
    
    
    if isinstance(curr_defs, list):
        cidx_offset = 0
        for cidx, curr_elem in enumerate(curr_defs):
            all_defs, made_deletion = search_def_body(all_defs, curr_defs, h_regex_list, regex_list_idx, hierachy_list, op, op_args_list, cidx-cidx_offset)
            if made_deletion:
                cidx_offset = cidx_offset + 1    
            
    elif isinstance(curr_defs, dict):
        for curr_field in curr_defs.keys():
            all_defs, made_deletion = search_def_body(all_defs, curr_defs, h_regex_list, regex_list_idx, hierachy_list, op, op_args_list, curr_field)
        
    else:
        sys.exit("Error. Object for replacement must be list or dict")
 
    return all_defs

#----------------------------------------------------------   
def edit_all_defs(all_defs, edit_defs, kite_cfg):     
    """ read the edit file and perform defined edits on all_defs"""
    
    print("Editing all_defs...")
    
    #edit_def = get_edit_def_file(kite_cfg)
    all_defs = make_all_def_edits(all_defs, edit_defs)
    
    return all_defs   
    
#----------------------------------------------------------   
def spec_edit_delete(curr_def,hierachy_list):
    """remove the entry specified by hierachy_list"""
    
    this_hierarchy = copy.deepcopy(hierachy_list)
    mod_curr_def  = copy.deepcopy(curr_def)
    ###print("deleting element at hierarchy "+str(hierachy_list))
    made_deletion = False
    # traverse the hierachy defined in hierachy_list, and remove the element at the end
    if len(this_hierarchy) == 0:
        # can't delete the root, so return an empty field of the curr_def type
        ###print("deleted "+str(curr_def))
        if isinstance(curr_def, dict):
            mod_curr_def = {}
        if isinstance(curr_def, list):
            mod_curr_def = []
        if isinstance(curr_def, str):
            mod_curr_def = ""
    elif len(this_hierarchy) == 1:
        if (isinstance(curr_def, dict)) | (isinstance(curr_def, list)):
            del mod_curr_def[this_hierarchy[0]]
            made_deletion = True
    else:
        popped = this_hierarchy.pop(0)
        mod_curr_def[popped], made_deletion = spec_edit_delete(curr_def[popped],this_hierarchy) 

    return mod_curr_def, made_deletion
    
    
#----------------------------------------------------------   
def spec_edit_delete_match(curr_def,hierachy_list,op_args_list):
    """remove the entry specified by hierachy_list"""
    
    this_hierarchy = copy.deepcopy(hierachy_list)
    mod_curr_def  = copy.deepcopy(curr_def)
    ###print("deleting element at hierarchy "+str(hierachy_list))
    made_deletion = False
    # traverse the hierachy defined in hierachy_list, and remove the element at the end
    # if it matches op_args_list[0]
    if len(this_hierarchy) == 0:
        # can't delete the root, so return an empty field of the curr_def type
        ###print("deleted "+str(curr_def))
        def_string = json5.dumps(curr_def)
        dmatch = re.search(r''+op_args_list[0],def_string)
        if dmatch:
            print("matched deletion target")
            if isinstance(curr_def, dict):
                mod_curr_def = {}
            if isinstance(curr_def, list):
                mod_curr_def = []
            if isinstance(curr_def, str):
                mod_curr_def = ""
    elif len(this_hierarchy) == 1:
        def_string = json5.dumps(mod_curr_def[this_hierarchy[0]])
        #dmatch = re.search(r''+op_args_list[0],def_string)
        dmatch = re.search(r''+str(op_args_list[0]),def_string)
        ###print("matching deletion target "+def_string+" with "+ op_args_list[0])
        if dmatch:
            print("matched deletion target")
            print("deleting "+str(mod_curr_def[this_hierarchy[0]]))
            if (isinstance(curr_def, dict)) | (isinstance(curr_def, list)):
#                if isinstance(mod_curr_def[this_hierarchy[0]], dict):
#                    mod_curr_def[this_hierarchy[0]] = {}
#                if isinstance(mod_curr_def[this_hierarchy[0]], list):
#                    mod_curr_def[this_hierarchy[0]] = []
#                if isinstance(mod_curr_def[this_hierarchy[0]], str):
#                    mod_curr_def[this_hierarchy[0]] = ""
#                if isinstance(mod_curr_def[this_hierarchy[0]], tuple):
#                    mod_curr_def[this_hierarchy[0]] = ()
                del mod_curr_def[this_hierarchy[0]]
                made_deletion = True
    else:
        popped = this_hierarchy.pop(0)
        mod_curr_def[popped], made_deletion = spec_edit_delete_match(curr_def[popped],this_hierarchy,op_args_list) 

    return mod_curr_def, made_deletion

    
    
#----------------------------------------------------------   
def spec_edit_replace(curr_def, hierachy_list,new_element):
    """replace the entry specified by hierachy_list"""
    
    this_hierarchy = copy.deepcopy(hierachy_list)
    mod_curr_def  = copy.deepcopy(curr_def)
    ###print("replacing element at hierarchy "+str(this_hierarchy))
    
    # traverse the hierachy defined in hierachy_list, and replace the element at the end
    if len(this_hierarchy) == 0:
        mod_curr_def = new_element
        ###print("replaced with "+str(new_element))
    else:
        popped = this_hierarchy.pop(0)
        mod_curr_def[popped] = spec_edit_replace(curr_def[popped],this_hierarchy,new_element)

    return mod_curr_def

#----------------------------------------------------------    
def spec_edit_get(curr_def,hierachy_list):
    # traverse the hierachy defined in hierachy_list, and replace the element at the end
    
    this_hierarchy = copy.deepcopy(hierachy_list)
    ###print("getting element at hierarchy "+str(hierachy_list)+" from "+str(curr_def))
    
    if len(this_hierarchy) == 0:
        get_def = curr_def
        ###print("got "+str(get_def))
    else:
        popped = this_hierarchy.pop(0)
        get_def = spec_edit_get(curr_def[popped],this_hierarchy)

    return get_def
    
#---------------------------------------------------------- 
def spec_edit_insert_dict_item(curr_def, hierachy_list, op_args_list):
    """insert item into list at the specified location"""
    
    this_hierarchy = copy.deepcopy(hierachy_list)
    mod_curr_def  = copy.deepcopy(curr_def)
    ###print("inserting element at hierarchy "+str(this_hierarchy))
    
    # traverse the hierachy defined in hierachy_list, and insert the element at the end
    if len(this_hierarchy) == 0:
        mod_curr_def = {}
        mod_curr_def[op_args_list[0]] = op_args_list[1]
        ###print("inserted item "+str(op_args_list[0])+" with value "+str(op_args_list[1])+" into new dict") 
    elif len(this_hierarchy) == 1:
        mod_curr_def[this_hierarchy[0]][op_args_list[0]] = op_args_list[1]
        ###print("inserted item "+str(op_args_list[0])+" with value "+str(op_args_list[1])+" into dict" +str(mod_curr_def))
    else:
        popped = this_hierarchy.pop(0)
        mod_curr_def[popped] = spec_edit_insert_dict_item(curr_def[popped],this_hierarchy,op_args_list)

    return mod_curr_def

    
#---------------------------------------------------------- 
def spec_edit_insert_list_item(curr_def, hierachy_list, op_args_list):
    """insert item into list at the specified location"""
    
    this_hierarchy = copy.deepcopy(hierachy_list)
    mod_curr_def  = copy.deepcopy(curr_def)
    ###print("inserting element at hierarchy "+str(this_hierarchy))
    
    # traverse the hierachy defined in hierachy_list, and insert the element at the end
    if len(this_hierarchy) == 0:
        mod_curr_def = [op_args_list[1]]
        ###print("inserted "+str(op_args_list[1])+" into new list")   
    elif len(this_hierarchy) == 1:
        mod_curr_def[hierachy_list[0]].append(op_args_list[1])
        ###print("inserted "+str(op_args_list[1])+" into list")
    else:
        popped = this_hierarchy.pop(0)
        mod_curr_def[popped] = spec_edit_insert_list_item(curr_def[popped],this_hierarchy,op_args_list)

    return mod_curr_def
    
    
#---------------------------------------------------------- 
def spec_edit_duplicate_dict_item(curr_def, hierachy_list, op_args_list):
    """duplicate, rename, and insert item into list at the specified location"""
    
    this_hierarchy = copy.deepcopy(hierachy_list)
    mod_curr_def  = copy.deepcopy(curr_def)
    ###print("inserting element at hierarchy "+str(this_hierarchy))
    
    # traverse the hierachy defined in hierachy_list, and copy/insert the element at the end
    if len(this_hierarchy) == 0:
        sys.exit("Error cannot duplicate (dict) at root") 
    elif len(this_hierarchy) == 1:
        
        new_item = mod_curr_def[this_hierarchy[0]]
        ###print("sub regex sub "+op_args_list[0]+ " for "+op_args_list[1]+" in "+this_hierarchy[0])
        new_name = re.sub(r''+op_args_list[0], op_args_list[1],this_hierarchy[0])
        ###print("new name is "+new_name)
        print("inserting duplicate element "+new_name+" into dict at hierarchy "+str(this_hierarchy[0]))
        mod_curr_def[new_name] = new_item
        ###print("inserted duplicate item "+new_name+" with value "+str(new_item)+" into dict" +str(mod_curr_def))
    else:
        popped = this_hierarchy.pop(0)
        mod_curr_def[popped] = spec_edit_duplicate_dict_item(curr_def[popped],this_hierarchy,op_args_list)

    return mod_curr_def

#----------------------------------------------------------     
def spec_edit_duplicate_list_item(curr_def, hierachy_list, op_args_list):
    """insert item into list at the specified location"""
    
    this_hierarchy = copy.deepcopy(hierachy_list)
    mod_curr_def  = copy.deepcopy(curr_def)
    ###print("inserting element at hierarchy "+str(this_hierarchy))
    
    # traverse the hierachy defined in hierachy_list, and insert the element at the end
    if len(this_hierarchy) == 0:
        sys.exit("Error cannot duplicate (list) at root")   
    elif len(this_hierarchy) == 1:
        print("inserting duplicate element into list at hierarchy "+str(this_hierarchy[0]))
        new_item = mod_curr_def[this_hierarchy[0]]
        mod_curr_def.append(new_item)
        ###print("inserted "+str(op_args_list[1])+" into list")
        ###print("inserted duplicate item "+str(op_args_list[0])+" with value "+str(op_args_list[1])+" into list")
    else:
        popped = this_hierarchy.pop(0)
        mod_curr_def[popped] = spec_edit_duplicate_list_item(curr_def[popped],this_hierarchy,op_args_list)

    return mod_curr_def
    
    
#---------------------------------------------------------- 
    
    
    
    
    
    
