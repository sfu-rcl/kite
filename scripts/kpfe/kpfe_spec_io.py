#
# Copyright 2020  Stuart Hoad, Lesley Shannon
# Reconfigurable Computing Lab, Simon Fraser University.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author(s):
#             Stuart Hoad <stuart_hoad@sfu.ca>
#
# kpfe_spec_io.py
# Handles reading and processing the Kite input specifications in JSON5 format
# Performs specification proprocessing

import re
import sys
import os
import copy
import json5
import math

from kpfe import kpfe_common
from kpfe import kpfe_st_opt
from kpfe import kpfe_syntax


#----------------------------------------------------------  
#---------------------------------------------------------- 


#----------------------------------------------------------    
def get_kite_cfg(kite_files_cfg):
    """ read and parse the kite model configuration file """
    kite_cfg = {}
    kite_files_cfg = os.path.expandvars(kite_files_cfg)
    print("*** Reading kite configuration file : " +kite_files_cfg)
    
    with open(kite_files_cfg, 'r') as input_file:
        defs = json5.loads(kpfe_common.remove_continuations(input_file))
    kite_cfg = copy.deepcopy(defs['kite_configuration'])
    return kite_cfg  

#----------------------------------------------------------    
def convert_enums(all_defs):
    """ convert enums to aliases for internal model use, and build full enum structure and enumeration """
    
    kpfe_syntax.check_enum_syntax(all_defs)
    new_enums = {}
    new_aliases = {}
    for enum in all_defs['enums'].keys():
        this_enum = all_defs['enums'][enum]
        this_enum_desc = {}
        this_enum_list = []
        next_unused = 0
        elen = len(this_enum)
        if elen == 1:
            ewidth = 1
        else:    
            ewidth = math.ceil(math.log(elen,2))
        print("adding alias for enum "+enum+" with width "+str(ewidth))
        # for each enumeration, check if it already has a literal assigned; if so use that, otherwise pick the
        # next unused literal sized appropriately.
        used_val_list = []
        for elem in this_enum:
            new_elem_desc = {}
            # if an element value is already assigned it will be declared in the form <name>=<value>
            elem_bits = elem.split('=')
            
            if len(elem_bits)== 1:
                new_elem_desc['name'] = elem_bits[0].lstrip().rstrip() 
                new_elem_desc['value'] = "tba"   
            elif len(elem_bits) == 2:
                new_elem_desc['name'] = elem_bits[0].lstrip().rstrip()
                # make the same format
                converted, conv_val = kpfe_common.convert_to_int(elem_bits[1].lstrip().rstrip()) 
                if not converted:
                    sys.exit("error. Could not convert enum element "+elem+" to an integer in "+ enum)
                new_elem_desc['value'] = str(ewidth)+"'d"+str(conv_val)
                used_val_list.append(conv_val)  
            else:
                sys.exit("error. Incorrect assignment for enum element "+elem+" in "+ enum)
            this_enum_list.append(new_elem_desc)
        this_enum_desc['dimensions'] = "["+str(ewidth-1)+":0]"
        this_enum_desc['elems'] = copy.deepcopy(this_enum_list)
        new_enums[enum] = copy.deepcopy(this_enum_desc)
        #now assign unused values to any marked "tba"
        for eidx, new_elem in enumerate(new_enums[enum]['elems']):
            if new_elem['value'] == "tba":
                while next_unused in used_val_list:
                    next_unused = next_unused + 1
                new_enums[enum]['elems'][eidx]['value'] = str(ewidth)+"'d"+str(next_unused) 
                used_val_list.append(next_unused)
            # create an alias for each elem for internal model use
            # check it does not already exist first!            
            if new_elem['name'] in all_defs['aliases'].keys():
                sys.exit("Error. Cannot create internal alias from enum element because alias "+new_elem['name']+" already exists")
            if new_elem['name'] in new_aliases.keys():
                sys.exit("Error. Cannot create internal alias from enum element "+new_elem['name']+" because another enum element has the same name")
            new_aliases[new_elem['name']] = new_elem['value']       
    # replace the old enum list with the new one
    all_defs['enums'] = copy.deepcopy(new_enums)
    # add any newly created aliases to the existing ones   
    all_defs['aliases'].update(new_aliases)
    return all_defs             
        
#----------------------------------------------------------    
def find_su_args(def_string,start_pos):
    # find signed/unsigned arguments
    # note that the arguments may include function (task) calls or other parenthesis, so need to find the 
    # matching closing parenthesis that terminates the arg list
    
    plevel = 0
    found_closing = 0
    idx = start_pos
    idx_offset = 0
    arg_delimiter_list = [0]
    arg_assign_delimiter_list = []
    while found_closing == 0:
         
        if(def_string[idx] == "("):
            plevel = plevel + 1
            
        elif (def_string[idx] == ")"):
            plevel = plevel - 1
            if(plevel < 0):
                sys.exit("unmatched closing )")
            if(plevel == 0):
               found_closing = 1
               closing_pos = idx
        elif(idx == len(def_string)):
            sys.exit("did not find matching macro arg closing )")
         
        if((def_string[idx] == ",") & (plevel == 1)):
            arg_delimiter_list.append(idx_offset)
        if((def_string[idx] == "=") & (plevel == 1)):
            arg_assign_delimiter_list.append(idx_offset)
            
        idx = idx + 1
        idx_offset = idx_offset +1
        

    arg_desc_list = []    
    arg_string = def_string[start_pos+1:closing_pos]
    return arg_string, closing_pos
    
       
#----------------------------------------------------------    
def find_macro_args(def_string,start_pos):
    # find macro arguments
    # note that the arguments may include function (task) calls or other parenthesis, so need to find the 
    # matching closing parenthesis that terminates the arg list
    
    plevel = 0
    found_closing = 0
    idx = start_pos
    idx_offset = 0
    arg_delimiter_list = [0]
    arg_assign_delimiter_list = []
    while found_closing == 0:
         
        if(def_string[idx] == "("):
            plevel = plevel + 1
            
        elif (def_string[idx] == ")"):
            plevel = plevel - 1
            if(plevel < 0):
                sys.exit("unmatched closing )")
            if(plevel == 0):
               found_closing = 1
               closing_pos = idx
        elif(idx == len(def_string)):
            sys.exit("did not find matching macro arg closing )")
        
        if((def_string[idx] == ",") & (plevel == 1)):
            arg_delimiter_list.append(idx_offset)
        if((def_string[idx] == "=") & (plevel == 1)):
            arg_assign_delimiter_list.append(idx_offset)
            
        idx = idx + 1
        idx_offset = idx_offset +1
        

    arg_desc_list = []    
    arg_string = def_string[start_pos+1:closing_pos]
    #print("delim list is "+str(arg_delimiter_list))
    #print("arg string len = "+str(len(arg_string)))
    arg_desc ={}
    if(len(arg_string) > 0):
        for arg_idx,arg_delim_pos in enumerate(arg_delimiter_list):
            
            arg_name = arg_string[arg_delim_pos:arg_assign_delimiter_list[arg_idx]-1].rstrip().lstrip()
            if ( len(arg_delimiter_list) == 0) | (arg_idx == len(arg_delimiter_list)-1):
                # last or only arg
                arg_desc[arg_name] = arg_string[arg_assign_delimiter_list[arg_idx]:].rstrip().lstrip()
            else:
                arg_desc[arg_name] = arg_string[arg_assign_delimiter_list[arg_idx]:arg_delimiter_list[arg_idx+1]-1].rstrip().lstrip()
                
        # return arguments (without enclosing parenthesis) and closing position
    
    return arg_desc,closing_pos
            
     
#----------------------------------------------------------
def eval_lit_expr(all_defs):   
    """ find expressions in the string containing only integers liteals and evaluate them """
    for item_type in all_defs.keys():
        #turn defs back into a string, simpler to make replacements without worrying about the structure
        def_string = json5.dumps(all_defs[item_type])
        found_lit_exp = False
        all_done = False
        while not all_done:
            found_lit_exp = False
            lematch = re.search(r'(?<![a-zA-Z0-9_\'])(\d+[\+|\-|\*|\/]\d+)(?![a-zA-Z0-9_\'])',def_string)
            if lematch:
                found_lit_exp = True
                new_val = eval(lematch.group(1))
                #need to escape special characters for regex
                replacee = str(lematch.group(1))
                replacee = replacee.replace("*","\*")
                replacee = replacee.replace("+","\+")
                replacee = replacee.replace("-","\-")
                replacee = replacee.replace("/","\/")
                #def_string = def_string.replace(lematch.group(1), str(new_val)) 
                def_string = re.sub(r'(?<![a-zA-Z0-9_])'+replacee+'(?!\s*\=[^\=]|[a-zA-Z0-9_])',str(new_val), def_string)
                
            if not found_lit_exp:
                all_done = True
        all_defs[item_type] = copy.deepcopy(json5.loads(def_string))
    return all_defs
               
           

#----------------------------------------------------------
def replace_aliases_j(all_defs):
    search_for_alias = 1
    
    # fix up the aliases themselves first as aliases can include aliases...
    
    # don't want to replace aliases in enum definitions, so 
    # save copy and replace after.
    tmp_enum_defs = copy.deepcopy(all_defs['enums'])
    
    while search_for_alias == 1:
        replaced_alias = 0
        for alias_name in all_defs['aliases'].keys():
                    
            # check against all aliases
            for alias in all_defs['aliases'].keys():
                if alias in all_defs['aliases'][alias_name]:
                    replacee = alias
                    replacer = all_defs['aliases'][alias]
                    all_defs['aliases'][alias_name] = all_defs['aliases'][alias_name].replace(replacee,replacer)
                    all_defs['aliases'][alias_name] = re.sub(r'\b'+replacee+ r'\b',replacer,all_defs['aliases'][alias_name])
                    replaced_alias = 1
        search_for_alias = replaced_alias                     
    
    # then swap them everywhere else                       
    search_for_alias = 0    
    alias_in_item = 0
    for item_type in all_defs.keys():
        # if an alias, need to still deal with item as a structured dict, otherwise alias key will try and replace itself!
        if not (item_type == "aliases"):
            #turn defs back into a string, simpler to make replacements without worrying about the structure
            def_string = json5.dumps(all_defs[item_type])
            alias_in_item = 0
            for alias in all_defs['aliases'].keys():
                
                if alias in def_string:
                    replacee = alias
                    replacer = all_defs['aliases'][alias]
                    def_string = re.sub(r'\b'+replacee+ r'\b',replacer,def_string)
                    replaced_alias = 1
                    alias_in_item = 1
            if(alias_in_item == 1):
                all_defs[item_type] = copy.deepcopy(json5.loads(def_string))
                
    # fixup enums
    all_defs['enums'] = copy.deepcopy(tmp_enum_defs)
    all_defs = copy.deepcopy(eval_lit_expr(all_defs))     
    return all_defs          

    
    
#----------------------------------------------------------
def replace_macros(all_defs):
    """inline replace macros"""
    
    # macros are similar to aliases except may have arguments
    # arguments are replaced in the macro definition for the macro instance
    # before replacing the macro instance in the definitions
    
    # first replace any macros in macro definitions themselves
    replaced_all_macros = 0
    while replaced_all_macros == 0:
        replaced_macro = 0
        item_type = 'macros'
        replaced_macro_in_item = 0
        #turn defs back into a string, simpler to make replacements without worrying about the structure
        def_string = json5.dumps(all_defs[item_type])
        
        for macro_name in all_defs['macros'].keys():
            # macros are indicated by `<macro_name>(optional args)
            macro_search = "`"+macro_name
            if macro_search in def_string:
                start_of_macro_inst = def_string.find(macro_search)
                if(def_string[(start_of_macro_inst+len(macro_search))] == "("):
                    start_pos = start_of_macro_inst+len(macro_search)
                    extracted_macro_args, end_pos = find_macro_args(def_string,start_pos)
                    
                    # replace args in macro definition with those passed by parameter instance
                    macro_string = json5.dumps(all_defs['macros'][macro_name]['macro_body'])
                    
                    # macro always defined as a list to avoide json sytax issue,
                    # remove the list definition at this point as its now just a string
                    macro_string = macro_string[1:] 
                    macro_string = macro_string[:-1]
                    for macro_arg in extracted_macro_args.keys():
                        replacee = macro_arg
                        replacer = extracted_macro_args[macro_arg]
                        macro_string = re.sub(r'\b'+replacee+ r'\b',replacer,macro_string)

                    # replace macro instance in def_string with macro_string   
                    # need to be careful to only replace the one instance found since
                    # macro has been specialized to this instance
                    pre_macro_string = def_string[:start_of_macro_inst-1] 
                    post_macro_string = def_string[end_pos+2:]
                    
                    def_string = pre_macro_string + macro_string + post_macro_string

                    replaced_macro = 1
                    replaced_macro_in_item = 1
        
        if replaced_macro_in_item == 1:
            all_defs[item_type] = copy.deepcopy(json5.loads(def_string))
                
        if replaced_macro == 0:
            replaced_all_macros = 1      
    
    # then replace everywhere else        
    for item_type in all_defs.keys():
        replaced_macro_in_item = 0
        if not (item_type == 'macros'):
            #turn defs back into a string, simpler to make replacements without worrying about the structure
            def_string = json5.dumps(all_defs[item_type])
        
            for macro_name in all_defs['macros'].keys():
                # macros are indicated by `<macro_name>(optional args)
                all_done = False
                while not all_done:
                    macro_search = "`"+macro_name
                    if macro_search in def_string:
                        start_of_macro_inst = def_string.find(macro_search)
                        if(def_string[(start_of_macro_inst+len(macro_search))] == "("):
                            start_pos = start_of_macro_inst+len(macro_search)
                            ###if start_pos > 1000:
                            extracted_macro_args, end_pos = find_macro_args(def_string,start_pos)
                            
                            # replace args in macro definition with those passed by parameter instance
                            macro_string = json5.dumps(all_defs['macros'][macro_name]['macro_body'])
                            
                            # macro always defined as a list to avoid json syntax issue,
                            # remove the list definition at this point as its now just a string
                            macro_string = macro_string[1:] 
                            macro_string = macro_string[:-1]

                            for macro_arg in extracted_macro_args.keys():
                                replacee = macro_arg
                                replacer = extracted_macro_args[macro_arg]
                                macro_string = re.sub(r'\b'+replacee+ r'\b',replacer,macro_string)

                            # replace macro instance in def_string with macro_string   
                            # need to be careful to only replace the one instance found since
                            # macro has been specialized to this instance
                            pre_macro_string = def_string[:start_of_macro_inst-1] 
                            post_macro_string = def_string[end_pos+2:]
                            
                            def_string = pre_macro_string + macro_string + post_macro_string

                            replaced_macro = 1
                            replaced_macro_in_item = 1
                    else:
                        all_done = True
        
            if replaced_macro_in_item == 1:
                all_defs[item_type] = copy.deepcopy(json5.loads(def_string))                   

    return all_defs            

#---------------------------------------------------------- 
def extract_cfg_opts(kite_files_cfg):
    """ read and parse model build configuration """
    model_cfg = {}
    kite_files_cfg = os.path.expandvars(kite_files_cfg)
    print("*** Parsing definition input file : " +kite_files_cfg)
    with open(kite_files_cfg, 'r') as input_file:
        defs = json5.loads(kpfe_common.remove_continuations(input_file))
    model_cfg = copy.deepcopy(defs['configuration'])
         
    return model_cfg        

#----------------------------------------------------------     
def replace_global_inline_refs(all_defs):
    """replace any inline refs in global signals"""
    
    # inline refs may be used in global signals in the default and assignment sections
    # replace these with the field in the specification
    print("replacing inline refs in global signals")
    for gsig in all_defs['global_sigs']:
        curr_gsig = all_defs['global_sigs'][gsig]
        replaced_this, all_defs['global_sigs'][gsig]['default'] = kpfe_common.replace_inline_ref(all_defs, curr_gsig['default'])
        for aidx, assgn in enumerate(curr_gsig['assignment']):
            replaced_this, all_defs['global_sigs'][gsig]['assignment'][aidx]['update_to_value'] = kpfe_common.replace_inline_ref(all_defs, assgn['update_to_value']) 
            replaced_this, all_defs['global_sigs'][gsig]['assignment'][aidx]['condition'] = kpfe_common.replace_inline_ref(all_defs, assgn['condition'])  
            
    
    return all_defs

#----------------------------------------------------------    
def eval_idx_in_str(a_string, idx_name, idx):   
    
    curr_string = a_string
    while True:
        rmatch = re.match(r'.*\<\|(.+?)\|\>.*',curr_string)
        if rmatch:
            #print("found index "+rmatch.group(1)+" in "+curr_string[:100])
            name_term = re.sub(r''+idx_name,str(idx),rmatch.group(1))
            name_idx = eval(name_term)
            try:
                name_idx = eval(name_term)
            except:
                sys.exit("could not evaluate "+name_term+" from index "+rmatch.group(1)+" in "+a_string)
            #curr_string = re.sub(r'\<\|'+rmatch.group(1)+'\|\>',str(name_idx),curr_string)
            curr_string = curr_string.replace("<|"+rmatch.group(1)+"|>",str(name_idx))
            #print("idx = "+str(idx)+ ", name term = "+name_term+". Replaced "+rmatch.group(1)+" with "+str(name_idx))
        else:
            break
            
    return curr_string
    

#----------------------------------------------------------    
def extract_definitions(all_defs, kite_cfg):
    """read definition files and extract typedefs, enums, aliases, tasks and macros"""
    file_array = []
    enums = {}
    typedefs = {}
    aliases = {}
    tasks = {}
    tmaps = {}
    macros = {}
    global_sigs = {}
    properties = {}
    legal_maps = {}
    memories = {}
    
    kite_files_spec_src = kite_cfg['file_descriptors']['kite_def_files'] 
        
    for def_file in kite_cfg['file_descriptors']['kite_def_files']:
        print("*** Parsing definition input file : " +def_file)
        filename = def_file 
        filename = os.path.expandvars(filename)
        with open(filename, 'r') as input_file:
            defs = json5.loads(kpfe_common.remove_continuations(input_file))
        # this will return an object which is a list of dicts    
        # iterate through each object and add to respective dict for the kind of definition
        
        for def_key in defs.keys():
            if "definitions" in defs[def_key]:
                for item in defs[def_key]['definitions']:
                    add_item_def = {}
                    add_item_def = copy.deepcopy(item['def'])
                    if(item['def_type'] == "enum"):
                        if item['def_name'] in enums.keys():
                            sys.exit("Enum "+item['def_name']+" already exists")
                        enums[item['def_name']] = add_item_def
                        print("added enum: "+ item['def_name'])
                    elif(item['def_type'] == "typedef"):
                        if item['def_name'] in typedefs.keys():
                            sys.exit("Typedef "+item['def_name']+" already exists")
                        typedefs[item['def_name']] = add_item_def
                        print("added typedef: "+ item['def_name'])
                    elif(item['def_type'] == "alias"):
                        if item['def_name'] in aliases.keys():
                            sys.exit("Alias "+item['def_name']+" already exists")
                        aliases[item['def_name']] = add_item_def
                        print("added alias: "+ item['def_name'])
                    elif(item['def_type'] == "task"):
                        if item['def_name'] in tasks.keys():
                            sys.exit("Task "+item['def_name']+" already exists")
                        tasks[item['def_name']] = add_item_def 
                        print("added task: "+ item['def_name'])
                    elif(item['def_type'] == "macro"):
                        if item['def_name'] in macros.keys():
                            sys.exit("Macro "+item['def_name']+" already exists")
                        macros[item['def_name']] = add_item_def 
                        print("added macro: "+ item['def_name'])
                    elif(item['def_type'] == "state_legal_map"):
                        if "multi_def" in add_item_def.keys():
                            # if defining multiple legal_maps, expand definition and replace the index in the definition for each
                            # replacement is denoted by <index_name>, evaluate and replace all instances
                            for idx in range(int(add_item_def['multi_def']['range_min']),int(add_item_def['multi_def']['range_max'])+1):
                                new_def = {}
                                new_name = eval_idx_in_str(item['def_name'], add_item_def['multi_def']['index'], idx)

                                state_string = json5.dumps(add_item_def['wr_map'])
                                state_string = eval_idx_in_str(state_string, add_item_def['multi_def']['index'], idx)
                                #state_string = re.sub(r'\<'+add_item_def['multi_def']['index']+'\>',str(idx),state_string)
                                new_def['wr_map'] = json5.loads(state_string)
                                
                                state_string = json5.dumps(add_item_def['rd_map'])
                                state_string = eval_idx_in_str(state_string, add_item_def['multi_def']['index'], idx)
                                #state_string = re.sub(r'\<'+add_item_def['multi_def']['index']+'\>',str(idx),state_string)
                                new_def['rd_map'] = json5.loads(state_string)
                                if new_name in legal_maps.keys():
                                    sys.exit("Legal map "+new_name+" already exists")
                                legal_maps[new_name] = new_def 
                                print("added legal map: "+new_name)
                        else:
                            if item['def_name'] in legal_maps.keys():
                                sys.exit("Legal map "+item['def_name']+" already exists")
                            legal_maps[item['def_name']] = add_item_def 
                            print("added legal map: "+ item['def_name'])
                    elif(item['def_type'] == "global_signal"):
                        if item['def_name'] in global_sigs.keys():
                            sys.exit("Global signal "+item['def_name']+" already exists")
                        global_sigs[item['def_name']] = add_item_def 
                        print("added global signal: "+ item['def_name'])
                    elif(item['def_type'] == "property"):
                        if item['def_name'] in properties.keys():
                            sys.exit("Property "+item['def_name']+" already exists")
                        properties[item['def_name']] = add_item_def 
                        print("added property: "+ item['def_name'])
                    else:
                        sys.exit("unknown definition type: "+ item['def_type'])       
   
    all_defs['enums'] = copy.deepcopy(enums)
    all_defs['typedefs'] = copy.deepcopy(typedefs)
    all_defs['properties'] = copy.deepcopy(properties)
    all_defs['aliases'] = copy.deepcopy(aliases)
    all_defs['tasks'] = copy.deepcopy(tasks)
    all_defs['tmaps'] = copy.deepcopy(tmaps)
    all_defs['macros'] = copy.deepcopy(macros) 
    all_defs['legal_maps'] = copy.deepcopy(legal_maps)  
    all_defs['global_sigs'] = copy.deepcopy(global_sigs)  
    all_defs['memories'] = copy.deepcopy(memories)
     
    # convert enums
    # enums are replaced by aliases in the kite spec, though are retained 
    # as enums (with defined enumeration) for use in tasks
    all_defs = convert_enums(all_defs) 
    
    # replace aliases
    all_defs_post_alias = {}
    all_defs_post_macros = {}
    all_defs_post_alias = replace_aliases_j(all_defs)
    
    # replace macros
    all_defs_post_macros = replace_macros(all_defs_post_alias)
    
    return all_defs_post_macros
            
#----------------------------------------------------------    
def extract_states(all_defs, kite_cfg):
    """read state files and extract all defined states"""
    
    for def_file in kite_cfg['file_descriptors']['kite_def_files']:
        print("*** Parsing definition input file : " +def_file)
        filename = def_file 
        filename = os.path.expandvars(filename)

        with open(filename, 'r') as input_file:
            state_defs = json5.loads(kpfe_common.remove_continuations(input_file))
            
        for state_def_key in state_defs.keys():
            if "states" in state_defs[state_def_key]:
                for this_state_name in state_defs[state_def_key]['states'].keys():
                    this_state = state_defs[state_def_key]['states'][this_state_name]
                    if "multi_def" in this_state.keys():
                        # if defining multiple states, expand definition and replace the index in the definition for each
                        # replacement is denoted by <index_name>, replace all instances
                        # don't need the multi_def in the individual copy
                        # evaluate the index later
                        for idx in range(int(this_state['multi_def']['range_min']),int(this_state['multi_def']['range_max'])+1):
                            new_state = {}
                            #new_name = re.sub(r'\<'+this_state['multi_def']['index']+'\>',str(idx),this_state_name)  
                            new_name = eval_idx_in_str(this_state_name, this_state['multi_def']['index'], idx)
                            
                            state_string = json5.dumps(this_state['views'])
                            #state_string = re.sub(r'\<'+this_state['multi_def']['index']+'\>',str(idx),state_string)
                            state_string = eval_idx_in_str(state_string, this_state['multi_def']['index'], idx)
                            new_state['views'] = json5.loads(state_string)
                            
                            state_string = json5.dumps(this_state['subfields'])
                            #state_string = re.sub(r'\<'+this_state['multi_def']['index']+'\>',str(idx),state_string)
                            state_string = eval_idx_in_str(state_string, this_state['multi_def']['index'], idx)
                            new_state['subfields'] = json5.loads(state_string)
                            #print("added new (expanded index) state "+new_name+" = "+str(new_state))
                            all_defs['states'][new_name] = copy.deepcopy(new_state) 
                    else:
                        all_defs['states'][this_state_name] = copy.deepcopy(this_state)
            
            if "array_aliases" in state_defs[state_def_key]:        
                all_defs['state_aliases'].update(copy.deepcopy(state_defs[state_def_key]['array_aliases']))
                
            if "memories" in state_defs[state_def_key]:         
                all_defs['memories'].update(copy.deepcopy(state_defs[state_def_key]['memories']))
    
    # replace aliases
    all_defs_post_alias = {}
    all_defs_post_macros = {}
    all_defs_post_alias = replace_aliases_j(all_defs)
    
    # replace macros
    all_defs_post_macros = replace_macros(all_defs_post_alias)
    
    
           
    return all_defs_post_macros
    
    
#----------------------------------------------------------    
def extract_edits(edit_defs, kite_cfg):
    """read definition files and extract all defined edits"""
    
    for def_file in kite_cfg['file_descriptors']['kite_def_files']:
        print("*** Parsing definition input file : " +def_file)
        filename = def_file 
        filename = os.path.expandvars(filename)

        with open(filename, 'r') as input_file:
            this_edit_defs = json5.loads(kpfe_common.remove_continuations(input_file))
        
        for edit_def_key in this_edit_defs.keys():  
            if "edits" in this_edit_defs[edit_def_key].keys():
                edit_defs.extend(this_edit_defs[edit_def_key]['edits'])
    return edit_defs
    
#----------------------------------------------------------    
def fix_ms_indices(all_defs, kite_cfg):
    """fixup the index taking account of the offset calculation"""
    
    for state_name in all_defs['states'].keys():
        this_state = all_defs['states'][state_name]
        for view_name in this_state['views']:
            this_view = this_state['views'][view_name]
            if "array_aliases" in this_view.keys():
                for aidx, aalias in enumerate(this_view["array_aliases"]):
                    alias_def_name = this_view['array_aliases'][aidx]['alias']
                    alias_index = this_view['array_aliases'][aidx]['index']
                    # find alias length
                    alias_length = all_defs['state_aliases'][alias_def_name]['index_width']
                    # convert everything to integers and evaluate (if possible)
                    alias_type = "dec_int"
                    while True:
                        litmatch = re.search(r'(?<![a-zA-Z0-9_])(\d+)\'([d|h|b])([\d|x|X|a-f|A-F|\?]+)(?![a-zA-Z0-9_])',alias_index)
                        if litmatch:
                            lit_expr = litmatch.group(1)+"'"+litmatch.group(2)+litmatch.group(3)
                            is_literal, length, value = kpfe_common.extract_literal_int(lit_expr) 
                            if not is_literal:
                                sys.exit("Error. Could not convert literal in index")
                            alias_index = alias_index.replace(lit_expr, str(value))
                            if litmatch.group(2) == "d":
                                alias_type = "dec_sv"
                            elif litmatch.group(2) == "h":
                                alias_type = "hex_sv"
                            elif litmatch.group(2) == "d":
                                alias_type = "bin_sv"
                        else:
                            break
                    try:
                        index_eval = eval(alias_index)                        
                    except:
                        sys.exit("Error. could not evaluate alias index expression "+str(this_view['array_aliases'][aidx]['index'])+" in "+view_name)
                    if alias_type == "dec_int":
                        index_eval_formatted = str(index_eval)
                    elif alias_type == "dec_sv":
                        index_eval_formatted = str(alias_length)+"'d"+str(index_eval)
                    elif alias_type == "hex_sv":
                        index_eval_formatted = str(alias_length)+"'h"+(format(index_eval, 'x').upper())
                    elif alias_type == "bin_sv":
                        index_eval_formatted = str(alias_length)+"'b"+str(bin(index_eval).replace("0b", "").zfill(length))
                    
                    #print("replaced alias expression "+this_view['array_aliases'][aidx]['index']+" with "+index_eval_formatted)
                    all_defs['states'][state_name]['views'][view_name]['array_aliases'][aidx]['index'] = index_eval_formatted
                
    return all_defs       
    

#----------------------------------------------------------    
def i_range_to_map(i_range):
    """turn the i_range field into an expanded list"""
    i_range_list = i_range.split(',')
    i_list = []
    for group_idx, ibit in enumerate(reversed(i_range_list)):
        # starts from msb
        extracted, ibit_range = kpfe_common.extract_pcontents(["[","]"], ibit)
        irange_bits = ibit_range.split(':')
        if len(irange_bits) == 1:
            int(imax = irange_bits[0])
            int(imin = irange_bits[0]) 
            field_length = 1
        elif len(irange_bits) == 2:
            if(int(irange_bits[0]) >= int(irange_bits[1])):
                field_length = eval("("+irange_bits[0]+"-"+irange_bits[1]+"+1)")
                imax = int(irange_bits[0])
                imin = int(irange_bits[1])
            else:
                field_length = eval("("+irange_bits[1]+"-"+irange_bits[0]+"+1)")
                imax = int(irange_bits[1])
                imin = int(irange_bits[0])
        else:
            sys.exit("Error. Invalid number of ranges in "+i_range)
        tmp_list = []
        for i in range(imin, imax+1):
            tmp_list.append((i,group_idx))
        i_list.extend(tmp_list)
          
    return i_list  
             
#----------------------------------------------------------        
def get_isym_replacer(i_map, smin, smax):
    """return mapping to instruction"""  
    # check if i_map[smin] and i_map[smax] are in the same continuous assignment
    if(i_map[smin][1] == i_map[smax][1]):
        if i_map[smax][0] == i_map[smin][0]:
            val = "instruction["+str(i_map[smax][0])+"]"
        else:
            val = "instruction["+str(i_map[smax][0])+":"+str(i_map[smin][0])+"]"  
    else:
        # find discontinuities
        instr_bits = []
        curr_idx = i_map[smax][1]
        last_dc = smax
        for i in range(smax, smin-1, -1):
            if (not (i_map[i][1] == curr_idx)):
                if(i_map[last_dc][0] == i_map[i+1][0]):
                    instr_bits.append("instruction["+str(i_map[last_dc][0])+"]")
                else:
                    instr_bits.append("instruction["+str(i_map[last_dc][0])+":"+str(i_map[i+1][0])+"]")
                last_dc = i
                curr_idx = i_map[i][1]
        if(i_map[last_dc][0] == i_map[smin][0]):
            instr_bits.append("instruction["+str(i_map[last_dc][0])+"]")
        else:
            instr_bits.append("instruction["+str(i_map[last_dc][0])+":"+str(i_map[smin][0])+"]")
        val = ','.join(instr_bits)  
        val = "{"+val+"}"  
    return val

#----------------------------------------------------------     
def extract_symbols_from_encoding(instr_encoding):
    """ extract any encoding symbols from the instruction encoding"""
    current_bit_pos =  0
    field_desc = {}
    
    # extract encoding string from enclosing {} if any
    extracted, encoding_fields, start = kpfe_common.extract_pcontent_fields(["{","}"], ",", instr_encoding, 0)
    if not extracted:
        encoding_fields = []
    
    # start at the last field
    for field in reversed(encoding_fields):        
        lmatch = re.match(r'\S*(.+)\'[b|d|h].+', field)
        if lmatch :
            #literal
            field_length = int(lmatch.group(1))
        else:
            f_extracted, frange = kpfe_common.extract_pcontents(["[","]"], field)
            if f_extracted:
                frange_bits = frange.split(':')
                field_name = field.split('[')[0]
                if(len(frange_bits) == 2):
                    field_length = int(frange_bits[0])-int(frange_bits[1])+1
                    field_frange = (int(frange_bits[0]),int(frange_bits[1]))
                elif(len(frange_bits) == 1):
                    field_length = 1
                    field_frange = (int(frange_bits[0]),int(frange_bits[0]))
                else:
                    sys.exit("invalid number of indices in symbol index")
                    
            else:
                field_length = 1
                field_name = field
                field_frange = (0,0)
            field_irange = ((current_bit_pos+field_length-1),current_bit_pos) 
            field_name = field_name.lstrip().rstrip()
            
            field_desc_bits = {}
            if(field_name in field_desc.keys()):
                field_desc[field_name]['length'].append(field_length) 
                field_desc[field_name]['f_range'].append(field_frange) 
                field_desc[field_name]['i_range'].append(field_irange)
                
            else :
                field_desc_bits['length'] = []
                field_desc_bits['length'].append(field_length)
                field_desc_bits['f_range'] = []
                field_desc_bits['f_range'].append(field_frange)
                field_desc_bits['i_range'] = []
                field_desc_bits['i_range'].append(field_irange)
                
                field_desc[field_name] = copy.deepcopy(field_desc_bits)
                
        current_bit_pos = current_bit_pos + field_length
        
        
    merged_desc = {}       
    #merge part symbols
    for name in field_desc.keys():
        if len(field_desc[name]['f_range']) > 1:
            # merge fields, sort by f_range[msb]
            sort_complete = 0
            while sort_complete == 0:
                swapped_something = 0
                for elem_idx, elem in enumerate(field_desc[name]['f_range']):
                    for cmp_elem_idx, cmp_elem in enumerate(field_desc[name]['f_range']):
                        if(elem > cmp_elem) & (elem_idx > cmp_elem_idx):
                            # swap elements
                            tmp_cmp_length = field_desc[name]['length'][cmp_elem_idx]
                            tmp_cmp_frange = field_desc[name]['f_range'][cmp_elem_idx]
                            tmp_cmp_irange = field_desc[name]['i_range'][cmp_elem_idx]
                            tmp_length_list = field_desc[name]['length']
                            tmp_frange_list = field_desc[name]['f_range']
                            tmp_irange_list = field_desc[name]['i_range']
                            tmp_length_list[cmp_elem_idx] = tmp_length_list[elem_idx]
                            tmp_frange_list[cmp_elem_idx] = tmp_frange_list[elem_idx]
                            tmp_irange_list[cmp_elem_idx] = tmp_irange_list[elem_idx]
                            tmp_length_list[elem_idx] = tmp_cmp_length
                            tmp_frange_list[elem_idx] = tmp_cmp_frange
                            tmp_irange_list[elem_idx] = tmp_cmp_irange
                            
                            swapped_something = 1
                if swapped_something == 0 :
                    sort_complete = 1 
            merged_length = sum(field_desc[name]['length'])
            merged_f_range = ( (field_desc[name]['f_range'][0][0]),(field_desc[name]['f_range'][-1][1]))
            merged_i_range = "{"
            for irange_elem_idx, irange_elem in enumerate(field_desc[name]['i_range']):
                merged_i_range = merged_i_range + "instruction["+str(irange_elem[0])+":"+str(irange_elem[1])+"]" 
                if(irange_elem_idx < len(field_desc[name]['i_range'])-1):
                    merged_i_range = merged_i_range + ","
            merged_i_range = merged_i_range + "}"
        else:
            # no merging needed
            merged_length = field_desc[name]['length'][0]
            merged_f_range = field_desc[name]['f_range'][0]
            merged_i_range = "instruction["+str(field_desc[name]['i_range'][0][0])+":"+str(field_desc[name]['i_range'][0][1])+"]"
        merged_desc_bits = {}   
        merged_desc_bits['length'] = merged_length
        merged_desc_bits['f_range'] = merged_f_range
        merged_desc_bits['i_range'] = merged_i_range
        merged_desc_bits['i_map'] = i_range_to_map(merged_i_range)
        merged_desc[name] = merged_desc_bits
        
    return merged_desc

#----------------------------------------------------------  
def replace_symbols_in_string(update_string, merged_desc):
    """" replace the symbols in the string """
    
    for symbol in merged_desc.keys():
        found_all_symbols = 0
        while found_all_symbols == 0:
            found_a_symbol = 0        
            s_match = re.match(r'.*'+symbol+'\[(.+?)\]+.*',update_string)
            if s_match:                
                sym_range = s_match.group(1)
                sym_range_bits = sym_range.split(':')
                if len(sym_range_bits) == 1:
                    #smax = int(sym_range_bits[0]) + merged_desc[symbol]['f_range'][1]
                    smax = int(sym_range_bits[0])
                    smin = int(sym_range_bits[0]) 
                    field_length = 1
                elif len(sym_range_bits) == 2:
                    if(int(sym_range_bits[0]) >= int(sym_range_bits[1])):
                        field_length = eval("("+sym_range_bits[0]+"-"+sym_range_bits[1]+"+1)")
                        smax = int(sym_range_bits[0])
                        smin = int(sym_range_bits[1])
                    else:
                        field_length = eval("("+sym_range_bits[1]+"-"+sym_range_bits[0]+"+1)")
                        smax = int(sym_range_bits[1])
                        smin = int(sym_range_bits[0])
                else:
                    sys.exit("Error. Invalid number of range fields in "+sym_range)
                
                smax = smax - merged_desc[symbol]['f_range'][1]
                smin = smin - merged_desc[symbol]['f_range'][1]
                # merged field can be discontinous, so check and return concatenation if necessary
                replacer = get_isym_replacer(merged_desc[symbol]['i_map'], smin, smax)                
                replacee = symbol+"["+s_match.group(1)+"]"   
                update_string = update_string.replace(replacee,replacer)
                found_a_symbol = 1
            s_match = re.match(r'.*'+symbol+'\s*[^\[].*',update_string)
                    
            if found_a_symbol == 0:
                found_all_symbols = 1
    return update_string             

#---------------------------------------------------------- 
def replace_encoding_symbols(all_defs):
    """replaces the symbols in the instruction definition with the instruction fields"""
    
    for instruction in all_defs['instructions']:
        # extract symbol fields from instruction encoding      
        merged_desc = extract_symbols_from_encoding(all_defs['instructions'][instruction]['encoding'])
                
        # search instruction description (match, exceptions, retire, interrupts, reset) for instruction symbols and replace
        # with instruction field        
        desc_to_check = ['locals','match','exceptions','retire','interrupt','reset']
        for update_type in all_defs['instructions'][instruction].keys():
        #for update_type in desc_to_check:
            if update_type in desc_to_check:
                update_string = json5.dumps(all_defs['instructions'][instruction][update_type])
                update_string =replace_symbols_in_string(update_string, merged_desc)
                all_defs['instructions'][instruction][update_type] = copy.deepcopy(json5.loads(update_string))                                 
        
    return all_defs        
            

#----------------------------------------------------------    
def extract_instructions(all_defs, kite_cfg):
    """read instruction files and extract all defined instructions"""
    
    for def_file in kite_cfg['file_descriptors']['kite_def_files']:
        print("*** Parsing definition input file : " +def_file)
        filename = def_file 
        filename = os.path.expandvars(filename)

        with open(filename, 'r') as input_file:
            instr_defs = json5.loads(kpfe_common.remove_continuations(input_file))
            
        for instr_def_key in instr_defs.keys():
            if "instructions" in instr_defs[instr_def_key]:
                all_defs['instructions'].update(copy.deepcopy(instr_defs[instr_def_key]['instructions']))
    
    # replace aliases
    all_defs_post_alias = {}
    all_defs_post_macros = {}
    all_defs_post_symbols = {}
    all_defs_post_alias = replace_aliases_j(all_defs)
    
    # replace macros
    all_defs_post_macros = replace_macros(all_defs_post_alias)
    
    # replace/extract encoding symbols
    all_defs_post_symbols = replace_encoding_symbols(all_defs_post_macros)

    return all_defs_post_symbols
    
#----------------------------------------------------------    
def extract_sa_from_string(all_defs, a_string, local_postfix, local_idx, instruction_name):
    """searches string for state aliases, removes them and returns task list, local desc, and fixed string"""
    
    # search the string for any state alias reads defined in all_defs['state_alias']
    # a 'state_alias' read will be of the form alias_name[alias_index]
    #
    
    sa_list = []
    local_desc = {}
    fixed_string = a_string
    marker_idx = 0
    marker_list = []
    
    all_done = False
    while not all_done:
        sa_index, sa_name, sa_repl = kpfe_common.extract_sa_term(all_defs, fixed_string)
        if sa_name == "":
            # might be done, if no stat aliases left in any other bits of the term
            sa_found = kpfe_common.find_state_alias(all_defs, fixed_string)
            if not (sa_found == ""):
                marker_desc = {}
                #replace the last term found with a marker so it's not found again - otherwise will loop forever!
                curr_marker = "***marker_"+str(marker_idx)+"***"
                replacee = "["+sa_index+"]"
                fixed_string = fixed_string.replace(replacee,curr_marker)
                marker_desc['marker'] = curr_marker 
                marker_desc['replaces'] = replacee
                marker_idx = marker_idx + 1
                marker_list.append(marker_desc)
            else:
                all_done = True
        else:
            # create sa locals
            # fix up any markers before adding the index
            for marker in marker_list:
                sa_index = sa_index.replace(marker['marker'], marker['replaces']) 
            l_dims = all_defs['state_aliases'][sa_name]['sub_dimensions']
            if l_dims == "":
                sys.exit("when creating state alias read local, could not find corresponding sub dimensions for "+sa_name) 
            ls_desc = {}
            ls_name = sa_name+local_postfix+str(local_idx)
            ls_desc['sa_name'] = sa_name
            ls_desc['dimensions'] = l_dims
            ls_desc['index'] = sa_index
            ls_desc['type'] = "state_alias_read_var"
            #ls_desc['instruction'] = instruction_name
            ls_desc['default'] = ""
            local_desc[ls_name] = copy.deepcopy(ls_desc)
            local_idx = local_idx+1
            
            # fix-up the read term 
            # replace the whole sa read with the local signal only
            fixed_string =  fixed_string.replace(sa_repl,ls_name)
    
    # fix up the markers 
    for marker in marker_list:
        fixed_string = fixed_string.replace(marker['marker'], marker['replaces']) 
    return local_desc, fixed_string, local_idx

    
#----------------------------------------------------------
def build_alias_map(all_defs, kite_cfg): 
    """for each state alias build the mapping function to the underlying state"""
    
    print("building sa map...")
    
    # initially build templates for accessing each alias defined
    # then seach in definitions for all allias read references (write references are 
    # seperately resolved to the underlying state or memory)
    # create an instance of the template as an alias local signal and replace the 
    # read reference in the definition with the alias local signal
    
    # find alias descriptions in state declarations
    # replace/create array alias mappings 
    for curr_state in all_defs['states'].keys():
        for curr_view in all_defs['states'][curr_state]['views'].keys():
            if("array_aliases" in all_defs['states'][curr_state]['views'][curr_view]):
                for aaa in all_defs['states'][curr_state]['views'][curr_view]['array_aliases']:
                    array_alias_name = aaa['alias'] 
                    array_alias_index = aaa['index']
                    # check that the specified alias is in the set of defined array_aliases
                    if not(array_alias_name in all_defs['state_aliases'].keys()):  
                        sys.exit("cannot find definition for array alias "+array_alias_name)
                        
                    # check that the index isn't already in the map (no duplicates allowed)
                    if 'mapping' in all_defs['state_aliases'][array_alias_name]:
                        for mapping in all_defs['state_aliases'][array_alias_name]['mapping']:
                            map_bits = mapping.split("<->")
                            extracted, map_index = kpfe_common.extract_pcontents(["[","]"], map_bits[1])
                            # REVISIT evaluate the index
                            if(array_alias_index == map_index):
                                sys.exit("Array alias index "+str(array_alias_index)+" already defined for "+curr_view)
                
                    new_map = curr_view+"<->"+array_alias_name+"["+array_alias_index+"]"
                    if 'mapping' in all_defs['state_aliases'][array_alias_name].keys():
                        all_defs['state_aliases'][array_alias_name]['mapping'].append(new_map)
                    else:
                        all_defs['state_aliases'][array_alias_name]['mapping'] = [new_map]
                    
                    print(" added alias "+new_map)
    
    alias_template_desc = {}
    
    # add to all_defs
    for sa in all_defs['state_aliases'].keys():
        print("-"*50)
        print("State alias mappings for "+sa)
        for sa_mapping in all_defs['state_aliases'][sa]['mapping']:
            print(sa_mapping)   
        print("-"*50)
    
        # build template (set of conditional actions)
        condaction_list = []
        for mapping in all_defs['state_aliases'][sa]['mapping']:
            map_bits = mapping.split("<->")
            ustate = map_bits[0].rstrip().lstrip()
            i_extracted, index = kpfe_common.extract_pcontents(["[","]"], map_bits[1])
            if not i_extracted:
                sys.exit("Error. Could not extract index for alias "+sa+", state "+ustate)
            # if the index is specified as an integer, fix it up to be an SV vector of length
            # all_defs['state_aliases'][sa]['index_width'] 
            limatch = re.match(r'^(\d+)$',index)
            if limatch:
                length = all_defs['state_aliases'][sa]['index_width']
                index = str(length)+"'d"+index
            
            condition_action = {}
            condition_action['index_condition'] = ""
            condition_action['condition'] = "***index*** == "+index
            condition_action['action'] = ustate
            condition_action['cause'] = {'instruction': "", 'update_type': "state alias mapping"}
            condaction_list.append(condition_action)
        alias_template_desc[sa] = copy.deepcopy(condaction_list)
    all_defs['state_alias_templates'] = alias_template_desc
    
    print("built alias templates...")
    
    # search for alias reads in all places state aliases can be used:
    #   instructions 
    #   global signals  
    #   local signal
    #   task calls
    # if found, replace each instance with a unique sa_local signal
    
    # search instructions
    desc_to_check = ['locals','match','exceptions','retire','interrupt','reset']
    for instruction_name in all_defs['instructions'].keys():  
        instruction = all_defs['instructions'][instruction_name]
        local_postfix = "_"+instruction_name.lower()+"_kite_salocal_sig_"
        local_idx = 0
        for update_type in instruction.keys():
            # locals sections is organised differently to others           
            if update_type == "locals":
                # tasks can be in the assignments list in the update_to_value or condition parts
                for aidx, assignment in enumerate(instruction['locals']['assignments']):
                    for apart in assignment.keys():
                        if (apart == 'condition') | (apart == 'update_to_value'): 
                            a_locals = [] 
                            a_locals, all_defs['instructions'][instruction_name]['locals']['assignments'][aidx][apart], local_idx  = \
                                    extract_sa_from_string(all_defs, assignment[apart], local_postfix, local_idx, instruction_name)
                            all_defs['local_sigs'].update(a_locals)   
                        
            elif (update_type in desc_to_check):
                # tasks can be in the condition list, or in the in the state_action_list update_to_value or condition parts
                for uidx, updater in enumerate(instruction[update_type]):
                    if "condition" in updater.keys():
                        a_locals = [] 
                        a_locals, all_defs['instructions'][instruction_name][update_type][uidx]['condition'], local_idx  = \
                                extract_sa_from_string(all_defs, updater['condition'], local_postfix, local_idx, instruction_name)
                        all_defs['local_sigs'].update(a_locals) 
                
                    for sidx, state_action in enumerate(updater['state_action_list']):
                        for apart in state_action.keys():
                            if (apart == 'condition') | (apart == 'update_to_value'): 
                                a_locals = []
                                a_locals, all_defs['instructions'][instruction_name][update_type][uidx]['state_action_list'][sidx][apart], local_idx  = \
                                        extract_sa_from_string(all_defs, state_action[apart], local_postfix, local_idx, instruction_name) 
                                all_defs['local_sigs'].update(a_locals)                           

    local_idx = 0
    for tidx, tc in enumerate(all_defs['task_calls']):
        print("checking for state aliases in task call "+tc)
        
#        tmatch = re.match(r'(.+)\(.+\)', tc)
#        if tmatch:
#            tc_name = tmatch.group(1) 
#        else:
#            sys.exit("Could not extract task name from string (state alias removal) "+tc)
            
            
        extracted, extracted_call, start, end = kpfe_common.extract_pcontents_pos(["(",")"], tc)
        if extracted:
            tc_name =  tc[:start].lstrip().rstrip()
            # check the extracted name is a defined task
            if not tc_name in all_defs['tasks'].keys():
                sys.exit(tc_name+" is not a defined task (extracted from "+tc+")")
        else:
            sys.exit("Could not extract task name from string (state alias removal) "+tc)   
        
        local_postfix = "_"+tc_name+"_kite_salocal_sig_"
        a_locals = [] 
        a_locals, all_defs['task_calls'][tidx], local_idx  = \
                extract_sa_from_string(all_defs, tc, local_postfix, local_idx, tc_name)
        all_defs['local_sigs'].update(a_locals)
        
        
    return all_defs

#----------------------------------------------------------    
def replace_global_locals(instruction,local_sigs_desc):
    """ replace global local signals """
    
    # replace local signal names with the new global version
    # in:
    #   - any condition
    #   - any state_update 
    #
    # need to be careful in task/macro calls not to replace the task/macro input/output name!
    
    desc_to_check = ['match','exceptions','retire','interrupt','reset']
    for update_type in instruction.keys():
        # locals sections is organised differently to others           
            if update_type == "locals":
                for aidx, assignment in enumerate(instruction['locals']['assignments']):
                    for apart in assignment.keys():
                        for local_sig in local_sigs_desc.keys():
                            instruction['locals']['assignments'][aidx][apart] = \
                                     re.sub(r'(?<![a-zA-Z0-9_])'+local_sigs_desc[local_sig]['local_name']+'(?!\s*\=[^\=]|[a-zA-Z0-9_])',local_sig,instruction['locals']['assignments'][aidx][apart])
                            
            elif (update_type in desc_to_check):
                for uidx, updater in enumerate(instruction[update_type]):
                    if "condition" in updater.keys():
                        for local_sig in local_sigs_desc.keys():
                            instruction[update_type][uidx]['condition'] = \
                                     re.sub(r'(?<![a-zA-Z0-9_])'+local_sigs_desc[local_sig]['local_name']+'(?!\s*\=[^\=]|[a-zA-Z0-9_])',local_sig,instruction[update_type][uidx]['condition'])

                    if "state_action_list" in updater.keys():
                        for sidx, state_action in enumerate(updater['state_action_list']):
                            for apart in state_action.keys():
                                for local_sig in local_sigs_desc.keys():
                                    instruction[update_type][uidx]['state_action_list'][sidx][apart] = \
                                            re.sub(r'(?<![a-zA-Z0-9_])'+local_sigs_desc[local_sig]['local_name']+'(?!\s*\=[^\=]|[a-zA-Z0-9_])',\
                                                   local_sig,instruction[update_type][uidx]['state_action_list'][sidx][apart])
                                       
                                
    return instruction    
    
#----------------------------------------------------------
def build_locals(all_defs, kite_cfg):
    """find local signal definitions"""
    
    # local signals can only be defined in instructions
    # local signals can be defined explicitly in the "locals" section, or implictly in
    # a task call eg asig:=f_do_something(arg0, arg1, asig) will automatically generate
    # a local signal - these are extracted in extract_task_calls
    
    local_sigs = {}
    
    # check for exlicitly declared signals
    for instruction_name in all_defs['instructions'].keys():  
        instruction = all_defs['instructions'][instruction_name]
        instr_local_sigs = {}
        lsig_postfix = "_"+instruction_name.lower()+"_kite_local_sig"
        if('locals' in instruction.keys()):
            for ldef in instruction['locals']['definitions']:
                # add in same form as global sigs
                ls_desc = {}
                ls_name = ldef['def_name']+lsig_postfix
                ls_desc['local_name'] = ldef['def_name']
                ls_desc['dimensions'] = ldef['dimensions']
                ls_desc['type'] = "local_var"
                ls_desc['instruction'] = instruction_name
                ls_desc['default'] = ldef['default']
                instr_local_sigs[ls_name] = copy.deepcopy(ls_desc)
                local_sigs[ls_name] = copy.deepcopy(ls_desc)
                
        # replace local signals with the new 'global' local signal names        
        all_defs['instructions'][instruction_name] = replace_global_locals(all_defs['instructions'][instruction_name],instr_local_sigs)
       
    all_defs['local_sigs'] = copy.deepcopy(local_sigs)       
    # replace aliases
    all_defs_post_alias = {}
    all_defs_post_macros = {}
    all_defs_post_lsigs = {}
    all_defs_post_alias = replace_aliases_j(all_defs)
    
    # replace macros
    all_defs_post_macros = replace_macros(all_defs_post_alias)    
    
    return all_defs_post_macros
    
#----------------------------------------------------------     
def cleanup_task_call_terms(call_term):
    """cleans up the task call"""
    map_list = []
    # arguments are comma delimited and mapped with =
    # care required as argumnets could be concatenations or similar so only split at bottom level
    t_extracted, term_args, start = kpfe_common.extract_pcontent_fields([], ",", call_term, 0)
#    term_args = call_term.split(',')
    for term_arg in term_args:
        map_term = ""
        arg_map = term_arg.split('=',1)
        if not(len(arg_map) == 2):
            sys.exit("Error. could not cleanup "+term_arg+" in call term "+call_term)
        map_assgn = arg_map[1].lstrip().rstrip()
        # check if had parenthesis to start with
        started_with_parenthesis = False 
        ipmatch = re.match(r'^\(.+\)$',map_assgn)  
        if ipmatch:
            started_with_parenthesis = True      
        map_assgn = map_assgn.replace("true","1'b1")
        map_assgn = map_assgn.replace("false","1'b0")
        map_assgn = kpfe_st_opt.remove_redundant_terms(map_assgn)
        # remove extra parenthesis at start and end (only)
        pmatch = re.match(r'^\((.+)\)$',map_assgn)
        if pmatch:
            #if not(started_with_parenthesis):
            map_assgn = pmatch.group(1)
        map_term  = arg_map[0].lstrip().rstrip()+"="+map_assgn
        map_list.append(map_term)
    
    new_call_term = ','.join(map_list)
    
    return new_call_term
        
#----------------------------------------------------------    
def extract_mem_refs_from_string(all_defs, a_string, local_prefix, local_idx):
    """searches the string for any memory references and replaces them with local signals"""
    ref_local_sigs = {}
    all_done = False
    for mem in all_defs['memories']:
        search_string = a_string
        while not all_done:
            found_mem_ref = False        
            if mem in search_string:
                start_of_ref = search_string.find(mem)
                end_of_ref = start_of_ref+len(mem)
                found_mem_ref = True
                # add a character for the test in case atart_of_ref is right at the start
                if start_of_ref == 0:   
                    rtest_string = " "+search_string
                else:
                    rtest_string = search_string[start_of_ref-1:] 
                rmatch = re.match(r'[^a-zA-Z0-9_]'+mem+'\s*\[.*',rtest_string)
                if rmatch:
                    extracted, extracted_ref_term, start_pos, end_pos = kpfe_common.extract_pcontents_pos(["[","]"], search_string[start_of_ref:])
                    if not extracted:
                        sys.exit("Error. Could not extract memory reference term in "+search_string)
                    # replace ref with a new local signal
                    replacee = mem+"["+extracted_ref_term+"]"
                    ls_desc = {}
                    ls_name = local_prefix+mem+"_"+str(local_idx)
                    local_idx = local_idx + 1
                    ls_desc['dimensions'] = all_defs['memories'][mem]['sub_dimensions']
                    ls_desc['type'] = "memref_var"
                    ls_desc['instruction'] = mem
                    ls_desc['default'] = ""
                    ls_desc['assignment'] = [{"update_to_value" : replacee,
                                              "condition" : "1'b1"}]
                    ref_local_sigs[ls_name] = copy.deepcopy(ls_desc)

                    a_string = a_string.replace(replacee,ls_name) 
                    
                search_string = search_string[end_of_ref:]
            if not found_mem_ref:
                all_done = True
            
    return a_string, ref_local_sigs, local_idx    
    
#----------------------------------------------------------    
def find_im_slc_expr(term):
    """ Find the innermost non-signal slice """
    
    found_ns_slice = False
    ns_expr = ""
    ns_expr_start = 0
    ns_expr_end = 0 
    string_start = 0
    
    all_done = False
    while not all_done:    
        extracted, extracted_term, start, end = kpfe_common.extract_pcontents_pos(["(",")"], term)
        if extracted:
            if (len(term) > end+1):
                if term[end+1] == "[":
                    found_ns_slice = True
                    # check for any more
                    next_found_ns_slice, next_ns_expr, next_ns_expr_start, next_ns_expr_end = find_im_slc_expr(extracted_term)
                    if  next_found_ns_slice:
                        ns_expr = next_ns_expr
                        ns_expr_start = next_ns_expr_start
                        ns_expr_end = next_ns_expr_end 
                    else:
                        ns_expr = extracted_term
                        ns_expr_start = start
                        ns_expr_end = end 
                    all_done = True
            if not found_ns_slice:
                # check there is nothing more to extract in the extracted term
                next_found_ns_slice, next_ns_expr, next_ns_expr_start, next_ns_expr_end = find_im_slc_expr(extracted_term)
                if  next_found_ns_slice:
                    found_ns_slice = True
                    ns_expr = next_ns_expr
                    ns_expr_start = next_ns_expr_start
                    ns_expr_end = next_ns_expr_end 
                    all_done = True
                else:
                    term = term[end+1:] 
        else:
            all_done = True
            
    return found_ns_slice, ns_expr, ns_expr_start, ns_expr_end
    
#----------------------------------------------------------    
def extract_ns_slices_from_string(all_defs, a_string, local_prefix, local_idx, bi_map):
    """searches the string for any non-signals with slices and replaces them with local signals"""
    
    # search for "(<expression>)[<slice>]"
    
    ref_local_sigs = {}
    all_done = False
    modified_ns_slice = False
    
    search_string = a_string
    while not all_done:
        found_ns_slice, ns_expr, ns_expr_start, ns_expr_end = find_im_slc_expr(a_string)      
        if found_ns_slice:
            modified_ns_slice = True
            #print("found ns slice expr "+ns_expr)
            # if the expression is a single term, just remove parenthesis
            replacee = "("+ns_expr+")"
            ematch = re.match(r'^[a-zA-Z0-9_]+$',ns_expr)
            if ematch:
                #print("just removing parenthesis")
                replacer = ns_expr               
            # otherwise replace the expression with a new_local_signal
            else:  
                #print("replacing with a local")              
                # need to find length of the expression to create new signal
                ns_expr_len = kpfe_common.find_term_length(all_defs, ns_expr, bi_map)
                ls_desc = {}
                ls_name = local_prefix+str(local_idx)
                local_idx = local_idx + 1
                ls_desc['dimensions'] = "["+str(ns_expr_len-1)+":0]"
                ls_desc['type'] = "ns_slice_local_var"
                ls_desc['instruction'] = ""
                ls_desc['default'] = ""
                ls_desc['assignment'] = [{"update_to_value" : ns_expr,
                                          "condition" : "1'b1"}]
                ref_local_sigs[ls_name] = copy.deepcopy(ls_desc)
            
                replacer = ls_name  
            a_string = a_string.replace(replacee,replacer)     
        else:
            all_done = True 
                
        
    return a_string, ref_local_sigs, local_idx, modified_ns_slice 
    
#----------------------------------------------------------    
def extract_ns_slices(all_defs, kite_cfg, bi_map):
    """extract and replace non-signal slices"""
    
    # Kite SA allows expressions to be defined as slices of more complex expressions
    # as long as those expressions are in parenthesis
    # This is not legal SystemVerilog (the Kite SA is nominally SV) and therefore complicates
    # SV generation.
    # -If the expression has already been replaced with a local signal (such as state aliases) and there 
    # are no other expressions, simply remove the parenthesis.
    # -If the expression is more complex, replace it with a new local signal representing that expression
    
    
    new_local_sigs = {}
    local_idx = 0
    desc_to_check = ['match','exceptions','retire','interrupt','reset']

    print("Extracting and replacing non-signal slices...")
    
    for item_type in all_defs.keys():
        if item_type == "instructions":
            for instruction_name in all_defs['instructions'].keys(): 
                print("extracting ns slices from instruction "+instruction_name)
                local_idx = 0 
                instruction = all_defs['instructions'][instruction_name]
                local_prefix = "kite_ns_slice_instr_local_sig_"+instruction_name.lower()+"_"
                #turn defs back into a string, simpler to make replacements without worrying about the structure
                def_string = json5.dumps(instruction)
                def_string, ref_local_sigs, local_idx, modified_ns_slice = extract_ns_slices_from_string(all_defs, def_string, local_prefix, local_idx, bi_map)
                if len(ref_local_sigs) > 0:
                    new_local_sigs.update(ref_local_sigs)
                if modified_ns_slice:
                    all_defs['instructions'][instruction_name] = copy.deepcopy(json5.loads(def_string))    
        if item_type == "task_calls":  
            for tcidx, tc in enumerate(all_defs['task_calls']): 
                extracted, extracted_call, start, end = kpfe_common.extract_pcontents_pos(["(",")"], tc)
                if extracted:
                    tc_name =  tc[:start].lstrip().rstrip()
                    # check the extracted name is a defined task
                    if not tc_name in all_defs['tasks'].keys():
                        sys.exit(tc_name+" is not a defined task (extracted from "+tc+")")
                    print("extracting ns slices from tc_call "+tc)
                else:
                    sys.exit("Could not extract task name from string (state alias removal) "+tc) 
                local_idx = 0 
                local_prefix = "kite_ns_slice_tc_local_sig_"+tc_name.lower()+"_"
                #turn defs back into a string, simpler to make replacements without worrying about the structure
                def_string = json5.dumps(tc)
                def_string, ref_local_sigs, local_idx, modified_ns_slice = extract_ns_slices_from_string(all_defs, def_string, local_prefix, local_idx, bi_map)
                if len(ref_local_sigs) > 0:
                    new_local_sigs.update(ref_local_sigs)
                if modified_ns_slice:
                    all_defs['task_calls'][tcidx] = copy.deepcopy(json5.loads(def_string))  
        
    if len(new_local_sigs) > 0:
        all_defs['local_sigs'].update(new_local_sigs)
             
    return all_defs

#----------------------------------------------------------    
def extract_tasks_from_string(all_defs, a_string, bi_ops_regex, local_postfix, local_idx, is_condition, instruction_name):
    """searches string for tasks, removes them and returns task list, local desc, and fixed string"""
    
    # search the string for any task defined in all_defs['tasks']
    # a task call will be of the form {asig,bsig}:=a_task(input1=arg1, input2=arg2, output1=asig, output2=bsig)
    # in this case the entire expression will be replaced by asig,bsig if asig,bsig are already locals
    # or new implicit local signals names if they are not
    # in the former case (outputs are locals):
    #   - the task including all arguments and mappings, will be added to the task_list
    #   - the string will be modified to replace the entire task call with the locals only 
    # in the latter case (outputs are not defined as locals)
    #   - the task including all arguments and mappings, will be added to the task_list with the output 
    #     arguments replaced with the new implicit locals
    #   - the string will be modified to replace the entire task call with the new implicit locals only
    #   - the new implicit locals will be added to the task list
    #
    # note that a task call may include another task call as an input argument, therefore need to search
    # recursively from the inside out until done
    
    task_list = []
    local_desc = {}
    fixed_string = a_string
    marker_idx = 0
    marker_list = []
    last_task_asmt = ""
    
    all_done = False
    
    while not all_done:
        call_term, task_name, task_asmt, task_repl = kpfe_common.extract_inner_term(all_defs, fixed_string, bi_ops_regex)
        orig_call_term = call_term
        if task_name == "":
            # might be done, if no tasks left in any other bits of the term
            task_name_list = kpfe_common.find_task(all_defs, fixed_string)
            if len(task_name_list) > 0:
                marker_desc = {}
                #replace the last term found with a marker so it's not found again - otherwise will loop forever!
                curr_marker = "***marker_"+str(marker_idx)+"***"
                replacee = "("+call_term+")"
                fixed_string = fixed_string.replace(replacee,curr_marker)
                marker_desc['marker'] = curr_marker 
                marker_desc['replaces'] = replacee
                marker_idx = marker_idx + 1
                marker_list.append(marker_desc)
            else:
                all_done = True
        else:
            # find the associated outputs the task_asmt may contain multiple assignments                
            # tasks may assign multiple terms, if so they must be defined as a concatenation
            last_task_asmt = task_asmt
            orig_asmt = task_asmt
            concat_extracted, concat_asmt = kpfe_common.extract_pcontents(["{","}"], task_asmt)
            if concat_extracted:
                task_asmt = concat_asmt  
            asmt_bits = task_asmt.split(",")
            if is_condition & (len(asmt_bits) > 1):
                # can only be one assignment for a condition; check this
                sys.exit("Error. More than one assignment for a condition in "+a_string)
            if (not concat_extracted) & (len(asmt_bits) > 1):
                # can only be one assignment if not a concatenation
                sys.exit("Error. More than one assignment for a non-concatenated term "+task_asmt)                
            
            for abit_idx, asmt_bit in enumerate(asmt_bits):
                curr_sig = asmt_bit.lstrip().rstrip()
                # check these match the ones in the mapping
                if curr_sig not in call_term:
                    sys.exit("Error. task assignment signal "+curr_sig+" is not in task call "+call_term)                   
                
                # create implicit locals, then later add a conditional action to assign the
                # new implicit locals to the defined signal (could be a global, subfield, state, memory or local)      
                # use dimensions of corresponding defined local field to determine the dimensions of the 
                # implicit signal
                l_dims = kpfe_common.find_sig_dims(all_defs, curr_sig)
                if l_dims == "":
                    sys.exit("when create taskcall local, could not find corresponding assignment dimensions for "+curr_sig) 
                ls_desc = {}
                ls_name = curr_sig+local_postfix+str(local_idx)
                ls_desc['local_name'] = curr_sig
                ls_desc['dimensions'] = l_dims
                ls_desc['type'] = "taskcall_local_var"
                ls_desc['instruction'] = instruction_name
                ls_desc['default'] = ""
                local_desc[ls_name] = copy.deepcopy(ls_desc)
                local_idx = local_idx+1
                ###print("added taskcall_local_var "+ls_name+ " with decription "+str(ls_desc))
                
                # fix-up the task call term 
                call_term = re.sub(r'(?<![a-zA-Z0-9_])'+curr_sig+'(?!\s*\=[^\=]|[a-zA-Z0-9_])',ls_name,call_term)
                #call_term = call_term.replace(curr_sig,ls_name)
                # fix-up the task assignments
                orig_asmt = orig_asmt.replace(curr_sig,ls_name)                
            
            # replace the whole task call with the assignment term only
            fixed_string =  fixed_string.replace(task_repl,'')
            fixed_string =  fixed_string.replace(curr_sig,ls_name)

            # fix up the markers in the call term
            while True:
                for marker in reversed(marker_list):
                    call_term = call_term.replace(marker['marker'], marker['replaces'])
                mmatch = re.search(r'(\*{3}marker_\d+\*{3})',call_term)
                if mmatch:
                    #check if the remaining marker found actually exists in the replacement list
                    if not mmatch.group(1) in marker_list:
                        sys.exit("unknown marker in string "+call_term)
                else:
                    break

            call_term = cleanup_task_call_terms(call_term)
            # add the task call to the list
            task_list.append(task_name+"("+call_term+")")
    
    # fix up the markers 
    while True:
        for marker in reversed(marker_list):
            fixed_string = fixed_string.replace(marker['marker'], marker['replaces']) 
        mmatch = re.search(r'(\*{3}marker_\d+\*{3})',fixed_string)
        if mmatch:
            #check if the remaining marker found actually exists in the replacement list
            if not mmatch.group(1) in marker_list:
                sys.exit("unknown marker in string "+fixed_string)
        else:
            break

    return task_list, last_task_asmt, local_desc, fixed_string, local_idx

#----------------------------------------------------------
def get_task_arg_dims(all_defs, this_arg):

    extracted, dfield = kpfe_common.extract_pcontents(["(",")"], this_arg)
    for state in all_defs['states']:
        if dfield in all_defs['states'][state]['views'].keys():
            dims = all_defs['states'][state]['views'][dfield]['dimensions']
        if dfield in all_defs['states'][state]['subfields'].keys():
            dims = all_defs['states'][state]['subfields'][dfield]['dimensions']
    if dfield in all_defs['state_aliases'].keys():
        dims = all_defs['state_aliases'][dfield]['sub_dimensions']
    if dfield in all_defs['global_sigs'].keys():
        dims = all_defs['global_sigs']['dimensions']
    if dfield in all_defs['local_sigs'].keys():
        dims = all_defs['local_sigs']['dimensions']
    if dfield in all_defs['memories'].keys():
        dims = all_defs['memories']['dimensions']  
    if dfield in all_defs['enums'].keys():
        dims = all_defs['enums'][dfield]['dimensions']
    return dims
#----------------------------------------------------------    
def replace_task_dims(all_defs, kite_cfg):

    for task_def in all_defs['tasks'].keys():
        this_task = all_defs['tasks'][task_def]  
        # check if any of the args have the dimensions specified as $size
        for arg in this_task['task_args'].keys():
            this_arg = this_task['task_args'][arg]
            if this_arg.startswith("$size"):
                dims = get_task_arg_dims(all_defs, this_arg)
                all_defs['tasks'][task_def]['task_args'][arg] = dims
        # check if any of the args have the dimensions specified as $size
        for arg in this_task['task_return'].keys():
            this_arg = this_task['task_return'][arg]
            if this_arg.startswith("$size"):
                dims = get_task_arg_dims(all_defs, this_arg)
                all_defs['tasks'][task_def]['task_return'][arg] = dims
                
    return all_defs
                

#----------------------------------------------------------    
def build_task_calls(all_defs, kite_cfg, bi_ops_regex):
    """build task call list"""
    
    # search all fields where tasks can be called
    # for each task call replace the task call with the result(s) from
    # the local list, and the signal to the local signal list (implicit)
    # and add the task to the task call list
    # check task has been defined and the call template matches!
    
    task_call_list = []
    task_local_desc = {}
    desc_to_check = ['match','exceptions','retire','interrupt','reset']
    
    
    # search instructions
    for instruction_name in all_defs['instructions'].keys():  
        instruction = all_defs['instructions'][instruction_name]
        local_postfix = "_"+instruction_name.lower()+"_kite_tclocal_sig_"
        local_idx = 0
        for update_type in instruction.keys():
            # locals sections is organised differently to others           
            if update_type == "locals":
                # tasks can be in the assignments list in the update_to_value or condition parts
                for aidx, assignment in enumerate(instruction['locals']['assignments']):
                    for apart in assignment.keys():
                        a_task_list = []
                        a_locals = []
                        is_condition = False
                        if apart == "condition":
                            is_condition = True  
                        a_task_list, a_task_asmt, a_locals, all_defs['instructions'][instruction_name]['locals']['assignments'][aidx][apart], local_idx  = \
                                extract_tasks_from_string(all_defs, assignment[apart], bi_ops_regex, local_postfix, local_idx, is_condition, instruction_name)
                        task_call_list.extend(a_task_list) 
                        if len(a_task_list) > 0:
                            ###print("added task call "+str(a_task_list))
                            # if the value to update is only the assignment from the task, remove the assignment as the signal(s) will already be assigned by the 
                            # task call                            
                            if not (apart == "state_to_update"):
                                print("checking if state to update "+assignment["state_to_update"]+" is the same as the task result "+a_task_asmt)
                                if a_task_asmt == assignment["state_to_update"]:
                                    all_defs['instructions'][instruction_name]['locals']['assignments'][aidx]["state_to_update"] = ""
                                
                        all_defs['local_sigs'].update(a_locals) 
                        
                        
            elif (update_type in desc_to_check):
                # tasks can be in the condition list, or in the in the state_action_list update_to_value or condition parts
                for uidx, updater in enumerate(instruction[update_type]):
                    if "condition" in updater.keys():
                        a_task_list = []
                        a_locals = [] 
                        a_task_list, a_task_asmt, a_locals, all_defs['instructions'][instruction_name][update_type][uidx]['condition'], local_idx  = \
                                extract_tasks_from_string(all_defs, updater['condition'], bi_ops_regex, local_postfix, local_idx, True, instruction_name)
                        task_call_list.extend(a_task_list) 
#                        if len(a_task_list) > 0:
#                            print("added task call "+str(a_task_list))
                        all_defs['local_sigs'].update(a_locals)
                
                    for sidx, state_action in enumerate(updater['state_action_list']):
                        for apart in state_action.keys():
                            a_task_list = []
                            a_locals = []
                            is_condition = False
                            if apart == "condition":
                                is_condition = True  
                            a_task_list, a_task_asmt, a_locals, all_defs['instructions'][instruction_name][update_type][uidx]['state_action_list'][sidx][apart], local_idx  = \
                                    extract_tasks_from_string(all_defs, state_action[apart], bi_ops_regex, local_postfix, local_idx, is_condition, instruction_name)
                            task_call_list.extend(a_task_list) 
#                            if len(a_task_list) > 0:
#                                print("added task call "+str(a_task_list))
                            all_defs['local_sigs'].update(a_locals)
                            
    # search globals
    # in globals task calls can only be in the assignment section
    for global_name in all_defs['global_sigs'].keys():  
        global_def = all_defs['global_sigs'][global_name]
        local_postfix = "_glbl_kite_tclocal_sig_"
        local_idx = 0
        for aidx, assgn in enumerate(global_def['assignment']) :
        #if not( global_def['assignment'] == ""):
            a_task_list = []
            a_locals = []
            is_condition = True 
            a_task_list, a_task_asmt, a_locals, all_defs['global_sigs'][global_name]['assignment'][aidx]['condition'], local_idx  = \
                    extract_tasks_from_string(all_defs, assgn['condition'], bi_ops_regex, local_postfix, local_idx, is_condition, global_name)
            task_call_list.extend(a_task_list) 
            all_defs['local_sigs'].update(a_locals) 
            #if "update_to_value" in  global_def['assignment']:
            a_task_list = []
            a_locals = []
            is_condition = True 
            a_task_list, a_task_asmt, a_locals, all_defs['global_sigs'][global_name]['assignment'][aidx]['update_to_value'], local_idx  = \
                    extract_tasks_from_string(all_defs, assgn['update_to_value'], bi_ops_regex, local_postfix, local_idx, is_condition, global_name)
            task_call_list.extend(a_task_list)
            if len(a_task_list) > 0:
                print("added task call "+str(a_task_list)) 
            all_defs['local_sigs'].update(a_locals) 
         
    # add any new task calls to all_defs['task_calls']
    all_defs['task_calls'] = copy.deepcopy(task_call_list)
                        
    return all_defs    
    
#----------------------------------------------------------    
def extract_mem_refs(all_defs, kite_cfg):
    """extract and replace memory references"""
    
    # search all fields where memory references can be made
    # for each memory reference replace the reference with the result(s) from
    # the local list, and the signal to the local signal list with the term it replaced as the action 
    new_local_sigs = {}
    local_prefix = "kite_mref_local_sig_"
    local_idx = 0
    desc_to_check = ['match','exceptions','retire','interrupt','reset']
        
    # search all definition sections except memory definitions themselves
    # handle instructions seperately as do not want to replace references in the 
    # state_to_update_fields
    print("Extracting and replacing memory references...")
    
    for item_type in all_defs.keys():
        if not ((item_type == "memories") | (item_type == "instructions")):
            #turn defs back into a string, simpler to make replacements without worrying about the structure
            def_string = json5.dumps(all_defs[item_type])
            def_string, ref_local_sigs, local_idx = extract_mem_refs_from_string(all_defs, def_string, local_prefix, local_idx)
            if len(ref_local_sigs) > 0:
                new_local_sigs.update(ref_local_sigs)
                all_defs[item_type] = copy.deepcopy(json5.loads(def_string))
        elif (item_type == "instructions"):
            for instruction_name in all_defs['instructions'].keys():  
                instruction = all_defs['instructions'][instruction_name]
                for update_type in instruction.keys():
                    # locals sections is organised differently to others           
                    if update_type == "locals":
                        # refs can be in the assignments list in the update_to_value or condition parts
                        for aidx, assignment in enumerate(instruction['locals']['assignments']):
                            all_defs['instructions'][instruction_name]['locals']['assignments'][aidx]['condition'], ref_local_sigs, local_idx = \
                                extract_mem_refs_from_string(all_defs, assignment['condition'], local_prefix, local_idx)
                            if len(ref_local_sigs) > 0:
                                new_local_sigs.update(ref_local_sigs) 
                            all_defs['instructions'][instruction_name]['locals']['assignments'][aidx]['update_to_value'], ref_local_sigs, local_idx = \
                                extract_mem_refs_from_string(all_defs, assignment['update_to_value'], local_prefix, local_idx)
                            if len(ref_local_sigs) > 0:
                                new_local_sigs.update(ref_local_sigs) 
                                
                    elif (update_type in desc_to_check):
                        # refs can be in the condition list, or in the in the state_action_list update_to_value or condition parts
                        for uidx, updater in enumerate(instruction[update_type]):
                            if "condition" in updater.keys():
                                all_defs['instructions'][instruction_name][update_type][uidx]['condition'], ref_local_sigs, local_idx = \
                                extract_mem_refs_from_string(all_defs, updater['condition'], local_prefix, local_idx)
                                if len(ref_local_sigs) > 0:
                                    new_local_sigs.update(ref_local_sigs)
                        
                            for sidx, state_action in enumerate(updater['state_action_list']):
                                all_defs['instructions'][instruction_name][update_type][uidx]['state_action_list'][sidx]['condition'], ref_local_sigs, local_idx = \
                                extract_mem_refs_from_string(all_defs, state_action['condition'], local_prefix, local_idx)
                                if len(ref_local_sigs) > 0:
                                    new_local_sigs.update(ref_local_sigs)
                                    
                                all_defs['instructions'][instruction_name][update_type][uidx]['state_action_list'][sidx]['update_to_value'], ref_local_sigs, local_idx = \
                                extract_mem_refs_from_string(all_defs, state_action['update_to_value'], local_prefix, local_idx)
                                if len(ref_local_sigs) > 0:
                                    new_local_sigs.update(ref_local_sigs)
                           
    # add the new locals signals at the end, otherwise will keep trying to replace the new ones 
    # with memory references!
    if len(new_local_sigs) > 0:
        all_defs['local_sigs'].update(new_local_sigs)
    print("Done extracting and replacing memory references")                
    return all_defs
            

#----------------------------------------------------------    
def gen_ifilter_list(irm, all_defs, kite_cfg):
    """generate a list of instructions that generated transitions"""
    
    all_instr_list = all_defs['instructions'].keys()
    
    used_instr_list = []
    
    ctype_desc = ['local_sig_trx','global_sig_trx', 'state_trx','memory_trx'] 
    ttype_desc = ['transitions', 'next_state_transitions','state_assignment', 'rd_map_assignment']
    for ctype in ctype_desc:
        for var in irm[ctype].keys():
            for desc_field in irm[ctype][var].keys():
                if(desc_field in ttype_desc):
                    for causer in irm[ctype][var][desc_field].keys():
                        for cidx, condaction in enumerate(irm[ctype][var][desc_field][causer]):
                            if (condaction['cause']['instruction'] in all_instr_list) & (condaction['cause']['instruction'] not in used_instr_list):
                                used_instr_list.append(condaction['cause']['instruction']) 
                                
    # add any instructions to add to the utilized list that do not have transations from the config filter_ilist_exclude list
    used_instr_list.extend(kite_cfg['build_opts']['filter_ilist_exclude'])
    
    return used_instr_list    

#----------------------------------------------------------    
def instruction_log(all_defs, kite_cfg, utilized_instr_list):
    """Writes list of instructions and encoding to stdout"""
    print("*"*90)
    print("")
    print("Instruction listing")
    print("")
    for instruction in all_defs['instructions']:
        if (instruction in utilized_instr_list) | (not (kite_cfg['build_opts']['filter_ilist'])):
            print(f"{instruction:30}{all_defs['instructions'][instruction]['mnemonic']:20}{all_defs['instructions'][instruction]['encoding']:50}")
    print("")
    print("*"*90)
    
    ifile_string_list = []
    ifile_string_list.append("*"*90+"\n")
    ifile_string_list.append("\n")
    ifile_string_list.append("Instruction listing\n")
    ifile_string_list.append("\n")
    for instruction in all_defs['instructions']:
        if (instruction in utilized_instr_list) | (not(kite_cfg['build_opts']['filter_ilist'])):
            ifile_string_list.append(f"{instruction:30}{all_defs['instructions'][instruction]['mnemonic']:20}{all_defs['instructions'][instruction]['encoding']:50}\n")
    ifile_string_list.append("\n")
    ifile_string_list.append("*"*90+"\n")
    
    ifile_name = kite_cfg['file_descriptors']['kite_files_ilist_def']
    ifile_name = os.path.expandvars(ifile_name)
    with open(ifile_name, 'w') as ifile:
        ifile.writelines(ifile_string_list) 
        
    print("wrote instruction list to "+ ifile_name)
    
#----------------------------------------------------------    
def dump_alldefs(all_defs, kite_cfg, name_postfix):
    """Writes all_defs to the specified file"""
    
    
    file_name = kite_cfg['file_descriptors']['kite_files_alldefs_def']
    file_name = os.path.expandvars(file_name)
    #insert the postfix into the filename
    name_bits = file_name.split(".",1)
    if len(name_bits) > 1:
        file_name = name_bits[0]+name_postfix+"."+name_bits[1]
    else:
        file_name = name_bits[0]+name_postfix
    
    with open(file_name, 'w') as adfile:
        json5.dump(all_defs, adfile, indent=2) 
        
    print("wrote all_defs to "+ file_name)
    
    
#----------------------------------------------------------    
def full_debug_instruction_log(all_defs, kite_cfg, utilized_instr_list):
    """Writes list of instructions and encoding to stdout"""
    
    # build a more comprehensive instruction list that can be used by back-end tools to construct debug information
    #log:
    # - instruction name
    # - mnemonic
    # - match condition
    # - encoding
    # - extracted encoding symbols
    
    print("extracting instruction debug information...")
    all_i_desc = {}
    for instruction in all_defs['instructions']: 
        if (instruction in utilized_instr_list) | (not(kite_cfg['build_opts']['filter_ilist'])):       
            i_desc = {}
            print("Search instruction "+instruction)
            instruction_name = instruction
            i_desc['mnemonic'] = all_defs['instructions'][instruction]['mnemonic']
            i_desc['match_cond'] = all_defs['instructions'][instruction]['match'][0]['condition']
            i_desc['match_cond'] = i_desc['match_cond'].replace("true","1'b1")
            i_desc['match_cond'] = i_desc['match_cond'].replace("false","1'b0")
            i_desc['encoding'] = all_defs['instructions'][instruction]['encoding']
        
            # extract encoding symbols (if any)
            merged_desc = extract_symbols_from_encoding(all_defs['instructions'][instruction]['encoding'])
            i_sym_desc = {}
            for symbol in merged_desc.keys():
                sym_desc = {}
                f_range = merged_desc[symbol]['f_range']
                sym_desc['dimensions'] = "["+str(f_range[0])+":"+str(f_range[1])+"]"
                sym_desc['instr_field'] = str(merged_desc[symbol]['i_range'])
                i_sym_desc[symbol] = copy.deepcopy(sym_desc)
            i_desc['symbols'] = copy.deepcopy(i_sym_desc)
            all_i_desc[instruction] = copy.deepcopy(i_desc)        
        
    ifile_name = kite_cfg['file_descriptors']['kite_files_ilist_def']+".debug"
    ifile_name = os.path.expandvars(ifile_name)
    with open(ifile_name, 'w') as dumpfile:
        json5.dump(all_i_desc, dumpfile, indent=2) 
        
    print("wrote instruction list to "+ ifile_name)
        
#----------------------------------------------------------    
def get_signed_unsigned(term):
    """ try and find innermost signed/unsigned term """
    term = copy.deepcopy(term)
    extracted, extracted_term, start, end = kpfe_common.extract_pcontents_pos(["(",")"], term)
    is_s = False
    is_u = False
    if extracted:
        pre_string =  term[:start].lstrip().rstrip()
        sumatch = re.match(r'.*(?<![a-zA-Z0-9_])(signed|unsigned)$',pre_string)
        if sumatch:
            # check if anymore to do
            extracted_bits, term_bits, start_bits = kpfe_common.extract_pcontent_fields(["(",")"], ",", term, 0)
            if(len(term_bits) != 2):
                sys.exit("signed/unsigned definition must include term and resulting length, in term "+term)   
            next_is_s, next_is_u, next_suterm, next_reslen = get_signed_unsigned(term_bits[0])
            if next_is_s | next_is_u:
                suterm = next_suterm
                reslen = next_reslen
                is_s = next_is_s
                is_u = next_is_u
            else:
                if(sumatch.group(1).lstrip().rstrip() == "unsigned"):
                    is_u = True
                    is_s = False                    
                else:
                    is_u = False
                    is_s = True
                suterm = term 
                reslen = int(eval(term_bits[1]))                 
        else:
            suterm = term 
            reslen = 0
            is_u = False
            is_s = False
    else:  
        suterm = term 
        reslen = 0
        is_u = False
        is_s = False 
    
    return is_s, is_u, suterm, reslen 
    
#----------------------------------------------------------    
def remove_signed_unsigned_term(term, all_defs, bi_map): 
    """remove any signed/unsigned ops from the current term"""
    
    fixed_term = term
    all_done = False
    attempts = 0
    while not all_done:
        # find innermost signed/unsigned operator to replace
        is_s, is_u, suterm, reslen = get_signed_unsigned(fixed_term)
        if (not is_s) & (not is_u):
            all_done = True
        else:
            # replace the signed/unsigned term 
            extracted_bits, term_bits, start_bits = kpfe_common.extract_pcontent_fields(["(",")"], ",", suterm, 0)
            if(len(term_bits) != 2):
                sys.exit("signed/unsigned definition must include term and resulting length, in term "+suterm)
            # find the bit length of the unsigned/signed term to be expanded
            su_term_len = kpfe_common.find_term_length(all_defs, term_bits[0], bi_map)
            su_reslen = reslen
            if(su_term_len > su_reslen):
                sys.exit("Error. Result length is shorter than the term length in term : "+term)
            elif (su_term_len == su_reslen):
                new_suterm = suterm
            else:
                num_pad_bits = su_reslen - su_term_len
                if is_s:
                    #sign extend
                    pad_bit = "("+suterm+")["+str(su_term_len-1)+"]"
                elif is_u:
                    pad_bit = "1'b0"
                pad_term = "{"+str(num_pad_bits)+"{"+pad_bit+"}}"
                new_suterm = "{"+pad_term+","+term_bits[0]+"}"
             
            fixed_term = fixed_term.replace(suterm,new_suterm)    
    return fixed_term
   
#----------------------------------------------------------    
def remove_signed_unsigned_ops(all_defs, kite_cfg, bi_map):
    """ replace signed and unsigned operators"""
    
    # replace any signed/unsigned operations in each section of the 
    # specification with a local signal for the term
    
    # replace as with macros by treating the spec as a single string
    # as signed/unsigned can be in expressions anywhere
    
    print("replacing sign/unsigned operators")
    local_postfix = "_kite_sulocal_sig_"
    local_idx = 0
    for item_type in all_defs.keys():
        replaced_su_in_item = 0
        replaced_su = 0
        def_string = json5.dumps(all_defs[item_type])
        all_done = False
        while not all_done:
            sumatch = re.search(r'(?<![a-zA-Z0-9_])unsigned', def_string)
            if sumatch:
                start_end = sumatch.span()
                extracted_su_args, su_end_pos = find_su_args(def_string,start_end[1])
                su_term = "unsigned("+extracted_su_args+")"
                extracted_bits, term_bits, start_bits = kpfe_common.extract_pcontent_fields(["(",")"], ",", su_term, 0) 
                if not(extracted_bits) | (len(term_bits) != 2):
                    sys.exit("Error. Could not extract unsigned fields in "+extracted_su_args)
                repl_field = remove_signed_unsigned_term(su_term, all_defs, bi_map)
                pre_su_string = def_string[:start_end[0]-1] 
                post_su_string = def_string[su_end_pos+2:]                
                def_string = pre_su_string +"\""+repl_field+"\""+ post_su_string
                replaced_su_in_item = 1
            else:
                all_done = True  
                
        if replaced_su_in_item == 1:
            all_defs[item_type] = copy.deepcopy(json5.loads(def_string))
    return all_defs
    
#----------------------------------------------------------    
def gen_tasks_enums(all_defs, kite_cfg):
    """ writes all specified enums and task templates to the specified file """
    
    all_desc = {}
    all_desc['enums'] = copy.deepcopy(all_defs['enums'])
    all_desc['tasks'] = copy.deepcopy(all_defs['tasks'])  
    
    # if the task includes references in the body (enclosed by @<reference>@) resolve the reference
    for task in all_desc['tasks']:
        new_body = []
        replaced_refs = False
        if 'task_body' in all_desc['tasks'][task]:
            for bidx, bodyline in enumerate(all_desc['tasks'][task]['task_body']):
                refmatch = re.findall(r'\@(.+?)\@',bodyline)
                replacing_bodyline = False
                for ref in refmatch:
                    def_fields = ref.split(".") 
                    cdict = all_defs[def_fields[0]]
                    if len(def_fields) > 1:                   
                        try:
                            for dfkey in def_fields[1:]:
                                if isinstance(cdict, list):
                                    cdict = cdict[int(dfkey)]
                                else:    
                                    cdict = cdict[dfkey]   
                            replacing_bodyline = True     
                        except:
                            cdict = ref 
                    refvalue = cdict
                    if replacing_bodyline: 
                        bodyline = bodyline.replace("@"+ref+"@",refvalue) 
                        all_desc['tasks'][task]['task_body'][bidx] = bodyline
    
    task_enum_filename = kite_cfg['file_descriptors']['kite_files_tasks_def']
    task_enum_filename = os.path.expandvars(task_enum_filename)
    with open(task_enum_filename, 'w') as dumpfile:
        json5.dump(all_desc, dumpfile, indent=2) 
        
    print("wrote tasks and enums to "+ task_enum_filename)
    

    
    
    
    
