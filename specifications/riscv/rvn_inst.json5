//
// Copyright 2020  Stuart Hoad, Lesley Shannon
// Reconfigurable Computing Lab, Simon Fraser University.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author(s):
//             Stuart Hoad <stuart_hoad@sfu.ca>
//
//
// RV32/64N extension instruction definition in JSON5 Kite format
// Based on:
//   The RISC-V Instruction Set Manual
//   Volume I: Unprivileged ISA
//   Document Version 20191214-draft
//
//   The RISC-V Instruction Set Manual
//   Volume II: Privileged Architecture
//   Document Version 1.12-draft (29 February 2020) - ***REMOVED IN LATER VERSIONS***


{
"instruction_def": {
    "extension" : "RVN",
    "instructions" : {
        
        "RVN_URET" : {
            "mnemonic" : "URET",
            "encoding" : "{7'b0000000,5'b00010,5'b00000,3'b000,5'b00000,7'b1110011}",
            "match" : [
                {"condition" : "(instruction[31:0] == 32'b00000000001000000000000001110011) &&\
                                HAVE_RVN && (instruction_valid == true) &&\
                                !((interrupt_taken == true) || (reset_taken == true))",
                 "state_action_list": [
                    {"state_to_update" : "instruction_match", "update_to_value" : "true", "condition" : "true"}]}],          
            "exceptions" : [
                {"exception_type": "ILLEGAL_INSTRUCTION",
                 "condition" : "((mode == SUPERVISOR & !(HAVE_SPVR_MODE | (status_tsr == 1'b1))) || \
                                 (mode == SUPERVISOR & !HAVE_USER_MODE)  || \
                                 (mode == MACHINE & !HAVE_USER_MODE))",
                 "state_action_list": [{"state_to_update" : "exc_illegal_instr","update_to_value" : "1'b1","condition" : "true"}] 
                }],
            "retire" : [
                {"condition" : "exception_taken == false",
                 "state_action_list": [
                    {"state_to_update" : "status_uie", "update_to_value" : "status_upie", "condition" : "true"},
                    {"state_to_update" : "status_upie", "update_to_value" : "1'b1", "condition" : "true"},
                    {"state_to_update" : "mode", "update_to_value" : "USER", "condition" : "true"},                   
                    {"state_to_update" : "pc", "update_to_value" : "uepc", "condition" : "true"},
                    {"state_to_update" : "instruction_retire", "update_to_value" : "1'b1", "condition" : "true"}]}]
        }
        
    }
    
    
}
}
