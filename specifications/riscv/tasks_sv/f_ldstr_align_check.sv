//
// Copyright 2020  Stuart Hoad, Lesley Shannon
// Reconfigurable Computing Lab, Simon Fraser University.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author(s):
//             Stuart Hoad <stuart_hoad@sfu.ca>
//
//
// Kite task description for memory access alignement checks
// Based on:
//   The RISC-V Instruction Set Manual
//   Volume I: Unprivileged ISA
//   Document Version 20191214-draft
//
//   The RISC-V Instruction Set Manual
//   Volume II: Privileged Architecture
//   Document Version 1.12-draft (31 July 2021)
//

module f_ldstr_align_check_task_module
(
   input logic                  clk,
   input logic                  rst,
   input logic[31:0]            paddr,
   input integer                bytes,
   input logic[2:0]             support_align,
   output logic                 aligned);

//----------------------------------------
//task body

logic [2:0] allow_misalign_mask;


always @*
begin
    case (support_align)
        3'd0 : allow_misalign_mask = 3'b000;
        3'd1 : allow_misalign_mask = 3'b001;
        3'd2 : allow_misalign_mask = 3'b011;
        3'd3 : allow_misalign_mask = 3'b111;
        default :allow_misalign_mask = 3'bx;
    endcase
end

always @*
begin
    case (bytes[3:0])
        4'd1 : aligned = 1'b1;
        4'd2 : aligned = !(paddr[0] & allow_misalign_mask[0]);
        4'd4 : aligned = !(paddr[1:0] & allow_misalign_mask[1:0]);
        4'd8 : aligned = !(paddr[2:0] & allow_misalign_mask[2:0]);
        default : aligned = 1'bx;
    endcase
end

endmodule
