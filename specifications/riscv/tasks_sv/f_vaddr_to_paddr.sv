//
// Copyright 2020  Stuart Hoad, Lesley Shannon
// Reconfigurable Computing Lab, Simon Fraser University.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author(s):
//             Stuart Hoad <stuart_hoad@sfu.ca>
//
//
// Kite task description for virtual to physical address translation
// REVISIT no translation currently support so returns paddr unmodified and no fault
// Based on:
//   The RISC-V Instruction Set Manual
//   Volume I: Unprivileged ISA
//   Document Version 20191214-draft
//
//   The RISC-V Instruction Set Manual
//   Volume II: Privileged Architecture
//   Document Version 1.12-draft (31 July 2021)
//

module f_vaddr_to_paddr_task_module
(
   input logic             clk,
   input logic             rst,
   input logic[31:0]       vaddr,
   output logic[31:0]      paddr,
   output logic            fault);

//----------------------------------------
//task body

assign paddr = vaddr;
assign fault = 1'b0;

endmodule
