//
// Copyright 2020  Stuart Hoad, Lesley Shannon
// Reconfigurable Computing Lab, Simon Fraser University.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author(s):
//             Stuart Hoad <stuart_hoad@sfu.ca>
//
//
// Kite task description for exception privilege delegation
// Based on:
//   The RISC-V Instruction Set Manual
//   Volume I: Unprivileged ISA
//   Document Version 20191214-draft
//
//   The RISC-V Instruction Set Manual
//   Volume II: Privileged Architecture
//   Document Version 1.12-draft (31 July 2021)
//

module f_deleg_exc_priv_task_module
(
   input logic             clk,
   input logic             rst,
   input logic[5:0]        exc_idx,
   output logic[1:0]       deleg_mode);

//----------------------------------------
//task body

    logic delegate_to_user; 
    logic delegate_to_supervisor;
    logic [1:0] deleg_mode_priv;
    assign delegate_to_supervisor = (misa_s && medeleg[exc_idx]); 
    assign delegate_to_user = misa_u && (misa_s ? (medeleg[exc_idx] && misa_n && sedeleg[exc_idx]):
                                                  (medeleg[exc_idx] && misa_n));
    
    always @*
    begin
        if (delegate_to_user)
            deleg_mode_priv = 2'b00; // user mode
        else
            if (delegate_to_supervisor)
                deleg_mode_priv = 2'b01; // supervisor mode
            else 
                deleg_mode_priv = 2'b11;  // machine mode
        if(deleg_mode_priv < mode) 
            deleg_mode = mode;
        else
            deleg_mode= deleg_mode_priv;
    end
    

endmodule

