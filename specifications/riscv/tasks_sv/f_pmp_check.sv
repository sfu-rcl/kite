//
// Copyright 2020  Stuart Hoad, Lesley Shannon
// Reconfigurable Computing Lab, Simon Fraser University.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author(s):
//             Stuart Hoad <stuart_hoad@sfu.ca>
//
//
// Kite task description for Physical memory protection checks
// Based on:
//   The RISC-V Instruction Set Manual
//   Volume I: Unprivileged ISA
//   Document Version 20191214-draft
//
//   The RISC-V Instruction Set Manual
//   Volume II: Privileged Architecture
//   Document Version 1.12-draft (31 July 2021)
//
//   PMP Enhancements for memory access and execution prevention on Machine effective_mode (Smepmp)
//   Version 0.9.3, 07/2021

module f_pmp_check_task_module
#(
   parameter PMP_PADDR_LEN = 32,
   parameter PMP_G_BYTES = 1,
   parameter PMP_ACTIVE_ENTRIES = 0,
   parameter PMP_ACCESS_MAX_BYTES = 4,
   parameter PMP_ACCESS_G_BYTES = 4
)
(
   input logic                          clk,
   input logic                          rst,
   input logic[PMP_PADDR_LEN-1:0]       paddr,
   input integer                        bytes,
   input logic[1:0]                     acctype,
   output logic                         pmp_check_pass
);

//----------------------------------------
//task body


//--------------------------------------------------------------------------------                              
                                                                                                                
    // A PMP entry is defined as the address and corresponding config                                           
    // Kite implements all 64 entries even if only 0, or 16 are defined as accessable,                          
    // unused ones will not be writable and not be used.        
    
    localparam NUM_ACCESSES = PMP_ACCESS_MAX_BYTES/PMP_ACCESS_G_BYTES; 
    
    localparam [1:0] PRIV_MACHINE = 2'b11;  
    localparam [1:0] ACCTYPE_RD = 2'b00;  
    localparam [1:0] ACCTYPE_WR = 2'b01;
    localparam [1:0] ACCTYPE_EX = 2'b10;  
                                                                                     
    
    logic [55:0] pmp_entries_addr [63:0];       
    logic        pmp_entries_locked [63:0];    
    logic [1:0]  pmp_entries_address_mode [63:0];
    logic        pmp_entries_readable [63:0];   
    logic        pmp_entries_writable [63:0];   
    logic        pmp_entries_executable [63:0]; 
    
    logic [63:0]                access_perms_match [NUM_ACCESSES-1:0];
    logic [63:0]                access_addr_match [NUM_ACCESSES-1:0];
    logic [63:0]                access_part_addr_match [NUM_ACCESSES-1:0];
    logic [NUM_ACCESSES-1:0]    pmp_access_check_pass;
    logic [NUM_ACCESSES-1:0]    pmp_access_check_valid;  
                                            
    logic [55:0] xlen_mask;   
    logic [1:0]  effective_mode;
                                                                               
    logic [55:0] paddr_min [NUM_ACCESSES-1:0];                                                                                 
    logic [55:0] paddr_max [NUM_ACCESSES-1:0];
    
 
    
                                                                   
    // map discrete kite registers to struct/array to make easier to manipulate                                                          
                                                                                                                
    assign pmp_entries_addr[0] = {pmpaddr0_pmpaddr,2'b00};                                                      
    assign pmp_entries_locked[0] = pmp0cfg_l;                                                                   
    assign pmp_entries_address_mode[0] = pmp0cfg_a;                                                             
    assign pmp_entries_readable[0] = pmp0cfg_xwr[0];                                                 
    assign pmp_entries_writable[0] = pmp0cfg_xwr[1];                                                 
    assign pmp_entries_executable[0] =  pmp0cfg_xwr[2];                                              
                                                                                                                
    assign pmp_entries_addr[1] = {pmpaddr1_pmpaddr,2'b00};                                                      
    assign pmp_entries_locked[1] = pmp1cfg_l;                                                                   
    assign pmp_entries_address_mode[1] = pmp1cfg_a;                                                             
    assign pmp_entries_readable[1] = pmp1cfg_xwr[0];                                                 
    assign pmp_entries_writable[1] = pmp1cfg_xwr[1];                                                 
    assign pmp_entries_executable[1] =  pmp1cfg_xwr[2];                                              
                                                                                                                
    assign pmp_entries_addr[2] = {pmpaddr2_pmpaddr,2'b00};                                                      
    assign pmp_entries_locked[2] = pmp2cfg_l;                                                                   
    assign pmp_entries_address_mode[2] = pmp2cfg_a;                                                             
    assign pmp_entries_readable[2] = pmp2cfg_xwr[0];                                                 
    assign pmp_entries_writable[2] = pmp2cfg_xwr[1];                                                 
    assign pmp_entries_executable[2] =  pmp2cfg_xwr[2];                                              
                                                                                                                
    assign pmp_entries_addr[3] = {pmpaddr3_pmpaddr,2'b00};                                                      
    assign pmp_entries_locked[3] = pmp3cfg_l;                                                                   
    assign pmp_entries_address_mode[3] = pmp3cfg_a;                                                             
    assign pmp_entries_readable[3] = pmp3cfg_xwr[0];                                                 
    assign pmp_entries_writable[3] = pmp3cfg_xwr[1];                                                 
    assign pmp_entries_executable[3] =  pmp3cfg_xwr[2];                                              
                                                                                                                
    assign pmp_entries_addr[4] = {pmpaddr4_pmpaddr,2'b00};                                                      
    assign pmp_entries_locked[4] = pmp4cfg_l;                                                                   
    assign pmp_entries_address_mode[4] = pmp4cfg_a;                                                             
    assign pmp_entries_readable[4] = pmp4cfg_xwr[0];                                                 
    assign pmp_entries_writable[4] = pmp4cfg_xwr[1];                                                 
    assign pmp_entries_executable[4] =  pmp4cfg_xwr[2];                                              
                                                                                                                
    assign pmp_entries_addr[5] = {pmpaddr5_pmpaddr,2'b00};                                                      
    assign pmp_entries_locked[5] = pmp5cfg_l;                                                                   
    assign pmp_entries_address_mode[5] = pmp5cfg_a;                                                             
    assign pmp_entries_readable[5] = pmp5cfg_xwr[0];                                                 
    assign pmp_entries_writable[5] = pmp5cfg_xwr[1];                                                 
    assign pmp_entries_executable[5] =  pmp5cfg_xwr[2];                                              
                                                                                                                
    assign pmp_entries_addr[6] = {pmpaddr6_pmpaddr,2'b00};                                                      
    assign pmp_entries_locked[6] = pmp6cfg_l;                                                                   
    assign pmp_entries_address_mode[6] = pmp6cfg_a;                                                             
    assign pmp_entries_readable[6] = pmp6cfg_xwr[0];                                                 
    assign pmp_entries_writable[6] = pmp6cfg_xwr[1];                                                 
    assign pmp_entries_executable[6] =  pmp6cfg_xwr[2];                                              
                                                                                                                
    assign pmp_entries_addr[7] = {pmpaddr7_pmpaddr,2'b00};                                                      
    assign pmp_entries_locked[7] = pmp7cfg_l;                                                                   
    assign pmp_entries_address_mode[7] = pmp7cfg_a;                                                             
    assign pmp_entries_readable[7] = pmp7cfg_xwr[0];                                                 
    assign pmp_entries_writable[7] = pmp7cfg_xwr[1];                                                 
    assign pmp_entries_executable[7] =  pmp7cfg_xwr[2];                                              
                                                                                                                
    assign pmp_entries_addr[8] = {pmpaddr8_pmpaddr,2'b00};                                                      
    assign pmp_entries_locked[8] = pmp8cfg_l;                                                                   
    assign pmp_entries_address_mode[8] = pmp8cfg_a;                                                             
    assign pmp_entries_readable[8] = pmp8cfg_xwr[0];                                                 
    assign pmp_entries_writable[8] = pmp8cfg_xwr[1];                                                 
    assign pmp_entries_executable[8] =  pmp8cfg_xwr[2];                                              
                                                                                                                
    assign pmp_entries_addr[9] = {pmpaddr9_pmpaddr,2'b00};                                                      
    assign pmp_entries_locked[9] = pmp9cfg_l;                                                                   
    assign pmp_entries_address_mode[9] = pmp9cfg_a;                                                             
    assign pmp_entries_readable[9] = pmp9cfg_xwr[0];                                                 
    assign pmp_entries_writable[9] = pmp9cfg_xwr[1];                                                 
    assign pmp_entries_executable[9] =  pmp9cfg_xwr[2];                                              
                                                                                                                
    assign pmp_entries_addr[10] = {pmpaddr10_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[10] = pmp10cfg_l;                                                                 
    assign pmp_entries_address_mode[10] = pmp10cfg_a;                                                           
    assign pmp_entries_readable[10] = pmp10cfg_xwr[0];                                               
    assign pmp_entries_writable[10] = pmp10cfg_xwr[1];                                               
    assign pmp_entries_executable[10] =  pmp10cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[11] = {pmpaddr11_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[11] = pmp11cfg_l;                                                                 
    assign pmp_entries_address_mode[11] = pmp11cfg_a;                                                           
    assign pmp_entries_readable[11] = pmp11cfg_xwr[0];                                               
    assign pmp_entries_writable[11] = pmp11cfg_xwr[1];                                               
    assign pmp_entries_executable[11] =  pmp11cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[12] = {pmpaddr12_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[12] = pmp12cfg_l;                                                                 
    assign pmp_entries_address_mode[12] = pmp12cfg_a;                                                           
    assign pmp_entries_readable[12] = pmp12cfg_xwr[0];                                               
    assign pmp_entries_writable[12] = pmp12cfg_xwr[1];                                               
    assign pmp_entries_executable[12] =  pmp12cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[13] = {pmpaddr13_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[13] = pmp13cfg_l;                                                                 
    assign pmp_entries_address_mode[13] = pmp13cfg_a;                                                           
    assign pmp_entries_readable[13] = pmp13cfg_xwr[0];                                               
    assign pmp_entries_writable[13] = pmp13cfg_xwr[1];                                               
    assign pmp_entries_executable[13] =  pmp13cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[14] = {pmpaddr14_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[14] = pmp14cfg_l;                                                                 
    assign pmp_entries_address_mode[14] = pmp14cfg_a;                                                           
    assign pmp_entries_readable[14] = pmp14cfg_xwr[0];                                               
    assign pmp_entries_writable[14] = pmp14cfg_xwr[1];                                               
    assign pmp_entries_executable[14] =  pmp14cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[15] = {pmpaddr15_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[15] = pmp15cfg_l;                                                                 
    assign pmp_entries_address_mode[15] = pmp15cfg_a;                                                           
    assign pmp_entries_readable[15] = pmp15cfg_xwr[0];                                               
    assign pmp_entries_writable[15] = pmp15cfg_xwr[1];                                               
    assign pmp_entries_executable[15] =  pmp15cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[16] = {pmpaddr16_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[16] = pmp16cfg_l;                                                                 
    assign pmp_entries_address_mode[16] = pmp16cfg_a;                                                           
    assign pmp_entries_readable[16] = pmp16cfg_xwr[0];                                               
    assign pmp_entries_writable[16] = pmp16cfg_xwr[1];                                               
    assign pmp_entries_executable[16] =  pmp16cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[17] = {pmpaddr17_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[17] = pmp17cfg_l;                                                                 
    assign pmp_entries_address_mode[17] = pmp17cfg_a;                                                           
    assign pmp_entries_readable[17] = pmp17cfg_xwr[0];                                               
    assign pmp_entries_writable[17] = pmp17cfg_xwr[1];                                               
    assign pmp_entries_executable[17] =  pmp17cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[18] = {pmpaddr18_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[18] = pmp18cfg_l;                                                                 
    assign pmp_entries_address_mode[18] = pmp18cfg_a;                                                           
    assign pmp_entries_readable[18] = pmp18cfg_xwr[0];                                               
    assign pmp_entries_writable[18] = pmp18cfg_xwr[1];                                               
    assign pmp_entries_executable[18] =  pmp18cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[19] = {pmpaddr19_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[19] = pmp19cfg_l;                                                                 
    assign pmp_entries_address_mode[19] = pmp19cfg_a;                                                           
    assign pmp_entries_readable[19] = pmp19cfg_xwr[0];                                               
    assign pmp_entries_writable[19] = pmp19cfg_xwr[1];                                               
    assign pmp_entries_executable[19] =  pmp19cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[20] = {pmpaddr20_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[20] = pmp20cfg_l;                                                                 
    assign pmp_entries_address_mode[20] = pmp20cfg_a;                                                           
    assign pmp_entries_readable[20] = pmp20cfg_xwr[0];                                               
    assign pmp_entries_writable[20] = pmp20cfg_xwr[1];                                               
    assign pmp_entries_executable[20] =  pmp20cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[21] = {pmpaddr21_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[21] = pmp21cfg_l;                                                                 
    assign pmp_entries_address_mode[21] = pmp21cfg_a;                                                           
    assign pmp_entries_readable[21] = pmp21cfg_xwr[0];                                               
    assign pmp_entries_writable[21] = pmp21cfg_xwr[1];                                               
    assign pmp_entries_executable[21] =  pmp21cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[22] = {pmpaddr22_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[22] = pmp22cfg_l;                                                                 
    assign pmp_entries_address_mode[22] = pmp22cfg_a;                                                           
    assign pmp_entries_readable[22] = pmp22cfg_xwr[0];                                               
    assign pmp_entries_writable[22] = pmp22cfg_xwr[1];                                               
    assign pmp_entries_executable[22] =  pmp22cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[23] = {pmpaddr23_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[23] = pmp23cfg_l;                                                                 
    assign pmp_entries_address_mode[23] = pmp23cfg_a;                                                           
    assign pmp_entries_readable[23] = pmp23cfg_xwr[0];                                               
    assign pmp_entries_writable[23] = pmp23cfg_xwr[1];                                               
    assign pmp_entries_executable[23] =  pmp23cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[24] = {pmpaddr24_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[24] = pmp24cfg_l;                                                                 
    assign pmp_entries_address_mode[24] = pmp24cfg_a;                                                           
    assign pmp_entries_readable[24] = pmp24cfg_xwr[0];                                               
    assign pmp_entries_writable[24] = pmp24cfg_xwr[1];                                               
    assign pmp_entries_executable[24] =  pmp24cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[25] = {pmpaddr25_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[25] = pmp25cfg_l;                                                                 
    assign pmp_entries_address_mode[25] = pmp25cfg_a;                                                           
    assign pmp_entries_readable[25] = pmp25cfg_xwr[0];                                               
    assign pmp_entries_writable[25] = pmp25cfg_xwr[1];                                               
    assign pmp_entries_executable[25] =  pmp25cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[26] = {pmpaddr26_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[26] = pmp26cfg_l;                                                                 
    assign pmp_entries_address_mode[26] = pmp26cfg_a;                                                           
    assign pmp_entries_readable[26] = pmp26cfg_xwr[0];                                               
    assign pmp_entries_writable[26] = pmp26cfg_xwr[1];                                               
    assign pmp_entries_executable[26] =  pmp26cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[27] = {pmpaddr27_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[27] = pmp27cfg_l;                                                                 
    assign pmp_entries_address_mode[27] = pmp27cfg_a;                                                           
    assign pmp_entries_readable[27] = pmp27cfg_xwr[0];                                               
    assign pmp_entries_writable[27] = pmp27cfg_xwr[1];                                               
    assign pmp_entries_executable[27] =  pmp27cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[28] = {pmpaddr28_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[28] = pmp28cfg_l;                                                                 
    assign pmp_entries_address_mode[28] = pmp28cfg_a;                                                           
    assign pmp_entries_readable[28] = pmp28cfg_xwr[0];                                               
    assign pmp_entries_writable[28] = pmp28cfg_xwr[1];                                               
    assign pmp_entries_executable[28] =  pmp28cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[29] = {pmpaddr29_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[29] = pmp29cfg_l;                                                                 
    assign pmp_entries_address_mode[29] = pmp29cfg_a;                                                           
    assign pmp_entries_readable[29] = pmp29cfg_xwr[0];                                               
    assign pmp_entries_writable[29] = pmp29cfg_xwr[1];                                               
    assign pmp_entries_executable[29] =  pmp29cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[30] = {pmpaddr30_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[30] = pmp30cfg_l;                                                                 
    assign pmp_entries_address_mode[30] = pmp30cfg_a;                                                           
    assign pmp_entries_readable[30] = pmp30cfg_xwr[0];                                               
    assign pmp_entries_writable[30] = pmp30cfg_xwr[1];                                               
    assign pmp_entries_executable[30] =  pmp30cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[31] = {pmpaddr31_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[31] = pmp31cfg_l;                                                                 
    assign pmp_entries_address_mode[31] = pmp31cfg_a;                                                           
    assign pmp_entries_readable[31] = pmp31cfg_xwr[0];                                               
    assign pmp_entries_writable[31] = pmp31cfg_xwr[1];                                               
    assign pmp_entries_executable[31] =  pmp31cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[32] = {pmpaddr32_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[32] = pmp32cfg_l;                                                                 
    assign pmp_entries_address_mode[32] = pmp32cfg_a;                                                           
    assign pmp_entries_readable[32] = pmp32cfg_xwr[0];                                               
    assign pmp_entries_writable[32] = pmp32cfg_xwr[1];                                               
    assign pmp_entries_executable[32] =  pmp32cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[33] = {pmpaddr33_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[33] = pmp33cfg_l;                                                                 
    assign pmp_entries_address_mode[33] = pmp33cfg_a;                                                           
    assign pmp_entries_readable[33] = pmp33cfg_xwr[0];                                               
    assign pmp_entries_writable[33] = pmp33cfg_xwr[1];                                               
    assign pmp_entries_executable[33] =  pmp33cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[34] = {pmpaddr34_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[34] = pmp34cfg_l;                                                                 
    assign pmp_entries_address_mode[34] = pmp34cfg_a;                                                           
    assign pmp_entries_readable[34] = pmp34cfg_xwr[0];                                               
    assign pmp_entries_writable[34] = pmp34cfg_xwr[1];                                               
    assign pmp_entries_executable[34] =  pmp34cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[35] = {pmpaddr35_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[35] = pmp35cfg_l;                                                                 
    assign pmp_entries_address_mode[35] = pmp35cfg_a;                                                           
    assign pmp_entries_readable[35] = pmp35cfg_xwr[0];                                               
    assign pmp_entries_writable[35] = pmp35cfg_xwr[1];                                               
    assign pmp_entries_executable[35] =  pmp35cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[36] = {pmpaddr36_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[36] = pmp36cfg_l;                                                                 
    assign pmp_entries_address_mode[36] = pmp36cfg_a;                                                           
    assign pmp_entries_readable[36] = pmp36cfg_xwr[0];                                               
    assign pmp_entries_writable[36] = pmp36cfg_xwr[1];                                               
    assign pmp_entries_executable[36] =  pmp36cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[37] = {pmpaddr37_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[37] = pmp37cfg_l;                                                                 
    assign pmp_entries_address_mode[37] = pmp37cfg_a;                                                           
    assign pmp_entries_readable[37] = pmp37cfg_xwr[0];                                               
    assign pmp_entries_writable[37] = pmp37cfg_xwr[1];                                               
    assign pmp_entries_executable[37] =  pmp37cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[38] = {pmpaddr38_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[38] = pmp38cfg_l;                                                                 
    assign pmp_entries_address_mode[38] = pmp38cfg_a;                                                           
    assign pmp_entries_readable[38] = pmp38cfg_xwr[0];                                               
    assign pmp_entries_writable[38] = pmp38cfg_xwr[1];                                               
    assign pmp_entries_executable[38] =  pmp38cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[39] = {pmpaddr39_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[39] = pmp39cfg_l;                                                                 
    assign pmp_entries_address_mode[39] = pmp39cfg_a;                                                           
    assign pmp_entries_readable[39] = pmp39cfg_xwr[0];                                               
    assign pmp_entries_writable[39] = pmp39cfg_xwr[1];                                               
    assign pmp_entries_executable[39] =  pmp39cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[40] = {pmpaddr40_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[40] = pmp40cfg_l;                                                                 
    assign pmp_entries_address_mode[40] = pmp40cfg_a;                                                           
    assign pmp_entries_readable[40] = pmp40cfg_xwr[0];                                               
    assign pmp_entries_writable[40] = pmp40cfg_xwr[1];                                               
    assign pmp_entries_executable[40] =  pmp40cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[41] = {pmpaddr41_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[41] = pmp41cfg_l;                                                                 
    assign pmp_entries_address_mode[41] = pmp41cfg_a;                                                           
    assign pmp_entries_readable[41] = pmp41cfg_xwr[0];                                               
    assign pmp_entries_writable[41] = pmp41cfg_xwr[1];                                               
    assign pmp_entries_executable[41] =  pmp41cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[42] = {pmpaddr42_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[42] = pmp42cfg_l;                                                                 
    assign pmp_entries_address_mode[42] = pmp42cfg_a;                                                           
    assign pmp_entries_readable[42] = pmp42cfg_xwr[0];                                               
    assign pmp_entries_writable[42] = pmp42cfg_xwr[1];                                               
    assign pmp_entries_executable[42] =  pmp42cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[43] = {pmpaddr43_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[43] = pmp43cfg_l;                                                                 
    assign pmp_entries_address_mode[43] = pmp43cfg_a;                                                           
    assign pmp_entries_readable[43] = pmp43cfg_xwr[0];                                               
    assign pmp_entries_writable[43] = pmp43cfg_xwr[1];                                               
    assign pmp_entries_executable[43] =  pmp43cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[44] = {pmpaddr44_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[44] = pmp44cfg_l;                                                                 
    assign pmp_entries_address_mode[44] = pmp44cfg_a;                                                           
    assign pmp_entries_readable[44] = pmp44cfg_xwr[0];                                               
    assign pmp_entries_writable[44] = pmp44cfg_xwr[1];                                               
    assign pmp_entries_executable[44] =  pmp44cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[45] = {pmpaddr45_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[45] = pmp45cfg_l;                                                                 
    assign pmp_entries_address_mode[45] = pmp45cfg_a;                                                           
    assign pmp_entries_readable[45] = pmp45cfg_xwr[0];                                               
    assign pmp_entries_writable[45] = pmp45cfg_xwr[1];                                               
    assign pmp_entries_executable[45] =  pmp45cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[46] = {pmpaddr46_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[46] = pmp46cfg_l;                                                                 
    assign pmp_entries_address_mode[46] = pmp46cfg_a;                                                           
    assign pmp_entries_readable[46] = pmp46cfg_xwr[0];                                               
    assign pmp_entries_writable[46] = pmp46cfg_xwr[1];                                               
    assign pmp_entries_executable[46] =  pmp46cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[47] = {pmpaddr47_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[47] = pmp47cfg_l;                                                                 
    assign pmp_entries_address_mode[47] = pmp47cfg_a;                                                           
    assign pmp_entries_readable[47] = pmp47cfg_xwr[0];                                               
    assign pmp_entries_writable[47] = pmp47cfg_xwr[1];                                               
    assign pmp_entries_executable[47] =  pmp47cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[48] = {pmpaddr48_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[48] = pmp48cfg_l;                                                                 
    assign pmp_entries_address_mode[48] = pmp48cfg_a;                                                           
    assign pmp_entries_readable[48] = pmp48cfg_xwr[0];                                               
    assign pmp_entries_writable[48] = pmp48cfg_xwr[1];                                               
    assign pmp_entries_executable[48] =  pmp48cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[49] = {pmpaddr49_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[49] = pmp49cfg_l;                                                                 
    assign pmp_entries_address_mode[49] = pmp49cfg_a;                                                           
    assign pmp_entries_readable[49] = pmp49cfg_xwr[0];                                               
    assign pmp_entries_writable[49] = pmp49cfg_xwr[1];                                               
    assign pmp_entries_executable[49] =  pmp49cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[50] = {pmpaddr50_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[50] = pmp50cfg_l;                                                                 
    assign pmp_entries_address_mode[50] = pmp50cfg_a;                                                           
    assign pmp_entries_readable[50] = pmp50cfg_xwr[0];                                               
    assign pmp_entries_writable[50] = pmp50cfg_xwr[1];                                               
    assign pmp_entries_executable[50] =  pmp50cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[51] = {pmpaddr51_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[51] = pmp51cfg_l;                                                                 
    assign pmp_entries_address_mode[51] = pmp51cfg_a;                                                           
    assign pmp_entries_readable[51] = pmp51cfg_xwr[0];                                               
    assign pmp_entries_writable[51] = pmp51cfg_xwr[1];                                               
    assign pmp_entries_executable[51] =  pmp51cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[52] = {pmpaddr52_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[52] = pmp52cfg_l;                                                                 
    assign pmp_entries_address_mode[52] = pmp52cfg_a;                                                           
    assign pmp_entries_readable[52] = pmp52cfg_xwr[0];                                               
    assign pmp_entries_writable[52] = pmp52cfg_xwr[1];                                               
    assign pmp_entries_executable[52] =  pmp52cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[53] = {pmpaddr53_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[53] = pmp53cfg_l;                                                                 
    assign pmp_entries_address_mode[53] = pmp53cfg_a;                                                           
    assign pmp_entries_readable[53] = pmp53cfg_xwr[0];                                               
    assign pmp_entries_writable[53] = pmp53cfg_xwr[1];                                               
    assign pmp_entries_executable[53] =  pmp53cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[54] = {pmpaddr54_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[54] = pmp54cfg_l;                                                                 
    assign pmp_entries_address_mode[54] = pmp54cfg_a;                                                           
    assign pmp_entries_readable[54] = pmp54cfg_xwr[0];                                               
    assign pmp_entries_writable[54] = pmp54cfg_xwr[1];                                               
    assign pmp_entries_executable[54] =  pmp54cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[55] = {pmpaddr55_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[55] = pmp55cfg_l;                                                                 
    assign pmp_entries_address_mode[55] = pmp55cfg_a;                                                           
    assign pmp_entries_readable[55] = pmp55cfg_xwr[0];                                               
    assign pmp_entries_writable[55] = pmp55cfg_xwr[1];                                               
    assign pmp_entries_executable[55] =  pmp55cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[56] = {pmpaddr56_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[56] = pmp56cfg_l;                                                                 
    assign pmp_entries_address_mode[56] = pmp56cfg_a;                                                           
    assign pmp_entries_readable[56] = pmp56cfg_xwr[0];                                               
    assign pmp_entries_writable[56] = pmp56cfg_xwr[1];                                               
    assign pmp_entries_executable[56] =  pmp56cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[57] = {pmpaddr57_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[57] = pmp57cfg_l;                                                                 
    assign pmp_entries_address_mode[57] = pmp57cfg_a;                                                           
    assign pmp_entries_readable[57] = pmp57cfg_xwr[0];                                               
    assign pmp_entries_writable[57] = pmp57cfg_xwr[1];                                               
    assign pmp_entries_executable[57] =  pmp57cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[58] = {pmpaddr58_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[58] = pmp58cfg_l;                                                                 
    assign pmp_entries_address_mode[58] = pmp58cfg_a;                                                           
    assign pmp_entries_readable[58] = pmp58cfg_xwr[0];                                               
    assign pmp_entries_writable[58] = pmp58cfg_xwr[1];                                               
    assign pmp_entries_executable[58] =  pmp58cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[59] = {pmpaddr59_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[59] = pmp59cfg_l;                                                                 
    assign pmp_entries_address_mode[59] = pmp59cfg_a;                                                           
    assign pmp_entries_readable[59] = pmp59cfg_xwr[0];                                               
    assign pmp_entries_writable[59] = pmp59cfg_xwr[1];                                               
    assign pmp_entries_executable[59] =  pmp59cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[60] = {pmpaddr60_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[60] = pmp60cfg_l;                                                                 
    assign pmp_entries_address_mode[60] = pmp60cfg_a;                                                           
    assign pmp_entries_readable[60] = pmp60cfg_xwr[0];                                               
    assign pmp_entries_writable[60] = pmp60cfg_xwr[1];                                               
    assign pmp_entries_executable[60] =  pmp60cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[61] = {pmpaddr61_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[61] = pmp61cfg_l;                                                                 
    assign pmp_entries_address_mode[61] = pmp61cfg_a;                                                           
    assign pmp_entries_readable[61] = pmp61cfg_xwr[0];                                               
    assign pmp_entries_writable[61] = pmp61cfg_xwr[1];                                               
    assign pmp_entries_executable[61] =  pmp61cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[62] = {pmpaddr62_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[62] = pmp62cfg_l;                                                                 
    assign pmp_entries_address_mode[62] = pmp62cfg_a;                                                           
    assign pmp_entries_readable[62] = pmp62cfg_xwr[0];                                               
    assign pmp_entries_writable[62] = pmp62cfg_xwr[1];                                               
    assign pmp_entries_executable[62] =  pmp62cfg_xwr[2];                                            
                                                                                                                
    assign pmp_entries_addr[63] = {pmpaddr63_pmpaddr,2'b00};                                                    
    assign pmp_entries_locked[63] = pmp63cfg_l;                                                                 
    assign pmp_entries_address_mode[63] = pmp63cfg_a;                                                           
    assign pmp_entries_readable[63] = pmp63cfg_xwr[0];                                               
    assign pmp_entries_writable[63] = pmp63cfg_xwr[1];                                               
    assign pmp_entries_executable[63] =  pmp63cfg_xwr[2];     
    
    
                                              
    
    
    // effective mode is status_mpp when status_mprv is 1 for load/stores
    assign effective_mode = (status_mprv && ((acctype == ACCTYPE_RD) || (acctype == ACCTYPE_WR))) ? status_mpp : mode;    
    
    // Kite always describes pmp_addr as a 54-bit state, so mask unused bits if xlen==32                                
    assign xlen_mask = (curr_xlen == 2'b00) ? {{22{1'b0}},{34{1'b1}}} : {56{1'b1}}; 
    
    // Hardware is allowed to perform a single memory access transaction as multiple accesses. These are checked independendently
    // rather than as a single PMP check, so split according to the minimum configured access granularity PMP_ACCESS_G_BYTES
    
    genvar i_pmp_entry;
    genvar i_pmp_access;                                                                                         
    generate for(i_pmp_access=0;i_pmp_access < (PMP_ACCESS_MAX_BYTES/PMP_ACCESS_G_BYTES);i_pmp_access++) begin: gen_i_pmp_access_chk                                                                                
                                                                                                                
        // calculate paddr min/mix - this is the range of the access                                            
        // all bytes within the access must match the address range in order to succeed 
        // for split accesses , all access where the number of bytes accessed > 0 must succeed for the pmp check to pass (checked later)
        
        logic [63:0] addr_match;                                                                                    
        logic [63:0] part_addr_match;                                                                           
        logic [63:0] perms_match;  
        logic [63:0] perms_match_mml0; 
        logic [63:0] perms_match_mml1;                                                                            
        logic [63:0] pmp_entry_match;
    
        integer num_bytes_in_access;
        integer this_access_offset;
        
        
        assign this_access_offset = i_pmp_access*PMP_ACCESS_G_BYTES;
        
        //generate 
        if(PMP_PADDR_LEN < 56) begin: gen_extend_paddr                                                     
            assign paddr_min[i_pmp_access] = {{(56-PMP_PADDR_LEN){1'b0}},paddr} + {{24{1'b0}},this_access_offset};                                                  
        end else begin : gen_truncate_paddr                                                                     
            assign paddr_min[i_pmp_access] = paddr[55:0] + {{24{1'b0}},this_access_offset};                                                                     
        end                                                                                                     
        //endgenerate   
        
        // have to check all possible accesses (static loop condition), determine how many bytes in this access
        assign num_bytes_in_access =  (bytes >= this_access_offset) ? 
                                        (((bytes-this_access_offset) > PMP_ACCESS_G_BYTES) ? PMP_ACCESS_G_BYTES : (bytes-this_access_offset)) :
                                          32'd0;
                                                                                                                                                                                       
        assign paddr_max[i_pmp_access] = (num_bytes_in_access > 0) ? (paddr_min[i_pmp_access] + {{24{1'b0}},(num_bytes_in_access-1)}) : paddr_min[i_pmp_access]; 
                                           
                                                                                                                    
        for(i_pmp_entry=0;i_pmp_entry < 64;i_pmp_entry++) begin: gen_i_pmp_entry_chk                       
                                                                                                                    
            logic [55:0] prev_pmp_addr;                                                                             
            logic [55:0] curr_pmp_addr; 
            logic [55:0] range_lo_tor;
            logic [55:0] range_hi_tor; 
            logic [55:0] range_lo_na4;
            logic [55:0] range_hi_na4;  
            logic [55:0] range_lo_napot;
            logic [55:0] range_hi_napot;
            logic [55:0] range_lo;
            logic [55:0] range_hi;                                                                                  
            logic [55:0] napot_mask;                                                                                
            logic [55:0] napot_len;
            logic check_addr;               
            
            // Address matching 
            // keep track of partial matches as if any partial match occurs then the access fails all entries       
            // (even if it matched another) according to Sail specification
                                                                           
                                                                                                                    
                                    
                                                                                                                    
            // For TOR need previous pmp_addr, if i_pmp_entry == 0 then the previous address is 0                   
            if(i_pmp_entry == 0) begin: gen_entry_0_tor                                                             
                assign prev_pmp_addr = {56{1'b0}};                                                                  
                assign curr_pmp_addr = pmp_entries_addr[i_pmp_entry] & xlen_mask;                                   
            end else begin : ngen_entry_0_tor                                                                       
                assign prev_pmp_addr = pmp_entries_addr[i_pmp_entry-1] & xlen_mask;                                 
                assign curr_pmp_addr = pmp_entries_addr[i_pmp_entry] & xlen_mask;                                   
            end   
            
            
            assign range_lo_tor = prev_pmp_addr; 
            assign range_hi_tor = curr_pmp_addr; 
            
            assign range_lo_na4 = curr_pmp_addr; 
            assign range_hi_na4 = curr_pmp_addr+56'd4;
            
            //calculate mask and length from napot low bits.                                                                   
            always @*                                                                                               
            begin                                                                                                   
                // find first 0                                                                                     
                logic [54:0] curr_mask;                                                                             
                logic [54:0] curr_pmp_addr_shift;                                                                   
                curr_pmp_addr_shift = {curr_pmp_addr[55:2], 1'b1};                                                  
                curr_mask[0] = 1'b1;                                                                                
                for(int i = 0; i < 54; i++) begin                                                                   
                    curr_mask[i+1] = curr_pmp_addr[i+2] && curr_pmp_addr_shift[i] && curr_mask[i];                  
                end                                                                                                 
                                                                                                                    
                napot_mask = {~curr_mask[53:0], 2'b00};                                                             
                napot_len  = {(curr_mask[53:0] + 54'd1),2'd00};                                                     
            end  
            
            assign range_lo_napot = curr_pmp_addr & napot_mask;                                                             
            assign range_hi_napot = range_lo_napot + napot_len;
            
            
            always @*                                                                                               
            begin                                                                                                   
                case(pmp_entries_address_mode[i_pmp_entry])                                                         
                    2'b00   : begin                                                                                 
                        // Off                                                                                          
                        range_lo   = {56{1'b0}};                                                      
                        range_hi   = {56{1'b0}};
                        check_addr = 1'b0;                                                        
                    end                                                                                             
                    2'b01   : begin                                                                                 
                        // TOR                                                                                      
                        range_lo   = range_lo_tor;                             
                        range_hi   = range_hi_tor;                             
                        check_addr = 1'b1;
                    end                                                                                             
                    2'b10   : begin  
                        // NA4                                                                               
                        range_lo   = range_lo_na4;                             
                        range_hi   = range_hi_na4;                             
                        check_addr = 1'b1;
                    end                                                                                             
                    2'b11   : begin   
                        //NAPOT                                                                              
                        range_lo   = range_lo_napot;                    
                        range_hi   = range_hi_napot;                        
                        check_addr = 1'b1;
                    end                                                                                             
                    default : begin                                                                                 
                        range_lo   = {56{1'b0}};                                                       
                        range_hi   = {56{1'b0}};                                                       
                        check_addr = 1'b0;
                    end                                                                                             
                endcase                                                                                                                                                                                                        
            end       
            
            
            always @*                                                                                               
            begin                                                                                                   
                addr_match[i_pmp_entry] = 1'b0;                                                                                   
                part_addr_match[i_pmp_entry] = 1'b0;  
                if (num_bytes_in_access > 0)
                begin
                    if(((paddr_max[i_pmp_access] < range_lo) ||(paddr_min[i_pmp_access] >= range_hi)) || (range_lo >= range_hi))
                    begin 
                        addr_match[i_pmp_entry] = 1'b0;
                        part_addr_match[i_pmp_entry] = 1'b0;
                    end else begin
                        if((paddr_min[i_pmp_access] >= range_lo) && (paddr_max[i_pmp_access] < range_hi))
                        begin
                            addr_match[i_pmp_entry] = 1'b1;
                            part_addr_match[i_pmp_entry] = 1'b0;
                        end else begin
                            addr_match[i_pmp_entry] = 1'b0;
                            part_addr_match[i_pmp_entry] = 1'b1;
                        end           
                    end
                end                                                                     
            end   
                                                                                    
                                                                                                                    
            // check permissions and privilege (mseccfg.mml = 0)                                                                     
            always @*                                                                                               
            begin                                                                                                   
                case(acctype)                                                                                       
                    2'b00   : perms_match_mml0[i_pmp_entry] = pmp_entries_readable[i_pmp_entry] ||                                
                                            ((effective_mode == 2'b11) && !pmp_entries_locked[i_pmp_entry]);                  
                    2'b01   : perms_match_mml0[i_pmp_entry] = pmp_entries_writable[i_pmp_entry] ||                                
                                            ((effective_mode == 2'b11) && !pmp_entries_locked[i_pmp_entry]);                  
                    2'b10   : perms_match_mml0[i_pmp_entry] = pmp_entries_executable[i_pmp_entry] ||                              
                                            ((effective_mode == 2'b11) && !pmp_entries_locked[i_pmp_entry]);                  
                    2'b11   : perms_match_mml0[i_pmp_entry] = 1'b0;                                                      
                    default : perms_match_mml0[i_pmp_entry] = 1'b0;                                                      
                endcase                                                                                             
            end    
            
            // check permissions and privilege (mseccfg.mml = 1)

            always @*                                                                                               
            begin                                                                                                   
                case({pmp_entries_locked[i_pmp_entry],pmp_entries_readable[i_pmp_entry],pmp_entries_writable[i_pmp_entry],pmp_entries_executable[i_pmp_entry]})   
                    4'b0000 : perms_match_mml1[i_pmp_entry] = 1'b0;
                    4'b0001 : perms_match_mml1[i_pmp_entry] = (effective_mode == PRIV_MACHINE) ? 1'b0 : 
                                                                                       (acctype == ACCTYPE_EX);
                    4'b0010 : perms_match_mml1[i_pmp_entry] = (effective_mode == PRIV_MACHINE) ? ((acctype == ACCTYPE_RD) || (acctype == ACCTYPE_WR)) :
                                                                                       (acctype == ACCTYPE_RD);                
                    4'b0011 : perms_match_mml1[i_pmp_entry] = (effective_mode == PRIV_MACHINE) ? ((acctype == ACCTYPE_RD) || (acctype == ACCTYPE_WR)) :
                                                                                       ((acctype == ACCTYPE_RD) || (acctype == ACCTYPE_WR));
                    4'b0100 : perms_match_mml1[i_pmp_entry] = (effective_mode == PRIV_MACHINE) ? 1'b0 : 
                                                                                       (acctype == ACCTYPE_RD);
                    4'b0101 : perms_match_mml1[i_pmp_entry] = (effective_mode == PRIV_MACHINE) ? 1'b0 : 
                                                                                       ((acctype == ACCTYPE_RD) || (acctype == ACCTYPE_EX));
                    4'b0110 : perms_match_mml1[i_pmp_entry] = (effective_mode == PRIV_MACHINE) ? 1'b0 : 
                                                                                       ((acctype == ACCTYPE_RD) || (acctype == ACCTYPE_WR));
                    4'b0111 : perms_match_mml1[i_pmp_entry] = (effective_mode == PRIV_MACHINE) ? 1'b0 : 
                                                                                       ((acctype == ACCTYPE_RD) || (acctype == ACCTYPE_WR) || (acctype == ACCTYPE_EX));
                    4'b1000 : perms_match_mml1[i_pmp_entry] = 1'b0;
                    4'b1001 : perms_match_mml1[i_pmp_entry] = (effective_mode == PRIV_MACHINE) ? (acctype == ACCTYPE_EX) :
                                                                                       1'b0;
                    4'b1010 : perms_match_mml1[i_pmp_entry] = (effective_mode == PRIV_MACHINE) ? (acctype == ACCTYPE_EX) :
                                                                                       (acctype == ACCTYPE_EX);
                    4'b1011 : perms_match_mml1[i_pmp_entry] = (effective_mode == PRIV_MACHINE) ? ((acctype == ACCTYPE_RD) || (acctype == ACCTYPE_EX)) :
                                                                                       (acctype == ACCTYPE_EX);
                    4'b1100 : perms_match_mml1[i_pmp_entry] = (effective_mode == PRIV_MACHINE) ? (acctype == ACCTYPE_RD) :
                                                                                       1'b0;
                    4'b1101 : perms_match_mml1[i_pmp_entry] = (effective_mode == PRIV_MACHINE) ? ((acctype == ACCTYPE_RD) || (acctype == ACCTYPE_EX)) :
                                                                                       1'b0;
                    4'b1110 : perms_match_mml1[i_pmp_entry] = (effective_mode == PRIV_MACHINE) ? ((acctype == ACCTYPE_RD) || (acctype == ACCTYPE_WR)) :
                                                                                       1'b0;
                    4'b1111 : perms_match_mml1[i_pmp_entry] = (effective_mode == PRIV_MACHINE) ? (acctype == ACCTYPE_RD) :
                                                                                       (acctype == ACCTYPE_RD); 
                    default : perms_match_mml1[i_pmp_entry] = 1'b0;                                                    
                endcase                                                                                             
            end 
            
            
            assign perms_match[i_pmp_entry] = mseccfg_mml ? perms_match_mml1[i_pmp_entry] : perms_match_mml0[i_pmp_entry];
                                                                                                             
        end // endgenerate per-entry check                                                                                              
                                                                                                                    
        // determine if access passes or fails                                                                      
        // pmp checks are prioritized by order 0 to 63
        // First PMP that matches determines if it passes or fails regardless of subsequent matches    
    
        logic entry_succeed;
                                                                                          
        always @*                                                                                                  
        begin                                                                                                      
            entry_succeed = 1'b0;                                                                                  
            // to prioritize 0, do in reverse order                                                                
            for(int i = 63; i >= 0; i--) begin                                                                     
                if(addr_match[i] || part_addr_match[i]) begin
                    if(part_addr_match[i])
                        entry_succeed = 1'b0; 
                    else                                                                               
                        entry_succeed = perms_match[i];                                         
                end                                                                                                
            end                                                                                                    
        end         
                                                  
        assign pmp_access_check_pass[i_pmp_access] = |(addr_match | part_addr_match) ? entry_succeed :   
                                                        (!mseccfg_mml ? ((effective_mode == PRIV_MACHINE) && !mseccfg_mmwp):                                                                        
                                                                        ((effective_mode == PRIV_MACHINE) && !mseccfg_mmwp &&                               
                                                                         ((acctype == ACCTYPE_RD) || (acctype == ACCTYPE_WR)))); 
                                                                    
        assign pmp_access_check_valid[i_pmp_access] = (num_bytes_in_access > 0);    
        
        assign access_perms_match[i_pmp_access]     = perms_match;
        assign access_addr_match[i_pmp_access]      = addr_match;
        assign access_part_addr_match[i_pmp_access] = part_addr_match;                                                                                          
                                                
    end                                                                                                           
    endgenerate  
    
    // for access to pass all valid checks (bytes in access > 0) must pass
    
    assign pmp_check_pass = &(pmp_access_check_pass | ~pmp_access_check_valid);
                                                               

endmodule
