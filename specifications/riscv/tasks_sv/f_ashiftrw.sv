//
// Copyright 2020  Stuart Hoad, Lesley Shannon
// Reconfigurable Computing Lab, Simon Fraser University.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author(s):
//             Stuart Hoad <stuart_hoad@sfu.ca>
//
//
// Kite task description for integer multiplication
// Based on:
//   The RISC-V Instruction Set Manual
//   Volume I: Unprivileged ISA
//   Document Version 20191214-draft
//

module f_ashiftrw_task_module
(
   input logic           clk,
   input logic           rst,
   input logic [31:0]    op0,
   input logic [5:0]     places,
   output logic[63:0]    result
);

//----------------------------------------
//task body


//-------------------------------------------------------------------------------- 

    logic [63:0]    ashiftr_double;
    logic [31:0]    ashiftr_res;
    
    assign ashiftr_double = {{32{op0[31]}},op0};    
    assign ashiftr_res = ashiftr_double[places +: 32];
    assign result = {{32{ashiftr_res[31]}},ashiftr_res};

//--------------------------------------------------------------------------------                             
    
endmodule
