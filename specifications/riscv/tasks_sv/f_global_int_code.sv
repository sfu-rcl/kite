//
// Copyright 2020  Stuart Hoad, Lesley Shannon
// Reconfigurable Computing Lab, Simon Fraser University.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author(s):
//             Stuart Hoad <stuart_hoad@sfu.ca>
//
//
// Kite task description for interrupt code and priority generation
// Based on:
//   The RISC-V Instruction Set Manual
//   Volume I: Unprivileged ISA
//   Document Version 20191214-draft
//
//   The RISC-V Instruction Set Manual
//   Volume II: Privileged Architecture
//   Document Version 1.12-draft (31 July 2021)
//

module f_global_int_code_task_module
(
   input logic             clk,
   input logic             rst,
   input logic[15:0]       global_int,
   output logic[5:0]       global_int_code);

//----------------------------------------
//task body

logic [5:0] tmp;
always @*
begin
    tmp = {6{1'b0}};
    // Priority of interrupts (as defined in priv ISA spec)
    if(global_int[11])
       tmp = 6'd11;
    else if(global_int[9])
       tmp = 6'd9;
    else if(global_int[8])
       tmp = 6'd8;
    else if(global_int[7])
       tmp = 6'd7;
    else if(global_int[5])
       tmp = 6'd5;
    else if(global_int[4])
       tmp = 6'd4;
    else if(global_int[3])
       tmp = 6'd3;
    else if(global_int[1])
       tmp = 6'd1;
    else if(global_int[0])
       tmp = 6'd0;
end
assign global_int_code = tmp;

endmodule
