//
// Copyright 2020  Stuart Hoad, Lesley Shannon
// Reconfigurable Computing Lab, Simon Fraser University.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author(s):
//             Stuart Hoad <stuart_hoad@sfu.ca>
//
//
// Kite task description for integer modtiplication
// Based on:
//   The RISC-V Instruction Set Manual
//   Volume I: Unprivileged ISA
//   Document Version 20191214-draft
//

module f_mod_task_module
#(
   parameter OP_WIDTH = 32
)
(
   input logic                          clk,
   input logic                          rst,
   input logic signed [OP_WIDTH-1:0]    op0,
   input logic                          op0_signed,
   input logic signed [OP_WIDTH-1:0]    op1,
   input logic                          op1_signed,
   output logic[OP_WIDTH-1:0]           result
);

//----------------------------------------
//task body


//-------------------------------------------------------------------------------- 


    logic signed [OP_WIDTH:0]     mod_op0_ext;
    logic signed [OP_WIDTH:0]     mod_op1_ext;
    logic signed [OP_WIDTH:0]     mod_result_s;
    logic                         op0_ext_bit;
    logic                         op1_ext_bit;
    logic [OP_WIDTH-1:0]          most_negative;
    
    assign most_negative = {1'b1,{OP_WIDTH-1{1'b0}}};
    
    assign op0_ext_bit = op0_signed ? op0[OP_WIDTH-1] : 1'b0;
    assign op1_ext_bit = op1_signed ? op1[OP_WIDTH-1] : 1'b0;
    
    assign mod_op0_ext = signed'({op0_ext_bit, op0});
    assign mod_op1_ext = signed'({op1_ext_bit, op1});
    
    assign mod_result_s = mod_op0_ext % mod_op1_ext; 
    
    assign result = (op1 == {OP_WIDTH{1'b0}}) ?  op0 :                                                                        // divide by 0
                    ((op0 == most_negative) && (op1 == {OP_WIDTH{1'b1}} && op0_signed && op1_signed)) ? {OP_WIDTH{1'b0}}:     // overflow (signed only)
                    mod_result_s[OP_WIDTH-1:0];


//--------------------------------------------------------------------------------                             
    
endmodule
