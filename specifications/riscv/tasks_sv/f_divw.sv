//
// Copyright 2020  Stuart Hoad, Lesley Shannon
// Reconfigurable Computing Lab, Simon Fraser University.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author(s):
//             Stuart Hoad <stuart_hoad@sfu.ca>
//
//
// Kite task description for integer division
// Based on:
//   The RISC-V Instruction Set Manual
//   Volume I: Unprivileged ISA
//   Document Version 20191214-draft
//

module f_divw_task_module
(
   input  logic           clk,
   input  logic           rst,
   input  logic [31:0]    op0,
   input  logic           op0_signed,
   input  logic [31:0]    op1,
   input  logic           op1_signed,
   output logic[63:0]     result
);

//----------------------------------------
//task body


//-------------------------------------------------------------------------------- 


    logic signed [32:0]     div_op0_ext;
    logic signed [32:0]     div_op1_ext;
    logic signed [32:0]     div_result_s;
    logic                   op0_ext_bit;
    logic                   op1_ext_bit;
    logic [31:0]            most_negative;
    
    assign most_negative = {1'b1,{31{1'b0}}};
    
    assign op0_ext_bit = op0_signed ? op0[31] : 1'b0;
    assign op1_ext_bit = op1_signed ? op1[31] : 1'b0;
    
    assign div_op0_ext = signed'({op0_ext_bit, op0});
    assign div_op1_ext = signed'({op1_ext_bit, op1});
    
    assign div_result_s = div_op0_ext / div_op1_ext; 
    
    assign result = (op1 == {32{1'b0}}) ?  {64{1'b1}} :                                                                         // divide by 0
                    ((op0 == most_negative) && (op1 == {32{1'b1}} && op0_signed && op1_signed)) ? {{32{1'b1}},most_negative}:   // overflow (signed only)
                    {{32{div_result_s[32]}},div_result_s[31:0]};

//--------------------------------------------------------------------------------                             
    
endmodule
